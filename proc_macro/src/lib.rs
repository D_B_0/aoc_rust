use std::ops::{Range, RangeInclusive};

use proc_macro::TokenStream;
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, LitInt, Result, Token,
};

enum Days {
    Single(usize),
    Vec(Vec<usize>),
    Range(Range<usize>),
    RangeInclusive(RangeInclusive<usize>),
}

enum DaysIter {
    Single(Option<usize>),
    Vec(<Vec<usize> as IntoIterator>::IntoIter),
    Range(<Range<usize> as IntoIterator>::IntoIter),
    RangeInclusive(<RangeInclusive<usize> as IntoIterator>::IntoIter),
}

impl Iterator for DaysIter {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            DaysIter::Single(val) => val.take(),
            DaysIter::Vec(vi) => vi.next(),
            DaysIter::Range(ri) => ri.next(),
            DaysIter::RangeInclusive(ri) => ri.next(),
        }
    }
}

impl IntoIterator for Days {
    type Item = usize;

    type IntoIter = DaysIter;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            Days::Single(val) => DaysIter::Single(Some(val)),
            Days::Vec(v) => DaysIter::Vec(v.into_iter()),
            Days::Range(r) => DaysIter::Range(r.into_iter()),
            Days::RangeInclusive(r) => DaysIter::RangeInclusive(r.into_iter()),
        }
    }
}

struct DaysList {
    days: Days,
}

impl Parse for DaysList {
    fn parse(input: ParseStream) -> Result<Self> {
        let first: LitInt = input.parse()?;
        let first = first.base10_parse::<usize>()?;

        if input.is_empty() {
            Ok(Self {
                days: Days::Single(first),
            })
        } else if input.peek(Token![,]) {
            let mut vec = Vec::new();
            vec.push(first);
            while input.parse::<Token![,]>().is_ok() {
                let num: LitInt = input.parse()?;
                let num = num.base10_parse::<usize>()?;
                vec.push(num);
            }
            Ok(Self {
                days: Days::Vec(vec),
            })
        } else if input.peek(Token![..]) {
            input.parse::<Token![..]>().unwrap();
            let inclusive = input.parse::<Token![=]>().is_ok();
            let second: LitInt = input.parse()?;
            let second = second.base10_parse::<usize>()?;
            if inclusive {
                Ok(Self {
                    days: Days::RangeInclusive(first..=second),
                })
            } else {
                Ok(Self {
                    days: Days::Range(first..second),
                })
            }
        } else {
            Err(syn::Error::new(input.span(), "Couldn't parse"))
        }
    }
}

#[proc_macro]
pub fn import_days(input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as DaysList);

    let mut res = String::new();
    for day in parsed.days {
        res.push_str(&format!("mod day_{day:02};\n"))
    }
    res.parse().unwrap()
}

#[proc_macro]
pub fn days(input: TokenStream) -> TokenStream {
    let parsed = parse_macro_input!(input as DaysList);

    let mut res = "vec![".to_owned();
    for day in parsed.days {
        res.push_str(&format!(
            "Box::new(day_{day:02}::Day{day:02}::default()) as Box<dyn Day>,"
        ))
    }
    res.push(']');
    res.parse().unwrap()
}
