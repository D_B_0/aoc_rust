use std::slice::Chunks;

use aoc::{output, Day};

#[derive(Default)]
pub struct Day08 {
    numbers: Vec<u32>,
}

impl Day08 {
    fn layers(&self) -> Chunks<'_, u32> {
        self.numbers.chunks(25 * 6)
    }
}

impl Day for Day08 {
    fn initialize(&mut self, input: &str) {
        self.numbers = input
            .trim()
            .chars()
            .map(|c| c.to_digit(10))
            .collect::<Option<Vec<_>>>()
            .unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(
            self.layers()
                .map(|layer| {
                    let zeroes = layer.iter().filter(|&&n| n == 0).count();
                    let ones = layer.iter().filter(|&&n| n == 1).count();
                    let twos = layer.iter().filter(|&&n| n == 2).count();
                    (zeroes, ones * twos)
                })
                .min_by_key(|(zeroes, _)| *zeroes)
                .unwrap()
                .1
        )
    }

    fn part2(&mut self) -> Vec<String> {
        self.layers()
            .rev()
            .fold([0; 25 * 6], |mut image, layer| {
                for x in 0..25 {
                    for y in 0..6 {
                        if layer[x + y * 25] != 2 {
                            image[x + y * 25] = layer[x + y * 25];
                        }
                    }
                }
                image
            })
            .map(|n| match n {
                0 => ' ',
                1 => '#',
                _ => unreachable!(),
            })
            .chunks(25)
            .map(String::from_iter)
            .collect()
    }

    fn name(&self) -> usize {
        8
    }
}
