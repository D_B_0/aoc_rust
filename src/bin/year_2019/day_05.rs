use aoc::{output, Day};

use super::intcode::Intcode;

#[derive(Default)]
pub struct Day05 {
    program: Intcode,
}

impl Day for Day05 {
    fn initialize(&mut self, input: &str) {
        self.program.parse(input).unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut program = self.program.clone();
        program.push_input(1);
        program.run_to_completion().unwrap();
        let mut last_output = 0;
        while let Some(output) = program.pop_output() {
            assert_eq!(last_output, 0);
            last_output = output;
        }
        output!(last_output)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut program = self.program.clone();
        program.push_input(5);
        program.run_to_completion().unwrap();
        output!(program.pop_output().unwrap())
    }

    fn name(&self) -> usize {
        5
    }
}
