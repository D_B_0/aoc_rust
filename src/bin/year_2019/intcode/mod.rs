use std::{
    collections::VecDeque,
    num::{ParseIntError, TryFromIntError},
};

#[cfg(test)]
mod test;

#[derive(Default, Clone)]
pub struct Intcode {
    program_counter: usize,
    relative_base: isize,
    terminate: bool,
    program: Vec<i128>,
    input: VecDeque<i128>,
    output: VecDeque<i128>,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum IntcodeError {
    UnknownOpCode(usize),
    NegativeOp,
    InvalidAddressingMode(usize),
    InputEmpty,
    RunWhileTerminated,
    InvalidRelativeAddress(TryFromIntError),
    InvalidAddress(TryFromIntError),
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum IntcodeStatus {
    Continue,
    Halted,
    WaitingForInput,
}

impl Intcode {
    pub fn parse(&mut self, input: &str) -> Result<(), ParseIntError> {
        self.program = input
            .trim()
            .split(',')
            .map(|op| op.parse::<i128>())
            .collect::<Result<Vec<_>, _>>()?;
        Ok(())
    }

    pub fn insert(&mut self, value: i128, address: usize) {
        if address >= self.program.len() {
            self.program.resize(address + 1, 0);
        }
        self.program[address] = value;
    }

    pub fn at(&mut self, address: usize) -> i128 {
        if address >= self.program.len() {
            self.program.resize(address + 1, 0);
        }
        self.program[address]
    }

    pub fn get_address(&mut self, address: usize) -> Result<usize, IntcodeError> {
        self.at(address)
            .try_into()
            .map_err(|err| IntcodeError::InvalidAddress(err))
    }

    pub fn dereference_at(&mut self, address: usize) -> Result<i128, IntcodeError> {
        let address = self.get_address(address)?;
        Ok(self.at(address))
    }

    pub fn run(&mut self) -> Result<IntcodeStatus, IntcodeError> {
        if self.terminate {
            return Err(IntcodeError::RunWhileTerminated);
        }
        let (mut modes, opcode) = self.pop_next_op()?;
        match opcode {
            1 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg3 = self.pop_next_literal(modes % 10)?;
                self.insert(arg1 + arg2, arg3);
            }
            2 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg3 = self.pop_next_literal(modes % 10)?;
                self.insert(arg1 * arg2, arg3);
            }
            3 => {
                if let Ok(input) = self.pop_input() {
                    let arg1 = self.pop_next_literal(modes % 10)?;
                    self.insert(input, arg1)
                } else {
                    self.program_counter -= 1;
                    return Ok(IntcodeStatus::WaitingForInput);
                }
            }
            4 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                self.push_output(arg1);
            }
            5 => {
                let arg1 = self.pop_next_bool(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                if arg1 {
                    self.program_counter = arg2
                        .try_into()
                        .map_err(|err| IntcodeError::InvalidAddress(err))?;
                }
            }
            6 => {
                let arg1 = self.pop_next_bool(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                if !arg1 {
                    self.program_counter = arg2
                        .try_into()
                        .map_err(|err| IntcodeError::InvalidAddress(err))?;
                }
            }
            7 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg3 = self.pop_next_literal(modes % 10)?;
                self.insert(if arg1 < arg2 { 1 } else { 0 }, arg3);
            }
            8 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg2 = self.pop_next_interpreted(modes % 10)?;
                modes /= 10;
                let arg3 = self.pop_next_literal(modes % 10)?;
                self.insert(if arg1 == arg2 { 1 } else { 0 }, arg3);
            }
            9 => {
                let arg1 = self.pop_next_interpreted(modes % 10)?;
                self.relative_base = self.relative_base + arg1 as isize;
            }
            99 => {
                self.terminate = true;
                return Ok(IntcodeStatus::Halted);
            }
            _ => {
                self.terminate = true;
                return Err(IntcodeError::UnknownOpCode(opcode));
            }
        }
        Ok(IntcodeStatus::Continue)
    }

    pub fn run_to_completion(&mut self) -> Result<IntcodeStatus, IntcodeError> {
        loop {
            match self.run() {
                Ok(IntcodeStatus::Continue) => {}
                ret => return ret,
            }
        }
    }

    pub fn push_input(&mut self, value: i128) {
        self.input.push_back(value);
    }

    fn pop_next_op(&mut self) -> Result<(usize, usize), IntcodeError> {
        let op = self.at(self.program_counter);
        if op < 0 {
            return Err(IntcodeError::NegativeOp);
        }
        self.program_counter += 1;
        Ok((op as usize / 100, op as usize % 100))
    }

    fn pop_next_interpreted(&mut self, mode: usize) -> Result<i128, IntcodeError> {
        let ret = match mode {
            // address mode
            0 => self.dereference_at(self.program_counter),
            // immediate mode
            1 => Ok(self.at(self.program_counter)),
            // relative mode
            2 => {
                let relative_address = self.at(self.program_counter);
                Ok(self.at((self.relative_base + relative_address as isize)
                    .try_into()
                    .map_err(|err| IntcodeError::InvalidRelativeAddress(err))?))
            }
            _ => Err(IntcodeError::InvalidAddressingMode(mode)),
        };
        if ret.is_ok() {
            self.program_counter += 1;
        }
        ret
    }

    fn pop_next_literal(&mut self, mode: usize) -> Result<usize, IntcodeError> {
        let ret = match mode {
            // address mode
            0 => self.get_address(self.program_counter),
            // immediate mode is not valid in this case
            // relative mode
            2 => {
                let relative_address = self.at(self.program_counter);
                (self.relative_base + relative_address as isize)
                    .try_into()
                    .map_err(|err| IntcodeError::InvalidRelativeAddress(err))
            }
            _ => Err(IntcodeError::InvalidAddressingMode(mode)),
        };
        if ret.is_ok() {
            self.program_counter += 1;
        }
        ret
    }

    fn pop_input(&mut self) -> Result<i128, IntcodeError> {
        self.input.pop_front().ok_or(IntcodeError::InputEmpty)
    }

    fn push_output(&mut self, value: i128) {
        self.output.push_back(value);
    }

    pub fn pop_output(&mut self) -> Option<i128> {
        self.output.pop_front()
    }

    fn pop_next_bool(&mut self, mode: usize) -> Result<bool, IntcodeError> {
        Ok(self.pop_next_interpreted(mode)? != 0)
    }
}
