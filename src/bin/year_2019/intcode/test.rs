use crate::year_2019::intcode::IntcodeStatus;

use super::Intcode;

#[test]
fn simple_program() {
    let mut program = Intcode::default();
    assert_eq!(program.parse("1,9,10,3,2,3,11,0,99,30,40,50"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.at(0), 3500);
}

#[test]
fn addressing_mode() {
    let mut program = Intcode::default();
    assert_eq!(program.parse("1002,4,3,4,33"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.at(4), 99);
}

#[test]
fn conditionals() {
    let mut program = Intcode::default();
    assert_eq!(program.parse("3,3,1108,-1,8,3,4,3,99"), Ok(()));
    program.push_input(8);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1));

    let mut program = Intcode::default();
    assert_eq!(program.parse("3,3,1108,-1,8,3,4,3,99"), Ok(()));
    program.push_input(7);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(0));

    let mut program = Intcode::default();
    assert_eq!(program.parse("3,9,8,9,10,9,4,9,99,-1,8"), Ok(()));
    program.push_input(8);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1));

    let mut program = Intcode::default();
    assert_eq!(program.parse("3,9,8,9,10,9,4,9,99,-1,8"), Ok(()));
    program.push_input(7);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(0));
}

#[test]
fn jumps() {
    let mut program = Intcode::default();
    assert_eq!(
        program.parse("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"),
        Ok(())
    );
    program.push_input(8);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1));

    let mut program = Intcode::default();
    assert_eq!(
        program.parse("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"),
        Ok(())
    );
    program.push_input(0);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(0));

    let mut program = Intcode::default();
    assert_eq!(program.parse("3,3,1105,-1,9,1101,0,0,12,4,12,99,1"), Ok(()));
    program.push_input(8);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1));

    let mut program = Intcode::default();
    assert_eq!(program.parse("3,3,1105,-1,9,1101,0,0,12,4,12,99,1"), Ok(()));
    program.push_input(0);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(0));
}

#[test]
fn relative_mode() {
    let mut program = Intcode::default();
    assert_eq!(
        program.parse("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"),
        Ok(())
    );
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(
        program.output.iter().cloned().collect::<Vec<_>>(),
        vec![109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
    );

    let mut program = Intcode::default();
    assert_eq!(program.parse("1102,34915192,34915192,7,4,7,99,0"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1219070632396864));

    let mut program = Intcode::default();
    assert_eq!(program.parse("104,1125899906842624,99"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1125899906842624));
}

#[test]
fn relative_input() {
    let mut program = Intcode::default();
    assert_eq!(program.parse("109,4,203,6,4,10,99"), Ok(()));
    program.push_input(69);
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(69))
}

#[test]
fn reddit_tests() {
    let mut program = Intcode::default();
    assert_eq!(program.parse("109,-1,4,1,99"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(-1));

    let mut program = Intcode::default();
    assert_eq!(program.parse("109,-1,104,1,99"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(1));

    let mut program = Intcode::default();
    assert_eq!(program.parse("109,-1,204,1,99"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(109));

    let mut program = Intcode::default();
    assert_eq!(program.parse("109,1,9,2,204,-6,99"), Ok(()));
    assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
    assert_eq!(program.pop_output(), Some(204));
}
