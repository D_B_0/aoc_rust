use aoc::{output, Day};

#[derive(Default)]
pub struct Day01 {
    masses: Vec<u32>,
}

fn mass_to_fuel(mass: u32) -> u32 {
    (mass / 3).saturating_sub(2)
}

fn additional_fuel(fuel: u32) -> u32 {
    let mut current_fuel = fuel;
    let mut additional_fuel = 0;
    while current_fuel > 0 {
        current_fuel = mass_to_fuel(current_fuel);
        additional_fuel += current_fuel
    }
    fuel + additional_fuel
}

impl Day for Day01 {
    fn initialize(&mut self, input: &str) {
        self.masses = input
            .lines()
            .map(|mass| mass.parse::<u32>().unwrap())
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.masses.iter().cloned().map(mass_to_fuel).sum::<u32>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .masses
            .iter()
            .cloned()
            .map(mass_to_fuel)
            .map(additional_fuel)
            .sum::<u32>())
    }

    fn name(&self) -> usize {
        1
    }
}
