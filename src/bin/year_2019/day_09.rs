use aoc::{output, Day};

use super::{Intcode, IntcodeStatus};

#[derive(Default)]
pub struct Day09 {
    program: Intcode,
}

impl Day for Day09 {
    fn initialize(&mut self, input: &str) {
        self.program.parse(input).unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut program = self.program.clone();
        program.push_input(1);
        assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
        output!(program.pop_output().unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut program = self.program.clone();
        program.push_input(2);
        assert_eq!(program.run_to_completion(), Ok(IntcodeStatus::Halted));
        output!(program.pop_output().unwrap())
    }

    fn name(&self) -> usize {
        9
    }
}
