use std::{cmp::Ordering, collections::HashSet};

use aoc::{output, Coprime, Day};

#[derive(Default)]
pub struct Day10 {
    map_width: usize,
    map_height: usize,
    asteroids: HashSet<(usize, usize)>,
    instant_monitoring_station_loc: Option<(usize, usize)>,
}

fn compare_asteroids(&(x1, y1): &(isize, isize), &(x2, y2): &(isize, isize)) -> Ordering {
    if x1 == 0 && y1 < 0 {
        if x2 == 0 && y2 < 0 {
            return y2.abs().cmp(&y1.abs());
        }
        return Ordering::Less;
    }
    if x2 == 0 && y2 < 0 {
        if x1 == 0 && y1 < 0 {
            return y1.abs().cmp(&y2.abs());
        }
        return Ordering::Greater;
    }
    // 1st quadrant         2nd 3rd or 4th quadrant
    if x1 >= 0 && y1 < 0 && !(x2 >= 0 && y2 < 0) {
        return Ordering::Less;
    }
    // 2nd quadrant          3rd or 4th quadrant
    if x1 >= 0 && y1 >= 0 && x2 < 0 {
        return Ordering::Less;
    }
    // 3rd quadrant         4th quadrant
    if x1 < 0 && y1 >= 0 && x2 < 0 && y2 < 0 {
        return Ordering::Less;
    }

    // 1st quadrant         2nd 3rd or 4th quadrant
    if x2 >= 0 && y2 < 0 && !(x1 >= 0 && y1 < 0) {
        return Ordering::Greater;
    }
    // 2nd quadrant          3rd or 4th quadrant
    if x2 >= 0 && y2 >= 0 && x1 < 0 {
        return Ordering::Greater;
    }
    // 3rd quadrant         4th quadrant
    if x2 < 0 && y2 >= 0 && x1 < 0 && y1 < 0 {
        return Ordering::Greater;
    }

    let det = x2 * y1 - x1 * y2;
    if det < 0 {
        return Ordering::Less;
    }
    if det > 0 {
        return Ordering::Greater;
    }
    (x2.abs() + y2.abs()).cmp(&(x1.abs() + y1.abs()))
}

fn get_200th_asteroid(
    asteroids: &HashSet<(usize, usize)>,
    (cx, cy): (usize, usize),
) -> (usize, usize) {
    let mut asteroids = asteroids
        .iter()
        .filter(|asteroid| asteroid != &&(cx, cy))
        .cloned()
        .collect::<Vec<_>>();
    // sort by angle
    asteroids.sort_by(|&(x1, y1), &(x2, y2)| {
        compare_asteroids(
            &(x1 as isize - cx as isize, y1 as isize - cy as isize),
            &(x2 as isize - cx as isize, y2 as isize - cy as isize),
        )
    });
    // give each asteroid a "layer"
    // meaning the order along the particular line it is shot at by the laser
    let mut asteroids = asteroids
        .into_iter()
        .map(|asteroid| (asteroid, 0usize))
        .collect::<Vec<_>>();
    asteroids[0].1 = 1;
    for i in 1..asteroids.len() {
        let (x1, y1) = (
            asteroids[i - 1].0 .0 as isize - cx as isize,
            asteroids[i - 1].0 .1 as isize - cy as isize,
        );
        let (x2, y2) = (
            asteroids[i].0 .0 as isize - cx as isize,
            asteroids[i].0 .1 as isize - cy as isize,
        );
        let det = x2 * y1 - x1 * y2;
        if det == 0 {
            asteroids[i].1 = asteroids[i - 1].1 + 1;
        } else {
            asteroids[i].1 = 1;
        }
    }
    // sort by layer
    asteroids.sort_by(|(_, layer1), (_, layer2)| layer1.cmp(layer2));
    // sort by angle within each layer
    asteroids
        .chunk_by_mut(|(_, layer1), (_, layer2)| layer1 == layer2)
        .for_each(|layer| {
            layer.sort_by(|(asteroid1, _), (asteroid2, _)| {
                let asteroid1 = (
                    asteroid1.0 as isize - cx as isize,
                    asteroid1.1 as isize - cy as isize,
                );
                let asteroid2 = (
                    asteroid2.0 as isize - cx as isize,
                    asteroid2.1 as isize - cy as isize,
                );
                compare_asteroids(&asteroid1, &asteroid2)
            })
        });
    asteroids[199].0
}

impl Day for Day10 {
    fn initialize(&mut self, input: &str) {
        self.map_width = input.lines().next().unwrap().len();
        self.map_height = input.lines().count();
        self.asteroids = input
            .trim()
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.char_indices()
                    .map(move |(x, ch)| if ch == '#' { Some((x, y)) } else { None })
            })
            .filter_map(|coords| coords)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let (coord, num_seeable) = self
            .asteroids
            .iter()
            .map(|(x, y)| {
                let mut seeable = HashSet::new();
                for dx in 0..self.map_width {
                    for dy in 0..self.map_height {
                        if !dx.is_coprime_with(&dy) {
                            continue;
                        }
                        let dirs = [
                            (
                                &usize::checked_add as &dyn Fn(usize, usize) -> Option<usize>,
                                &usize::checked_add as &dyn Fn(usize, usize) -> Option<usize>,
                            ),
                            (
                                &usize::checked_add as &dyn Fn(_, _) -> _,
                                &usize::checked_sub as &dyn Fn(_, _) -> _,
                            ),
                            (
                                &usize::checked_sub as &dyn Fn(_, _) -> _,
                                &usize::checked_add as &dyn Fn(_, _) -> _,
                            ),
                            (
                                &usize::checked_sub as &dyn Fn(_, _) -> _,
                                &usize::checked_sub as &dyn Fn(_, _) -> _,
                            ),
                        ];
                        for (dir_x, dir_y) in dirs {
                            for mul in 1.. {
                                if dx * mul > self.map_width || dy * mul > self.map_height {
                                    break;
                                }
                                let new_x = dir_x(*x, dx * mul);
                                let new_y = dir_y(*y, dy * mul);
                                if let Some(coords) = new_x.zip(new_y) {
                                    if self.asteroids.contains(&coords) {
                                        seeable.insert(coords);
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
                ((*x, *y), seeable.len())
            })
            .max_by_key(|(_, num)| *num)
            .unwrap();
        self.instant_monitoring_station_loc = Some(coord);
        output!(num_seeable)
    }

    fn part2(&mut self) -> Vec<String> {
        let asteroid_200 = get_200th_asteroid(
            &self.asteroids,
            self.instant_monitoring_station_loc.unwrap(),
        );
        output!(asteroid_200.0 * 100 + asteroid_200.1)
    }

    fn name(&self) -> usize {
        10
    }
}

#[cfg(test)]
mod test {
    use aoc::Day;

    use super::*;

    #[test]
    fn test() {
        let mut day = Day10::default();
        day.initialize(".#..#\n.....\n#####\n....#\n...##\n");
        assert_eq!(day.part1(), vec!["8".to_owned()]);
    }

    #[test]
    fn get_200th_asteroid_test() {
        let mut day = Day10::default();
        day.initialize(".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##\n");
        assert_eq!(day.part1(), vec!["210".to_owned()]);
        assert_eq!(day.part2(), vec!["802".to_owned()]);
    }

    #[test]
    fn asteroid_ordering() {
        let mut asteroids: Vec<(isize, isize)> = vec![
            (0, -1),
            (1, -1),
            (1, 0),
            (1, 1),
            (0, 1),
            (-1, 1),
            (-1, 0),
            (-1, -1),
        ];
        asteroids.sort_by(compare_asteroids);
        assert_eq!(
            asteroids,
            vec![
                (0, -1),
                (1, -1),
                (1, 0),
                (1, 1),
                (0, 1),
                (-1, 1),
                (-1, 0),
                (-1, -1),
            ],
            "\n{}",
            {
                let mut string = [' ', ' ', ' ', '\n', ' ', 'X', ' ', '\n', ' ', ' ', ' '];
                for (i, (x, y)) in asteroids.iter().enumerate() {
                    string[(x + 1) as usize + (y + 1) as usize * 4] =
                        char::from_digit(i as u32, 10).unwrap();
                }
                String::from_iter(string.iter())
            }
        );
    }
}
