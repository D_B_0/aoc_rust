use std::{collections::HashSet, str::FromStr};

use aoc::{output, Day};

#[derive(Default)]
struct Wire {
    moves: Vec<(Direction, usize)>,
}

impl Wire {
    fn spots(&self) -> Vec<(isize, isize)> {
        let mut spots = Vec::new();
        let mut curr_spot = (0, 0);
        spots.push(curr_spot);
        for mov in &self.moves {
            for _ in 0..mov.1 {
                match mov.0 {
                    Direction::Up => curr_spot.1 -= 1,
                    Direction::Right => curr_spot.0 += 1,
                    Direction::Down => curr_spot.1 += 1,
                    Direction::Left => curr_spot.0 -= 1,
                }
                spots.push(curr_spot);
            }
        }
        spots
    }
}

enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.chars().next() {
            Some('U') => Ok(Direction::Up),
            Some('R') => Ok(Direction::Right),
            Some('D') => Ok(Direction::Down),
            Some('L') => Ok(Direction::Left),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
enum ParseWireError {
    DirectionError,
    #[allow(dead_code)]
    ParseIntError(std::num::ParseIntError),
}

impl FromStr for Wire {
    type Err = ParseWireError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Wire {
            moves: s
                .split(',')
                .map(|mov| -> Result<_, ParseWireError> {
                    Ok((
                        mov[0..1]
                            .parse::<Direction>()
                            .map_err(|_| ParseWireError::DirectionError)?,
                        mov[1..]
                            .parse::<usize>()
                            .map_err(|err| ParseWireError::ParseIntError(err))?,
                    ))
                })
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}

#[derive(Default)]
pub struct Day03 {
    wire1: Wire,
    wire2: Wire,
}

impl Day for Day03 {
    fn initialize(&mut self, input: &str) {
        let mut lines = input.trim().lines();
        self.wire1 = lines.next().unwrap().parse().unwrap();
        self.wire2 = lines.next().unwrap().parse().unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .wire1
            .spots()
            .into_iter()
            .collect::<HashSet<_>>()
            .intersection(&self.wire2.spots().into_iter().collect::<HashSet<_>>())
            .filter(|&&int| int != (0, 0))
            .map(|(x, y)| x.abs() + y.abs())
            .min()
            .unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        let wire1_spots = self.wire1.spots();
        let wire2_spots = self.wire2.spots();
        let intersections = wire1_spots
            .iter()
            .collect::<HashSet<_>>()
            .intersection(&wire2_spots.iter().collect::<HashSet<_>>())
            .filter(|&&&int| int != (0, 0))
            .cloned()
            .cloned()
            .collect::<Vec<_>>();
        output!(intersections
            .into_iter()
            .map(
                |intersection| wire1_spots.iter().position(|&p| p == intersection).unwrap()
                    + wire2_spots.iter().position(|&p| p == intersection).unwrap()
            )
            .min()
            .unwrap())
    }

    fn name(&self) -> usize {
        3
    }
}

#[cfg(test)]
mod test {
    use aoc::Day;

    use super::Day03;

    #[test]
    fn test() {
        let mut day = Day03::default();
        day.initialize("R8,U5,L5,D3\nU7,R6,D4,L4");
        assert_eq!(day.part1(), vec!["6".to_owned()]);
        assert_eq!(day.part2(), vec!["30".to_owned()]);
    }
}
