use aoc::{Day, Year};

mod intcode;
use intcode::*;

use proc_macro::{days, import_days};

import_days!(1..=10);

#[derive(Default)]
pub(crate) struct Year2019 {}

impl Year for Year2019 {
    fn days(&self) -> Vec<Box<dyn Day>> {
        days!(1..=10)
    }

    fn name(&self) -> usize {
        2019
    }
}
