use aoc::{output, Day, Permutations};

use crate::year_2019::intcode::{IntcodeError, IntcodeStatus};

use super::intcode::Intcode;

#[derive(Default)]
pub struct Day07 {
    program: Intcode,
}

impl Day07 {
    fn eval_phases_1(&self, phases: &[i128]) -> i128 {
        let mut signal = 0;
        for &phase in phases {
            let mut program = self.program.clone();
            program.push_input(phase);
            program.push_input(signal);
            program.run_to_completion().unwrap();
            signal = program.pop_output().unwrap();
        }
        signal
    }

    fn eval_phases_2(&self, phases: &[i128]) -> i128 {
        let mut amps = [
            self.program.clone(),
            self.program.clone(),
            self.program.clone(),
            self.program.clone(),
            self.program.clone(),
        ];
        for i in 0..phases.len() {
            amps[i].push_input(phases[i]);
        }
        let mut signal = 0;
        for i in (0..amps.len()).cycle() {
            amps[i].push_input(signal);
            match amps[i].run_to_completion() {
                Err(IntcodeError::RunWhileTerminated) => {
                    break;
                }
                Err(err) => panic!("Interpreter error {err:?}"),
                Ok(IntcodeStatus::Continue) => unreachable!(),
                Ok(IntcodeStatus::Halted) | Ok(IntcodeStatus::WaitingForInput) => {}
            }
            signal = amps[i].pop_output().unwrap();
        }
        signal
    }
}

impl Day for Day07 {
    fn initialize(&mut self, input: &str) {
        self.program.parse(input).unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut signal = 0;
        for phases in (0..=4).permutations() {
            signal = signal.max(self.eval_phases_1(&phases))
        }
        output!(signal)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut signal = 0;
        for phases in (5..=9).permutations() {
            signal = signal.max(self.eval_phases_2(&phases))
        }
        output!(signal)
    }

    fn name(&self) -> usize {
        7
    }
}

#[cfg(test)]
mod test {
    use aoc::Day;

    use super::Day07;

    #[test]
    fn test() {
        let mut day = Day07::default();
        day.initialize("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0");
        assert_eq!(day.part1(), vec!["43210".to_owned()]);
        let mut day = Day07::default();
        day.initialize(
            "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5",
        );
        assert_eq!(day.part2(), vec!["139629729".to_owned()]);
    }
}
