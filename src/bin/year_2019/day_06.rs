use std::collections::{HashMap, HashSet};

use aoc::{output, Day};

#[derive(Default)]
pub struct Day06 {
    orbits: HashMap<[char; 3], [char; 3]>,
}

impl Day for Day06 {
    fn initialize(&mut self, input: &str) {
        self.orbits = input
            .trim()
            .lines()
            .map(|orbit| {
                let (orbitee, orbiter) = orbit.split_once(')').unwrap();
                let mut orbitee = orbitee.chars();
                let orbitee = [
                    orbitee.next().unwrap(),
                    orbitee.next().unwrap(),
                    orbitee.next().unwrap(),
                ];
                let mut orbiter = orbiter.chars();
                let orbiter = [
                    orbiter.next().unwrap(),
                    orbiter.next().unwrap(),
                    orbiter.next().unwrap(),
                ];
                (orbiter, orbitee)
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut orbit_ammt = 0;
        for object in self.orbits.keys() {
            let mut maybe_orbitee = self.orbits.get(object);
            while let Some(orbitee) = maybe_orbitee {
                maybe_orbitee = self.orbits.get(orbitee);
                orbit_ammt += 1;
            }
        }
        output!(orbit_ammt)
    }

    fn part2(&mut self) -> Vec<String> {
        let goal = self.orbits.get(&['S', 'A', 'N']).unwrap();
        let node = self.orbits.get(&['Y', 'O', 'U']).unwrap();
        let mut visited = HashSet::new();
        visited.insert(node);
        let mut stack = Vec::new();
        let mut distances = HashMap::new();
        distances.insert(node, 0u32);
        stack.push(node);
        while let Some(node) = stack.pop() {
            visited.insert(node);
            let mut neighbors = self
                .orbits
                .iter()
                .filter_map(|(k, v)| if v == node { Some(k) } else { None })
                .collect::<Vec<_>>();
            if let Some(orbitee) = self.orbits.get(node) {
                neighbors.push(orbitee);
            }
            if distances.keys().find(|n| **n == node).is_none() {
                let distance = neighbors
                    .iter()
                    .filter_map(|n| distances.get(*n))
                    .cloned()
                    .min()
                    .unwrap()
                    + 1;
                distances.insert(node, distance);
            }
            stack.extend(neighbors.iter().filter(|n| !visited.contains(**n)));
        }
        output!(distances.get(goal).unwrap())
    }

    fn name(&self) -> usize {
        6
    }
}

#[cfg(test)]
mod test {
    use aoc::Day;

    use super::Day06;

    #[test]
    fn test() {
        let mut day = Day06::default();
        day.initialize("COM)BBB\nBBB)CCC\nCCC)DDD\nDDD)EEE\nEEE)FFF\nBBB)GGG\nGGG)HHH\nDDD)III\nEEE)JJJ\nJJJ)KKK\nKKK)LLL\n");
        assert_eq!(day.part1(), vec!["42".to_owned()]);
    }
}
