use aoc::{output, Day};

use super::intcode::Intcode;

#[derive(Default)]
pub struct Day02 {
    program: Intcode,
}

impl Day for Day02 {
    fn initialize(&mut self, input: &str) {
        self.program.parse(input).unwrap();
        self.program.insert(12, 1);
        self.program.insert(2, 2);
    }

    fn part1(&mut self) -> Vec<String> {
        let mut program = self.program.clone();
        program.run_to_completion().unwrap();
        output!(program.at(0))
    }

    fn part2(&mut self) -> Vec<String> {
        for noun in 0..100 {
            for verb in 0..100 {
                let mut program = self.program.clone();
                program.insert(noun, 1);
                program.insert(verb, 2);
                program.run_to_completion().unwrap();
                if program.at(0) == 19690720 {
                    return output!(100 * noun + verb);
                }
            }
        }
        unreachable!()
    }

    fn name(&self) -> usize {
        2
    }
}
