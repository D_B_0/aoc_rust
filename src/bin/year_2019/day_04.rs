use aoc::{output, Day};

#[derive(Default)]
pub struct Day04 {
    low: usize,
    high: usize,
}

impl Day04 {
    fn low5(&self) -> usize {
        self.low / 100_000 % 10
    }

    fn high5(&self) -> usize {
        self.high / 100_000 % 10
    }
}

impl Day for Day04 {
    fn initialize(&mut self, input: &str) {
        let (low, high) = input.split_once('-').unwrap();
        self.low = low.parse().unwrap();
        self.high = high.parse().unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut possibilities = 0;
        for digit5 in self.low5()..=self.high5() {
            for digit4 in digit5..=9 {
                for digit3 in digit4..=9 {
                    for digit2 in digit3..=9 {
                        for digit1 in digit2..=9 {
                            for digit0 in digit1..=9 {
                                if (self.low..=self.high).contains(
                                    &(digit5 * 100_000
                                        + digit4 * 10_000
                                        + digit3 * 1_000
                                        + digit2 * 100
                                        + digit1 * 10
                                        + digit0),
                                ) && (digit0 == digit1
                                    || digit1 == digit2
                                    || digit2 == digit3
                                    || digit3 == digit4
                                    || digit4 == digit5)
                                {
                                    possibilities += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        output!(possibilities)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut possibilities = 0;
        for digit5 in self.low5()..=self.high5() {
            for digit4 in digit5..=9 {
                for digit3 in digit4..=9 {
                    for digit2 in digit3..=9 {
                        for digit1 in digit2..=9 {
                            for digit0 in digit1..=9 {
                                if (self.low..=self.high).contains(
                                    &(digit5 * 100_000
                                        + digit4 * 10_000
                                        + digit3 * 1_000
                                        + digit2 * 100
                                        + digit1 * 10
                                        + digit0),
                                ) && ((digit0 == digit1 && digit1 != digit2)
                                    || (digit1 == digit2 && digit1 != digit0 && digit2 != digit3)
                                    || (digit2 == digit3 && digit2 != digit1 && digit3 != digit4)
                                    || (digit3 == digit4 && digit3 != digit2 && digit4 != digit5)
                                    || (digit4 == digit5 && digit4 != digit3))
                                {
                                    possibilities += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        output!(possibilities)
    }

    fn name(&self) -> usize {
        4
    }
}
