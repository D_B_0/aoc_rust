#![allow(clippy::nonminimal_bool)]

use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day18 {
    grid_1: Option<[bool; 100 * 100]>,
    grid_2: Option<[bool; 100 * 100]>,
}

impl Day18 {
    fn get_1<T: TryInto<usize>>(&self, x: T, y: T) -> bool {
        let x = x.try_into().unwrap_or(101);
        let y = y.try_into().unwrap_or(101);

        if x >= 100 {
            return false;
        }

        *self.grid_1.unwrap().get(y * 100 + x).unwrap_or(&false)
    }

    fn count_neighbours_1(&self, x: usize, y: usize) -> usize {
        let mut count = 0;
        for dx in -1..=1isize {
            for dy in -1..=1isize {
                if dx == 0 && dy == 0 {
                    continue;
                }
                let x = x as isize + dx;
                let y = y as isize + dy;
                if self.get_1(x, y) {
                    count += 1;
                }
            }
        }
        count
    }

    fn step_1(&mut self) {
        let mut new_grid = [false; 100 * 100];
        for x in 0..100 {
            for y in 0..100 {
                let count = self.count_neighbours_1(x, y);
                new_grid[y * 100 + x] = if self.get_1(x, y) {
                    count == 2 || count == 3
                } else {
                    count == 3
                };
            }
        }
        self.grid_1 = Some(new_grid);
    }

    fn get_2<T: TryInto<usize>>(&self, x: T, y: T) -> bool {
        let x = x.try_into().unwrap_or(101);
        let y = y.try_into().unwrap_or(101);

        if x >= 100 {
            return false;
        }

        *self.grid_2.unwrap().get(y * 100 + x).unwrap_or(&false)
    }

    fn count_neighbours_2(&self, x: usize, y: usize) -> usize {
        let mut count = 0;
        for dx in -1..=1isize {
            for dy in -1..=1isize {
                if dx == 0 && dy == 0 {
                    continue;
                }
                let x = x as isize + dx;
                let y = y as isize + dy;
                if self.get_2(x, y) {
                    count += 1;
                }
            }
        }
        count
    }
    fn step_2(&mut self) {
        let mut new_grid = [false; 100 * 100];
        for x in 0..100 {
            for y in 0..100 {
                if (x == 0 && y == 0)
                    || (x == 0 && y == 99)
                    || (x == 99 && y == 0)
                    || (x == 99 && y == 99)
                {
                    new_grid[y * 100 + x] = true;
                    continue;
                }
                let count = self.count_neighbours_2(x, y);
                new_grid[y * 100 + x] = if self.get_2(x, y) {
                    count == 2 || count == 3
                } else {
                    count == 3
                };
            }
        }
        self.grid_2 = Some(new_grid);
    }
}

impl Day for Day18 {
    fn initialize(&mut self, input: &str) {
        let mut grid = [false; 100 * 100];
        for (y, line) in input.lines().enumerate() {
            for (x, char) in line.char_indices() {
                grid[y * 100 + x] = char == '#';
            }
        }
        self.grid_1 = Some(grid);
        self.grid_2 = Some(grid);
    }

    fn part1(&mut self) -> Vec<String> {
        for _ in 0..100 {
            self.step_1();
        }
        output!(self.grid_1.unwrap().iter().filter(|&&b| b).count())
    }

    fn part2(&mut self) -> Vec<String> {
        for _ in 0..100 {
            self.step_2();
        }
        output!(self.grid_2.unwrap().iter().filter(|&&b| b).count())
    }

    fn name(&self) -> usize {
        18
    }
}
