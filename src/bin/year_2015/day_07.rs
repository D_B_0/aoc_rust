use std::{collections::HashMap, num::ParseIntError, str::FromStr};

use aoc::{output, Day};

#[derive(Debug, Clone)]
enum Gate {
    And(Value, Value),
    Or(Value, Value),
    LShift(Value, Value),
    RShift(Value, Value),
    Not(Value),
}

#[derive(Debug, Clone)]
enum Value {
    Lit(u16),
    Wire(String),
}

impl FromStr for Value {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(if let Ok(num) = u16::from_str(s) {
            Value::Lit(num)
        } else {
            Value::Wire(s.into())
        })
    }
}

#[derive(Debug, Clone)]
enum Source {
    Value(Value),
    Gate(Gate),
}

#[derive(Default)]
pub(crate) struct Day07 {
    wires_backup: HashMap<String, Source>,
    wires: HashMap<String, Source>,
}

impl Day07 {
    fn resolve(&mut self, wire: impl Into<String>) {
        let wire = wire.into();
        let val = self.wires.get(&wire).unwrap().clone();
        let new_val = match val {
            Source::Value(val) => match val {
                Value::Lit(_) => return,
                Value::Wire(w) => {
                    self.resolve(&w);
                    self.get(&w).unwrap()
                }
            },
            Source::Gate(g) => match g {
                Gate::And(v1, v2) => {
                    let num1 = match v1 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    let num2 = match v2 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    num1 & num2
                }
                Gate::Or(v1, v2) => {
                    let num1 = match v1 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    let num2 = match v2 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    num1 | num2
                }
                Gate::LShift(v1, v2) => {
                    let num1 = match v1 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    let num2 = match v2 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    num1 << num2
                }
                Gate::RShift(v1, v2) => {
                    let num1 = match v1 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    let num2 = match v2 {
                        Value::Lit(num) => num,
                        Value::Wire(w) => {
                            self.resolve(&w);
                            self.get(&w).unwrap()
                        }
                    };
                    num1 >> num2
                }
                Gate::Not(v) => !match v {
                    Value::Lit(num) => num,
                    Value::Wire(w) => {
                        self.resolve(&w);
                        self.get(&w).unwrap()
                    }
                },
            },
        };
        *self.wires.get_mut(&wire).unwrap() = Source::Value(Value::Lit(new_val));
    }

    fn get(&self, wire: impl Into<String>) -> Option<u16> {
        match self.wires.get(&wire.into()).unwrap() {
            Source::Value(Value::Lit(v)) => Some(*v),
            _ => None,
        }
    }

    fn set(&mut self, wire: impl Into<String>, val: u16) {
        *self.wires.get_mut(&wire.into()).unwrap() = Source::Value(Value::Lit(val));
    }
}

impl Day for Day07 {
    fn initialize(&mut self, input: &str) {
        let mut wires = HashMap::new();
        for line in input.lines() {
            let (lhs, rhs) = line.split_once(" -> ").unwrap();
            let name = rhs.to_string();
            let source = if let Some((s1, s2)) = lhs.split_once(" AND ") {
                let val1 = Value::from_str(s1).unwrap();
                let val2 = Value::from_str(s2).unwrap();
                Source::Gate(Gate::And(val1, val2))
            } else if let Some((s1, s2)) = lhs.split_once(" OR ") {
                let val1 = Value::from_str(s1).unwrap();
                let val2 = Value::from_str(s2).unwrap();
                Source::Gate(Gate::Or(val1, val2))
            } else if let Some((s1, s2)) = lhs.split_once(" LSHIFT ") {
                let val1 = Value::from_str(s1).unwrap();
                let val2 = Value::from_str(s2).unwrap();
                Source::Gate(Gate::LShift(val1, val2))
            } else if let Some((s1, s2)) = lhs.split_once(" RSHIFT ") {
                let val1 = Value::from_str(s1).unwrap();
                let val2 = Value::from_str(s2).unwrap();
                Source::Gate(Gate::RShift(val1, val2))
            } else if lhs.starts_with("NOT") {
                let (_, s) = lhs.split_once(' ').unwrap();
                Source::Gate(Gate::Not(Value::from_str(s).unwrap()))
            } else if let Ok(val) = u16::from_str(lhs) {
                Source::Value(Value::Lit(val))
            } else {
                Source::Value(Value::Wire(lhs.into()))
            };
            wires.insert(name, source);
        }
        self.wires_backup = wires.clone();
        self.wires = wires;
    }

    fn part1(&mut self) -> Vec<String> {
        self.resolve("a");
        let val = self.get("a").unwrap();
        output!(val)
    }

    fn part2(&mut self) -> Vec<String> {
        self.resolve("a");
        let prev_a = self.get("a").unwrap();
        self.wires = self.wires_backup.clone();
        self.set("b", prev_a);

        self.resolve("a");
        let val = self.get("a").unwrap();
        output!(val)
    }

    fn name(&self) -> usize {
        7
    }
}
