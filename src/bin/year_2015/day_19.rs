use std::collections::{HashMap, HashSet};

use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day19 {
    rules: HashMap<String, Vec<String>>,
    molecule: String,
}

enum Token {
    X,
    Paren,
    Comma,
}

impl Day for Day19 {
    fn initialize(&mut self, input: &str) {
        let (rules, mol) = input.trim().split_once("\n\n").unwrap();
        self.molecule = mol.to_owned();
        rules.lines().for_each(|line| {
            let (from, to) = line.split_once(" => ").unwrap();
            self.rules
                .entry(from.to_string())
                .or_default()
                .push(to.to_string());
        });
    }

    fn part1(&mut self) -> Vec<String> {
        let mut substitutions = HashSet::new();
        for (from, to) in &self.rules {
            for (i, _) in self.molecule.match_indices(from) {
                for to in to {
                    let mut copy = self.molecule.clone();
                    copy.replace_range(i..i + from.len(), to);
                    substitutions.insert(copy);
                }
            }
        }
        output!(substitutions.len())
    }

    fn part2(&mut self) -> Vec<String> {
        // Stolen :_(
        // https://www.reddit.com/r/adventofcode/comments/3xflz8/comment/cy4etju/
        let mut tokens = vec![];
        let chars = self.molecule.chars().collect::<Vec<_>>();
        for c in chars.windows(2) {
            if c[0] == 'Y' {
                tokens.push(Token::Comma)
            } else if (c[0] == 'R' && c[1] == 'n') || (c[0] == 'A' && c[1] == 'r') {
                tokens.push(Token::Paren);
            } else if !c[0].is_lowercase() {
                tokens.push(Token::X);
            }
        }
        if chars.last().unwrap().is_uppercase() {
            tokens.push(Token::X);
        }
        output!(
            tokens.len()
                - tokens.iter().filter(|t| matches!(t, Token::Paren)).count()
                - 2 * tokens.iter().filter(|t| matches!(t, Token::Comma)).count()
                - 1
        )
    }

    fn name(&self) -> usize {
        19
    }
}
