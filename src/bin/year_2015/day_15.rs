use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day15 {
    ingredients: [Ingredient; 4],
}

#[derive(Default)]
struct Ingredient {
    capacity: isize,
    durability: isize,
    flavor: isize,
    texture: isize,
    calories: isize,
}

impl Day for Day15 {
    fn initialize(&mut self, input: &str) {
        let mut ing = input.lines().map(|l| {
            let (_, args) = l.split_once(": ").unwrap();
            let mut args = args.split(", ").map(|arg| arg.split_once(' ').unwrap().1);
            Ingredient {
                capacity: args.next().unwrap().parse().unwrap(),
                durability: args.next().unwrap().parse().unwrap(),
                flavor: args.next().unwrap().parse().unwrap(),
                texture: args.next().unwrap().parse().unwrap(),
                calories: args.next().unwrap().parse().unwrap(),
            }
        });
        self.ingredients = [
            ing.next().unwrap(),
            ing.next().unwrap(),
            ing.next().unwrap(),
            ing.next().unwrap(),
        ];
    }

    fn part1(&mut self) -> Vec<String> {
        let mut best = 0;
        for i0 in 0..=100 {
            for i1 in 0..=(100 - i0) {
                for i2 in 0..=(100 - i1 - i0) {
                    let i3 = 100 - i2 - i1 - i0;
                    let capacity = (i0 * self.ingredients[0].capacity
                        + i1 * self.ingredients[1].capacity
                        + i2 * self.ingredients[2].capacity
                        + i3 * self.ingredients[3].capacity)
                        .max(0);
                    let durability = (i0 * self.ingredients[0].durability
                        + i1 * self.ingredients[1].durability
                        + i2 * self.ingredients[2].durability
                        + i3 * self.ingredients[3].durability)
                        .max(0);
                    let flavor = (i0 * self.ingredients[0].flavor
                        + i1 * self.ingredients[1].flavor
                        + i2 * self.ingredients[2].flavor
                        + i3 * self.ingredients[3].flavor)
                        .max(0);
                    let texture = (i0 * self.ingredients[0].texture
                        + i1 * self.ingredients[1].texture
                        + i2 * self.ingredients[2].texture
                        + i3 * self.ingredients[3].texture)
                        .max(0);
                    let score = capacity * durability * flavor * texture;
                    best = best.max(score);
                }
            }
        }
        output!(best)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut best = 0;
        for i0 in 0..=100 {
            for i1 in 0..=(100 - i0) {
                for i2 in 0..=(100 - i1 - i0) {
                    let i3 = 100 - i2 - i1 - i0;
                    let capacity = (i0 * self.ingredients[0].capacity
                        + i1 * self.ingredients[1].capacity
                        + i2 * self.ingredients[2].capacity
                        + i3 * self.ingredients[3].capacity)
                        .max(0);
                    let durability = (i0 * self.ingredients[0].durability
                        + i1 * self.ingredients[1].durability
                        + i2 * self.ingredients[2].durability
                        + i3 * self.ingredients[3].durability)
                        .max(0);
                    let flavor = (i0 * self.ingredients[0].flavor
                        + i1 * self.ingredients[1].flavor
                        + i2 * self.ingredients[2].flavor
                        + i3 * self.ingredients[3].flavor)
                        .max(0);
                    let texture = (i0 * self.ingredients[0].texture
                        + i1 * self.ingredients[1].texture
                        + i2 * self.ingredients[2].texture
                        + i3 * self.ingredients[3].texture)
                        .max(0);
                    let calories = (i0 * self.ingredients[0].calories
                        + i1 * self.ingredients[1].calories
                        + i2 * self.ingredients[2].calories
                        + i3 * self.ingredients[3].calories)
                        .max(0);
                    if calories == 500 {
                        let score = capacity * durability * flavor * texture;
                        best = best.max(score);
                    }
                }
            }
        }
        output!(best)
    }

    fn name(&self) -> usize {
        15
    }
}
