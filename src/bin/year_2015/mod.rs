use aoc::{Day, Year};

use proc_macro::{days, import_days};

import_days!(1..=22);

#[derive(Default)]
pub(crate) struct Year2015 {}

impl Year for Year2015 {
    fn days(&self) -> Vec<Box<dyn Day>> {
        days!(1..=22)
    }

    fn name(&self) -> usize {
        2015
    }
}
