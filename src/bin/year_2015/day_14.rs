use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day14 {
    reindeers: Vec<Reindeer>,
}

#[derive(Clone, Copy)]
struct Reindeer {
    speed: usize,
    duration: usize,
    rest: usize,
}

impl Reindeer {
    fn at(&self, time: usize) -> usize {
        let cycle = time / (self.duration + self.rest);
        let time = time % (self.duration + self.rest);

        self.speed * self.duration * cycle + self.speed * usize::min(time, self.duration)
    }
}

impl Day for Day14 {
    fn initialize(&mut self, input: &str) {
        self.reindeers = input
            .lines()
            .map(|line| {
                let words = line.split(' ').collect::<Vec<_>>();
                Reindeer {
                    speed: words[3].parse().unwrap(),
                    duration: words[6].parse().unwrap(),
                    rest: words[13].parse().unwrap(),
                }
            })
            .collect()
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .reindeers
            .iter()
            .map(|r| Reindeer::at(r, 2503))
            .max()
            .unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut scores = vec![0; self.reindeers.len()];
        for t in 1..=2503 {
            let dists = self
                .reindeers
                .iter()
                .map(|r| Reindeer::at(r, t))
                .collect::<Vec<_>>();
            let max = dists.iter().max().unwrap();
            for (i, dist) in dists.iter().enumerate() {
                if dist == max {
                    scores[i] += 1;
                }
            }
        }
        output!(scores.iter().max().unwrap())
    }

    fn name(&self) -> usize {
        14
    }
}
