use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day05 {
    input: Vec<String>,
}

impl Day05 {
    fn part1_filter(s: &str) -> bool {
        if s.chars()
            .filter(|c| ['a', 'e', 'i', 'o', 'u'].contains(c))
            .count()
            < 3
        {
            return false;
        }
        if !s
            .chars()
            .collect::<Vec<_>>()
            .windows(2)
            .any(|win| win[0] == win[1])
        {
            return false;
        }

        if ["ab", "cd", "pq", "xy"].iter().any(|bad| s.contains(bad)) {
            return false;
        }

        true
    }

    fn part2_filter(s: &str) -> bool {
        let pairs = s
            .chars()
            .collect::<Vec<_>>()
            .windows(2)
            .enumerate()
            .map(|(pos, win)| {
                let mut s = String::new();
                s.push(win[0]);
                s.push(win[1]);
                (pos, s)
            })
            .collect::<Vec<_>>();

        // println!("{s}");

        let mut found = false;
        for (pos, pair) in pairs {
            // println!("Pair {pair}");
            let finds: Vec<_> = s.match_indices(&pair).collect();
            if finds
                .iter()
                .any(|(i, _)| *i != pos && *i != pos + 1 && *i != pos.saturating_sub(1))
            {
                // println!("Found");
                found = true;
                break;
            }
        }

        if !found {
            return false;
        }

        if !s
            .chars()
            .collect::<Vec<_>>()
            .windows(3)
            .any(|win| win[0] == win[2])
        {
            return false;
        }
        true
    }
}

impl Day for Day05 {
    fn initialize(&mut self, input: &str) {
        self.input = input.lines().map(str::to_owned).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.input.iter().filter(|s| Day05::part1_filter(s)).count())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .input
            .iter()
            .filter(|s| { Day05::part2_filter(s) })
            .count())
    }

    fn name(&self) -> usize {
        5
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn nice_1() {
        assert!(Day05::part2_filter("qjhvhtzxzqqjkmpb"))
    }

    #[test]
    fn nice_2() {
        assert!(Day05::part2_filter("xxyxx"))
    }

    #[test]
    fn naughty_1() {
        assert!(!Day05::part2_filter("uurcxstgmygtbstg"))
    }

    #[test]
    fn naughty_2() {
        assert!(!Day05::part2_filter("ieodomkazucvgmuy"))
    }
}
