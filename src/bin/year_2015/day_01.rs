use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day01 {
    data: Vec<i32>,
}

impl Day for Day01 {
    fn initialize(&mut self, input: &str) {
        self.data = input
            .chars()
            .map(|c| match c {
                '(' => 1,
                ')' => -1,
                _ => panic!("Unexpected char in input {c}"),
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.data.iter().sum::<i32>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut sum = 0;
        for (pos, add) in self.data.iter().enumerate() {
            sum += add;
            if sum == -1 {
                return output!(pos + 1);
            }
        }
        panic!("Could not find solution");
    }

    fn name(&self) -> usize {
        1
    }
}
