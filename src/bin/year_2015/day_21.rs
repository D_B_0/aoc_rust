use aoc::{output, Day};

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
struct Item {
    cost: usize,
    dmg: usize,
    arm: usize,
}

const WEAPONS: &[Item] = &[
    Item {
        cost: 8,
        dmg: 4,
        arm: 0,
    },
    Item {
        cost: 10,
        dmg: 5,
        arm: 0,
    },
    Item {
        cost: 25,
        dmg: 6,
        arm: 0,
    },
    Item {
        cost: 40,
        dmg: 7,
        arm: 0,
    },
    Item {
        cost: 74,
        dmg: 8,
        arm: 0,
    },
];

const ARMORS: &[Item] = &[
    Item {
        cost: 13,
        dmg: 0,
        arm: 1,
    },
    Item {
        cost: 31,
        dmg: 0,
        arm: 2,
    },
    Item {
        cost: 53,
        dmg: 0,
        arm: 3,
    },
    Item {
        cost: 75,
        dmg: 0,
        arm: 4,
    },
    Item {
        cost: 102,
        dmg: 0,
        arm: 5,
    },
];

const RINGS: &[Item] = &[
    Item {
        cost: 25,
        dmg: 1,
        arm: 0,
    },
    Item {
        cost: 50,
        dmg: 2,
        arm: 0,
    },
    Item {
        cost: 100,
        dmg: 3,
        arm: 0,
    },
    Item {
        cost: 20,
        dmg: 0,
        arm: 1,
    },
    Item {
        cost: 40,
        dmg: 0,
        arm: 2,
    },
    Item {
        cost: 80,
        dmg: 0,
        arm: 3,
    },
];

#[derive(Default)]
pub(crate) struct Day21 {
    hit_pts: usize,
    dmg: usize,
    arm: usize,
}

impl Day21 {
    fn wins(&self, weapon: Item, armor: Option<Item>, rings: Vec<Item>) -> bool {
        assert!(rings.len() <= 2);
        let mut player_hit_pts: usize = 100;
        let mut boss_hit_pts = self.hit_pts;
        let dmg = weapon.dmg + rings.iter().map(|i| i.dmg).sum::<usize>();
        let arm = armor.unwrap_or_default().arm + rings.iter().map(|i| i.arm).sum::<usize>();
        loop {
            // boss_hit_pts -= dmg - self.arm;
            boss_hit_pts = boss_hit_pts.saturating_sub(dmg.checked_sub(self.arm).unwrap_or(1));
            if boss_hit_pts == 0 {
                return true;
            }
            // player_hit_pts -= self.dmg - arm;
            player_hit_pts = player_hit_pts.saturating_sub(self.dmg.checked_sub(arm).unwrap_or(1));
            if player_hit_pts == 0 {
                return false;
            }
        }
    }

    fn all_possible_equipments() -> Vec<(Item, Option<Item>, Vec<Item>)> {
        let mut equipment = vec![];
        for &weapon in WEAPONS {
            equipment.push((weapon, None, vec![]));
            for &armor in ARMORS {
                equipment.push((weapon, Some(armor), vec![]));
                for &ring_1 in RINGS {
                    equipment.push((weapon, Some(armor), vec![ring_1]));
                    for &ring_2 in RINGS {
                        if ring_1 != ring_2 {
                            equipment.push((weapon, Some(armor), vec![ring_1, ring_2]));
                        }
                    }
                }
            }
            for &ring_1 in RINGS {
                equipment.push((weapon, None, vec![ring_1]));
                for &ring_2 in RINGS {
                    if ring_1 != ring_2 {
                        equipment.push((weapon, None, vec![ring_1, ring_2]));
                    }
                }
            }
        }
        equipment
    }

    fn cost((w, a, r): &(Item, Option<Item>, Vec<Item>)) -> usize {
        w.cost + a.unwrap_or_default().cost + r.iter().map(|i| i.cost).sum::<usize>()
    }
}

impl Day for Day21 {
    fn initialize(&mut self, input: &str) {
        let mut stats = input
            .lines()
            .map(|l| l.split_once(": ").unwrap().1.parse::<usize>().unwrap());
        self.hit_pts = stats.next().unwrap();
        self.dmg = stats.next().unwrap();
        self.arm = stats.next().unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(Self::all_possible_equipments()
            .iter()
            .filter(|(w, a, r)| self.wins(*w, *a, r.clone()))
            .map(Self::cost)
            .min()
            .unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(Self::all_possible_equipments()
            .iter()
            .filter(|(w, a, r)| !self.wins(*w, *a, r.clone()))
            .map(Self::cost)
            .max()
            .unwrap())
    }

    fn name(&self) -> usize {
        21
    }
}
