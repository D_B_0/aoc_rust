use std::collections::HashSet;

use aoc::{output, point::Point, Day};

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl From<char> for Direction {
    fn from(c: char) -> Self {
        match c {
            '^' => Direction::Up,
            'v' => Direction::Down,
            '>' => Direction::Right,
            '<' => Direction::Left,
            _ => panic!("Invalid char"),
        }
    }
}

#[derive(Default)]
pub(crate) struct Day03 {
    data: Vec<Direction>,
}

impl Day for Day03 {
    fn initialize(&mut self, input: &str) {
        self.data = input.chars().map(|c| c.into()).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut set = HashSet::new();
        let mut pos = Point::<isize>::new(0, 0);
        set.insert(pos);
        for d in &self.data {
            match d {
                Direction::Up => pos.y += 1,
                Direction::Down => pos.y -= 1,
                Direction::Left => pos.x -= 1,
                Direction::Right => pos.x += 1,
            }
            set.insert(pos);
        }
        output!(set.len())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut set = HashSet::new();
        let mut pos = [Point::<isize>::new(0, 0); 2];
        let mut robo = false;
        set.insert(pos[usize::from(robo)]);
        for d in &self.data {
            let i = usize::from(robo);
            match d {
                Direction::Up => pos[i].y += 1,
                Direction::Down => pos[i].y -= 1,
                Direction::Left => pos[i].x -= 1,
                Direction::Right => pos[i].x += 1,
            }
            set.insert(pos[i]);
            robo = !robo;
        }
        output!(set.len())
    }

    fn name(&self) -> usize {
        3
    }
}
