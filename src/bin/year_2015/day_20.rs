use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day20 {
    input: usize,
}

impl Day20 {
    fn lowest(n: usize, max_houses: usize) -> usize {
        let limit = n;
        let mut houses = vec![0; limit];
        for elf in 1..limit {
            for visited in (elf..limit).step_by(elf).take(max_houses) {
                houses[visited] += elf;
            }
        }
        houses
            .iter()
            .enumerate()
            .find(|(_, presents)| **presents >= n)
            .map(|(idx, _)| idx)
            .unwrap()
    }
}

impl Day for Day20 {
    fn initialize(&mut self, input: &str) {
        self.input = input.parse().unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(Self::lowest(self.input / 10, usize::MAX))
    }

    fn part2(&mut self) -> Vec<String> {
        output!(Self::lowest(self.input / 11, 50))
    }

    fn name(&self) -> usize {
        20
    }
}
