use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day17 {
    sizes: Vec<usize>,
    part_1: usize,
    part_2: usize,
}

impl Day17 {
    fn count(&mut self, total: usize) {
        let mut counts = vec![0; self.sizes.len()];
        for mask in 1..=2usize.pow(self.sizes.len() as u32) {
            let mut bit_count = 0;
            let mut sum = 0;
            for (i, &size) in self.sizes.iter().enumerate() {
                if mask & (1 << i) != 0 {
                    bit_count += 1;
                    sum += size;
                    if sum > total {
                        break;
                    }
                }
            }
            if sum == total {
                counts[bit_count - 1] += 1;
            }
        }
        self.part_1 = counts.iter().sum();
        self.part_2 = *counts.iter().find(|&&c| c != 0).unwrap();
    }
}

impl Day for Day17 {
    fn initialize(&mut self, input: &str) {
        self.sizes = input
            .split_whitespace()
            .map(|s| s.parse::<usize>())
            .collect::<Result<Vec<_>, _>>()
            .unwrap();
        self.count(150);
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.part_1)
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.part_2)
    }

    fn name(&self) -> usize {
        17
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example() {
        let mut d = Day17 {
            sizes: vec![20, 15, 10, 5, 5],
            ..Default::default()
        };
        d.count(25);
        assert_eq!(4, d.part_1);
        assert_eq!(3, d.part_2);
    }
}
