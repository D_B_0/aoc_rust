use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day11 {
    password: [char; 8],
}

macro_rules! char_array {
    ($str:expr) => {{
        let mut arr: [char; 8] = Default::default();
        arr.copy_from_slice(&$str.chars().collect::<Vec<_>>());
        arr
    }};
}

impl Day11 {
    fn increment(mut pass: [char; 8], i: usize) -> [char; 8] {
        if pass[i] == 'z' {
            pass[i] = 'a';
            Day11::increment(pass, i - 1)
        } else {
            pass[i] = (pass[i] as u8 + 1) as char;
            pass
        }
    }

    fn next(pass: [char; 8]) -> [char; 8] {
        Day11::increment(pass, 7)
    }

    fn next_valid(pass: [char; 8]) -> [char; 8] {
        let mut ret = Day11::next(pass);
        while !Day11::valid(&ret) {
            ret = Day11::next(ret);
        }

        ret
    }

    fn valid(pass: &[char; 8]) -> bool {
        if !pass
            .windows(3)
            .map(|win| win.iter().map(|&c| c as u8).collect::<Vec<_>>())
            .any(|win| win[0] + 1 == win[1] && win[0] + 2 == win[2])
        {
            return false;
        }

        if pass.iter().any(|c| ['i', 'o', 'l'].contains(c)) {
            return false;
        }

        let doubles = pass
            .windows(2)
            .enumerate()
            .filter_map(|(pos, win)| if win[0] == win[1] { Some(pos) } else { None })
            .collect::<Vec<_>>();

        if doubles
            .iter()
            .filter(|&&pos| !doubles.contains(&(pos + 1)) && !doubles.contains(&(pos - 1)))
            .count()
            < 2
        {
            return false;
        }
        true
    }
}

impl Day for Day11 {
    fn initialize(&mut self, input: &str) {
        self.password = char_array!(input);
    }

    fn part1(&mut self) -> Vec<String> {
        self.password = Day11::next_valid(self.password);
        output!(self.password.iter().collect::<String>())
    }

    fn part2(&mut self) -> Vec<String> {
        self.password = Day11::next_valid(self.password);
        output!(self.password.iter().collect::<String>())
    }

    fn name(&self) -> usize {
        11
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn next() {
        assert_eq!(
            Day11::next(char_array!("abcdefgh")),
            char_array!("abcdefgi")
        );
        assert_eq!(
            Day11::next(char_array!("abcdefgz")),
            char_array!("abcdefha")
        );
        assert_eq!(
            Day11::next(char_array!("azzzzzzz")),
            char_array!("baaaaaaa")
        );
    }
}
