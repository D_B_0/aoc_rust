use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day10 {
    look: Vec<u8>,
}

impl Day10 {
    fn look_and_say(look: &[u8]) -> Vec<u8> {
        let mut res = Vec::new();
        let mut iter = look.iter();
        let mut curr = iter.next().unwrap();
        let mut count = 1;
        for n in iter {
            if curr == n {
                count += 1;
            } else {
                res.push(count);
                res.push(*curr);
                curr = n;
                count = 1;
            }
        }
        res.push(count);
        res.push(*curr);
        res
    }
}

impl Day for Day10 {
    fn initialize(&mut self, input: &str) {
        self.look = input
            .chars()
            .filter_map(|c| c.to_digit(10).map(|n| n as u8))
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        for _ in 0..40 {
            self.look = Day10::look_and_say(&self.look);
        }
        output!(self.look.len())
    }

    fn part2(&mut self) -> Vec<String> {
        for _ in 0..10 {
            self.look = Day10::look_and_say(&self.look);
        }
        output!(self.look.len())
    }

    fn name(&self) -> usize {
        10
    }
}
