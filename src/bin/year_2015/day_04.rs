use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day04 {
    input: String,
    start: usize,
}

impl Day for Day04 {
    fn initialize(&mut self, input: &str) {
        self.input = input.to_owned();
        self.start = 1;
    }

    fn part1(&mut self) -> Vec<String> {
        let mut answer = 0;
        for i in self.start.. {
            let key = format!("{}{i}", self.input);
            let digest = md5::compute(key);
            if digest.0[0] == 0 && digest.0[1] == 0 && digest.0[2] & 0xf0 == 0 {
                answer = i;
                break;
            }
        }
        self.start = answer;
        output!(answer)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut answer = 0;
        for i in self.start.. {
            let key = format!("{}{i}", self.input);
            let digest = md5::compute(key);
            if digest.0[0] == 0 && digest.0[1] == 0 && digest.0[2] == 0 {
                answer = i;
                break;
            }
        }
        output!(answer)
    }

    fn name(&self) -> usize {
        4
    }
}
