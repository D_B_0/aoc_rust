use std::collections::{HashMap, HashSet};

use aoc::{output, Day, Permutations};

#[derive(Default)]
pub(crate) struct Day13 {
    people: HashSet<String>,
    relative_happiness: HashMap<(String, String), i64>,
}

impl Day13 {
    fn get_max_happiness(&self) -> i64 {
        let first = self.people.iter().next().unwrap().clone();
        let permutations = self
            .people
            .iter()
            .skip(1)
            .cloned()
            .permutations()
            .map(|mut vec| {
                vec.insert(0, first.clone());
                vec.push(first.clone());
                vec
            })
            .collect::<Vec<_>>();

        permutations
            .iter()
            .map(|order| {
                order
                    .windows(2)
                    .map(|win| {
                        self.relative_happiness
                            .get(&(win[0].clone(), win[1].clone()))
                            .unwrap()
                            + self
                                .relative_happiness
                                .get(&(win[1].clone(), win[0].clone()))
                                .unwrap()
                    })
                    .sum::<i64>()
            })
            .max()
            .unwrap()
    }
}

impl Day for Day13 {
    fn initialize(&mut self, input: &str) {
        let mut people = HashSet::new();
        let relative_happiness = input
            .lines()
            .map(|line| {
                let mut split = line.split(' ');
                let from = split.next().unwrap().to_string();
                let _ = split.next();
                let lose_gain = split.next().unwrap();
                let sign = if lose_gain == "lose" { -1 } else { 1 };
                let num = sign * split.next().unwrap().parse::<i64>().unwrap();
                for _ in 0..6 {
                    let _ = split.next();
                }
                let to = split
                    .next()
                    .unwrap()
                    .split_terminator('.')
                    .next()
                    .unwrap()
                    .to_string();
                people.insert(from.clone());
                people.insert(to.clone());
                ((from, to), num)
            })
            .collect();

        self.people = people;
        self.relative_happiness = relative_happiness;
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.get_max_happiness())
    }

    fn part2(&mut self) -> Vec<String> {
        let me = "Me".to_string();
        for other in &self.people {
            self.relative_happiness
                .insert((me.clone(), other.clone()), 0);
            self.relative_happiness
                .insert((other.clone(), me.clone()), 0);
        }
        self.people.insert(me);
        output!(self.get_max_happiness())
    }

    fn name(&self) -> usize {
        13
    }
}
