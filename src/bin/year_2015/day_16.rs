use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day16 {
    infos: Vec<SueInfo>,
}

#[derive(Default)]
struct SueInfo {
    children: Option<usize>,
    cats: Option<usize>,
    samoyeds: Option<usize>,
    pomeranians: Option<usize>,
    akitas: Option<usize>,
    vizslas: Option<usize>,
    goldfish: Option<usize>,
    trees: Option<usize>,
    cars: Option<usize>,
    perfumes: Option<usize>,
}

struct SueInfoTrue {
    children: usize,
    cats: usize,
    samoyeds: usize,
    pomeranians: usize,
    akitas: usize,
    vizslas: usize,
    goldfish: usize,
    trees: usize,
    cars: usize,
    perfumes: usize,
}

impl SueInfoTrue {
    fn matches_part_1(&self, other: &SueInfo) -> bool {
        if other.children.is_some() && self.children != other.children.unwrap() {
            return false;
        }
        if other.cats.is_some() && self.cats != other.cats.unwrap() {
            return false;
        }
        if other.samoyeds.is_some() && self.samoyeds != other.samoyeds.unwrap() {
            return false;
        }
        if other.pomeranians.is_some() && self.pomeranians != other.pomeranians.unwrap() {
            return false;
        }
        if other.akitas.is_some() && self.akitas != other.akitas.unwrap() {
            return false;
        }
        if other.vizslas.is_some() && self.vizslas != other.vizslas.unwrap() {
            return false;
        }
        if other.goldfish.is_some() && self.goldfish != other.goldfish.unwrap() {
            return false;
        }
        if other.trees.is_some() && self.trees != other.trees.unwrap() {
            return false;
        }
        if other.cars.is_some() && self.cars != other.cars.unwrap() {
            return false;
        }
        if other.perfumes.is_some() && self.perfumes != other.perfumes.unwrap() {
            return false;
        }

        true
    }

    fn matches_part_2(&self, other: &SueInfo) -> bool {
        if other.children.is_some() && self.children != other.children.unwrap() {
            return false;
        }
        if other.cats.is_some() && self.cats >= other.cats.unwrap() {
            return false;
        }
        if other.samoyeds.is_some() && self.samoyeds != other.samoyeds.unwrap() {
            return false;
        }
        if other.pomeranians.is_some() && self.pomeranians <= other.pomeranians.unwrap() {
            return false;
        }
        if other.akitas.is_some() && self.akitas != other.akitas.unwrap() {
            return false;
        }
        if other.vizslas.is_some() && self.vizslas != other.vizslas.unwrap() {
            return false;
        }
        if other.goldfish.is_some() && self.goldfish <= other.goldfish.unwrap() {
            return false;
        }
        if other.trees.is_some() && self.trees >= other.trees.unwrap() {
            return false;
        }
        if other.cars.is_some() && self.cars != other.cars.unwrap() {
            return false;
        }
        if other.perfumes.is_some() && self.perfumes != other.perfumes.unwrap() {
            return false;
        }

        true
    }
}

impl Day for Day16 {
    fn initialize(&mut self, input: &str) {
        for line in input.lines() {
            let (_, line) = line.split_once(": ").unwrap();
            let args = line.split(", ");
            let mut sue = SueInfo::default();
            for arg in args {
                let (name, value) = arg.split_once(": ").unwrap();
                match name {
                    "children" => sue.children = Some(value.parse().unwrap()),
                    "cats" => sue.cats = Some(value.parse().unwrap()),
                    "samoyeds" => sue.samoyeds = Some(value.parse().unwrap()),
                    "pomeranians" => sue.pomeranians = Some(value.parse().unwrap()),
                    "akitas" => sue.akitas = Some(value.parse().unwrap()),
                    "vizslas" => sue.vizslas = Some(value.parse().unwrap()),
                    "goldfish" => sue.goldfish = Some(value.parse().unwrap()),
                    "trees" => sue.trees = Some(value.parse().unwrap()),
                    "cars" => sue.cars = Some(value.parse().unwrap()),
                    "perfumes" => sue.perfumes = Some(value.parse().unwrap()),
                    _ => panic!("malformed input"),
                }
            }
            self.infos.push(sue);
        }
    }

    fn part1(&mut self) -> Vec<String> {
        let correct_sue = SueInfoTrue {
            children: 3,
            cats: 7,
            samoyeds: 2,
            pomeranians: 3,
            akitas: 0,
            vizslas: 0,
            goldfish: 5,
            trees: 3,
            cars: 2,
            perfumes: 1,
        };
        let (index, _) = self
            .infos
            .iter()
            .enumerate()
            .find(|(_, info)| correct_sue.matches_part_1(info))
            .unwrap();
        output!(index + 1)
    }

    fn part2(&mut self) -> Vec<String> {
        let correct_sue = SueInfoTrue {
            children: 3,
            cats: 7,
            samoyeds: 2,
            pomeranians: 3,
            akitas: 0,
            vizslas: 0,
            goldfish: 5,
            trees: 3,
            cars: 2,
            perfumes: 1,
        };
        let (index, _) = self
            .infos
            .iter()
            .enumerate()
            .find(|(_, info)| correct_sue.matches_part_2(info))
            .unwrap();
        output!(index + 1)
    }

    fn name(&self) -> usize {
        16
    }
}
