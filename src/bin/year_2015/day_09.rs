use std::{collections::HashMap, vec};

use aoc::{output, Day, Permutations};

#[derive(Default)]
pub(crate) struct Day09 {
    cities: Vec<String>,
    distances: HashMap<(String, String), usize>,
    path_distances: Vec<usize>,
}

impl Day for Day09 {
    fn initialize(&mut self, input: &str) {
        let mut cities = Vec::new();
        let mut distances = HashMap::new();
        let lines = input.lines();
        for line in lines {
            let s = line.split(' ').collect::<Vec<_>>();
            let from = s[0].to_owned();
            let to = s[2].to_owned();
            let dist = s[4].parse().unwrap();
            if !cities.contains(&from) {
                cities.push(from.clone());
            }
            if !cities.contains(&to) {
                cities.push(to.clone());
            }
            distances.insert((from.clone(), to.clone()), dist);
            distances.insert((to.clone(), from.clone()), dist);
        }
        self.cities = cities;
        self.distances = distances;
    }

    fn part1(&mut self) -> Vec<String> {
        self.path_distances = self
            .cities
            .iter()
            .permutations()
            .map(|perm| {
                perm.windows(2)
                    .map(|win| {
                        *self
                            .distances
                            .get(&(win[0].clone(), win[1].clone()))
                            .unwrap()
                    })
                    .sum::<usize>()
            })
            .collect();
        output!(self.path_distances.iter().min().unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.path_distances.iter().max().unwrap())
    }

    fn name(&self) -> usize {
        9
    }
}
