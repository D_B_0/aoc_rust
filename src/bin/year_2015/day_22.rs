use aoc::{output, Day};

#[derive(Default, Clone, Debug)]
struct Boss {
    hit_points: usize,
    damage: usize,
}

#[derive(Clone, Debug)]
struct Player {
    hit_points: usize,
    mana: usize,
    armor: usize,
}

#[derive(Clone, Copy, Debug)]
enum Spell {
    MagicMissile,
    Drain,
    Shield,
    Poison,
    Recharge,
}

const SPELLS: &[Spell] = &[
    Spell::MagicMissile,
    Spell::Drain,
    Spell::Shield,
    Spell::Poison,
    Spell::Recharge,
];

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum EffectType {
    Shield,
    Poison,
    Recharge,
}

#[derive(Clone, Copy, Debug)]
struct Effect {
    e_type: EffectType,
    timer: usize,
}

#[derive(Debug, PartialEq)]
enum Success {
    Continue,
    PlayerWin,
    PlayerLose,
}

#[derive(Debug, PartialEq)]
enum Failure {
    CannotUseSpell,
    TooMuchMana,
}

#[derive(Clone, Debug)]
struct Play {
    player: Player,
    boss: Boss,
    spent_mana: usize,
    effects: Vec<Effect>,
}

impl Play {
    fn new(boss: Boss) -> Play {
        Play {
            player: Player {
                hit_points: 50,
                mana: 500,
                armor: 0,
            },
            //   player: Player {
            //     hit_points: 10,
            //     mana: 250,
            //     armor: 0,
            // },
            boss,
            spent_mana: 0,
            effects: vec![],
        }
    }

    fn _is_effect_active(&self, e_type: EffectType) -> bool {
        self.effects.iter().any(|e| e.e_type == e_type)
    }

    fn can_play_effect(&self, e_type: EffectType) -> bool {
        self.effects
            .iter()
            .any(|e| e.e_type == e_type && e.timer != 0)
    }

    fn can_use_spell(&self, spell: Spell) -> bool {
        match spell {
            Spell::MagicMissile => self.player.mana >= 53,
            Spell::Drain => self.player.mana >= 73,
            Spell::Shield => self.player.mana >= 113 && !self.can_play_effect(EffectType::Shield),
            Spell::Poison => self.player.mana >= 173 && !self.can_play_effect(EffectType::Poison),
            Spell::Recharge => {
                self.player.mana >= 229 && !self.can_play_effect(EffectType::Recharge)
            }
        }
    }

    fn use_spell(&mut self, spell: Spell) {
        assert!(self.can_use_spell(spell));
        // println!("Using spell {spell:?}");
        match spell {
            Spell::MagicMissile => {
                self.player.mana -= 53;
                self.spent_mana += 53;
                self.boss.hit_points = self.boss.hit_points.saturating_sub(4);
            }
            Spell::Drain => {
                self.player.mana -= 73;
                self.spent_mana += 73;
                self.boss.hit_points = self.boss.hit_points.saturating_sub(2);
                self.player.hit_points += 2;
            }
            Spell::Shield => {
                self.player.armor += 7;
                self.player.mana -= 113;
                self.spent_mana += 113;
                self.effects.push(Effect {
                    e_type: EffectType::Shield,
                    timer: 6,
                })
            }
            Spell::Poison => {
                self.player.mana -= 173;
                self.spent_mana += 173;
                self.effects.push(Effect {
                    e_type: EffectType::Poison,
                    timer: 6,
                })
            }
            Spell::Recharge => {
                self.player.mana -= 229;
                self.spent_mana += 229;
                self.effects.push(Effect {
                    e_type: EffectType::Recharge,
                    timer: 5,
                })
            }
        }
        // println!("New state: {self:#?}")
    }

    fn boss_turn(&mut self) {
        // println!("Boss turn");
        self.player.hit_points = self
            .player
            .hit_points
            .saturating_sub(self.boss.damage.saturating_sub(self.player.armor).max(1));
        // println!("New state: {self:#?}")
    }

    fn play_effects(&mut self) {
        if self
            .effects
            .iter()
            .any(|e| e.e_type == EffectType::Shield && e.timer == 0)
        {
            self.player.armor -= 7;
        }
        self.effects = self
            .effects
            .iter()
            .cloned()
            .filter(|effect| effect.timer != 0)
            .collect();
        for effect in &self.effects {
            match effect.e_type {
                EffectType::Shield => {}
                EffectType::Poison => self.boss.hit_points = self.boss.hit_points.saturating_sub(3),
                EffectType::Recharge => self.player.mana += 101,
            }
        }
        self.effects = self
            .effects
            .iter()
            .map(|effect| {
                let mut effect = *effect;
                effect.timer -= 1;
                effect
            })
            .collect();
    }

    fn play(
        &mut self,
        spell: Spell,
        max_mana_allowed: usize,
        hard_mode: bool,
    ) -> Result<Success, Failure> {
        if !self.can_use_spell(spell) {
            return Err(Failure::CannotUseSpell);
        }
        if hard_mode {
            self.player.hit_points = self.player.hit_points.saturating_sub(1);
            if self.player.hit_points == 0 {
                return Ok(Success::PlayerLose);
            }
        }
        self.play_effects();
        if self.boss.hit_points == 0 {
            return Ok(Success::PlayerWin);
        }
        self.use_spell(spell);
        if self.boss.hit_points == 0 {
            return Ok(Success::PlayerWin);
        }
        if self.spent_mana > max_mana_allowed {
            return Err(Failure::TooMuchMana);
        }
        self.play_effects();
        if self.boss.hit_points == 0 {
            return Ok(Success::PlayerWin);
        }
        self.boss_turn();
        if self.player.hit_points == 0 {
            return Ok(Success::PlayerLose);
        }
        if self.boss.hit_points == 0 {
            return Ok(Success::PlayerWin);
        }
        Ok(Success::Continue)
    }
}

#[derive(Default)]
pub(crate) struct Day22 {
    boss: Boss,
}

impl Day22 {
    fn find_win(start: Play, hard_mode: bool) -> Option<usize> {
        let mut plays = vec![start];
        let mut min_mana = usize::MAX;
        loop {
            let play = plays.pop().unwrap();
            for &spell in SPELLS {
                let mut new_play = play.clone();
                match new_play.play(spell, min_mana, hard_mode) {
                    Ok(Success::Continue) => {
                        plays.push(new_play);
                    }
                    Ok(Success::PlayerWin) => {
                        min_mana = min_mana.min(new_play.spent_mana);
                    }
                    Ok(Success::PlayerLose)
                    | Err(Failure::CannotUseSpell)
                    | Err(Failure::TooMuchMana) => {}
                }
            }
            if plays.is_empty() {
                break;
            }
        }
        if min_mana == usize::MAX {
            None
        } else {
            Some(min_mana)
        }
    }
}

impl Day for Day22 {
    fn initialize(&mut self, input: &str) {
        let mut stats = input
            .lines()
            .map(|l| l.split_once(": ").unwrap().1.parse::<usize>().unwrap());
        self.boss.hit_points = stats.next().unwrap();
        self.boss.damage = stats.next().unwrap();
        // self.boss.hit_points = 14;
        // self.boss.damage = 8;
    }

    fn part1(&mut self) -> Vec<String> {
        if let Some(part1) = Day22::find_win(Play::new(self.boss.clone()), false) {
            output!(part1)
        } else {
            output!("failed to solve :(")
        }
    }

    fn part2(&mut self) -> Vec<String> {
        if let Some(part2) = Day22::find_win(Play::new(self.boss.clone()), true) {
            output!(part2)
        } else {
            output!("failed to solve :(")
        }
    }

    fn name(&self) -> usize {
        22
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example() {
        let mut play = Play {
            player: Player {
                hit_points: 10,
                mana: 250,
                armor: 0,
            },
            boss: Boss {
                hit_points: 14,
                damage: 8,
            },
            spent_mana: 0,
            effects: vec![],
        };
        play.play_effects();
        play.use_spell(Spell::Recharge);
        assert_eq!(play.player.hit_points, 10);
        assert_eq!(play.player.mana, 21);
        assert_eq!(play.boss.hit_points, 14);
        play.play_effects();
        play.boss_turn();
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 122);
        assert_eq!(play.boss.hit_points, 14);
        play.play_effects();
        play.use_spell(Spell::Shield);
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 110);
        assert_eq!(play.boss.hit_points, 14);
        play.play_effects();
        play.boss_turn();
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 211);
        assert_eq!(play.boss.hit_points, 14);
        play.play_effects();
        play.use_spell(Spell::Drain);
        assert_eq!(play.player.hit_points, 3);
        assert_eq!(play.player.mana, 239);
        assert_eq!(play.boss.hit_points, 12);
        play.play_effects();
        play.boss_turn();
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 340);
        assert_eq!(play.boss.hit_points, 12);
        play.play_effects();
        play.use_spell(Spell::Poison);
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 167);
        assert_eq!(play.boss.hit_points, 12);
        play.play_effects();
        play.boss_turn();
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 167);
        assert_eq!(play.boss.hit_points, 9);
        play.play_effects();
        play.use_spell(Spell::MagicMissile);
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 114);
        assert_eq!(play.boss.hit_points, 2);
        play.play_effects();
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 114);
        assert_eq!(play.boss.hit_points, 0);

        assert_eq!(play.spent_mana, 641);
    }

    #[test]
    fn example_play() {
        let mut play = Play {
            player: Player {
                hit_points: 10,
                mana: 250,
                armor: 0,
            },
            boss: Boss {
                hit_points: 14,
                damage: 8,
            },
            spent_mana: 0,
            effects: vec![],
        };
        let res = play.play(Spell::Recharge, usize::MAX, false);
        assert_eq!(res, Ok(Success::Continue));
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 122);
        assert_eq!(play.boss.hit_points, 14);
        let res = play.play(Spell::Shield, usize::MAX, false);
        assert_eq!(res, Ok(Success::Continue));
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 211);
        assert_eq!(play.boss.hit_points, 14);
        let res = play.play(Spell::Drain, usize::MAX, false);
        assert_eq!(res, Ok(Success::Continue));
        assert_eq!(play.player.hit_points, 2);
        assert_eq!(play.player.mana, 340);
        assert_eq!(play.boss.hit_points, 12);
        let res = play.play(Spell::Poison, usize::MAX, false);
        assert_eq!(res, Ok(Success::Continue));
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 167);
        assert_eq!(play.boss.hit_points, 9);
        let res = play.play(Spell::MagicMissile, usize::MAX, false);
        assert_eq!(res, Ok(Success::PlayerWin));
        assert_eq!(play.player.hit_points, 1);
        assert_eq!(play.player.mana, 114);
        assert_eq!(play.boss.hit_points, 0);

        assert_eq!(play.spent_mana, 641);
    }

    #[test]
    fn find() {
        assert_eq!(
            Some(641),
            Day22::find_win(
                Play {
                    player: Player {
                        hit_points: 10,
                        mana: 250,
                        armor: 0,
                    },
                    boss: Boss {
                        hit_points: 14,
                        damage: 8,
                    },
                    spent_mana: 0,
                    effects: vec![],
                },
                false
            )
        )
    }
}
