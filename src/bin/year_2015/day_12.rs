use aoc::{output, Day};
use serde_json::Value;

#[derive(Default)]
pub(crate) struct Day12 {
    data: Vec<char>,
}

impl Day12 {
    fn count(json: Value) -> i32 {
        match json {
            Value::Null => 0,
            Value::Bool(_) => 0,
            Value::Number(n) => n.as_i64().unwrap() as i32,
            Value::String(_) => 0,
            Value::Array(vec) => vec.iter().map(|val| Day12::count(val.clone())).sum(),
            Value::Object(map) => {
                if map
                    .values()
                    .any(|val| matches!(val, Value::String(s) if s == "red"))
                {
                    0
                } else {
                    map.values().map(|val| Day12::count(val.clone())).sum()
                }
            }
        }
    }
}

enum CharType {
    Num(char),
    Sep,
}

impl Day for Day12 {
    fn initialize(&mut self, input: &str) {
        self.data = input.chars().collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let chars = self
            .data
            .iter()
            .map(|c| {
                if c.is_ascii_digit() || c == &'-' {
                    CharType::Num(*c)
                } else {
                    CharType::Sep
                }
            })
            .collect::<Vec<_>>();

        let mut sum = 0;
        let mut tmp_str = String::new();
        for c_type in chars {
            match c_type {
                CharType::Num(c) => tmp_str.push(c),
                CharType::Sep => {
                    if !tmp_str.is_empty() {
                        sum += tmp_str.parse::<i32>().unwrap();
                        tmp_str.clear();
                    }
                }
            }
        }

        output!(sum)
    }

    fn part2(&mut self) -> Vec<String> {
        let json: Value = serde_json::from_str(&self.data.iter().collect::<String>()).unwrap();

        output!(Day12::count(json))
    }

    fn name(&self) -> usize {
        12
    }
}
