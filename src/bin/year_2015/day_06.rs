use std::{str::FromStr, vec};

use aoc::{output, point::Point, Day};

#[derive(Debug)]
enum Command {
    On(Point<usize>, Point<usize>),
    Off(Point<usize>, Point<usize>),
    Toggle(Point<usize>, Point<usize>),
}

#[derive(Default)]
pub(crate) struct Day06 {
    commands: Vec<Command>,
}

impl Day for Day06 {
    fn initialize(&mut self, input: &str) {
        self.commands = input
            .lines()
            .map(|line| {
                let mut s = line.split_whitespace();
                match s.next().unwrap() {
                    "toggle" => {
                        let start = Point::<_>::from_str(s.next().unwrap()).unwrap();
                        s.next();
                        let end = Point::<_>::from_str(s.next().unwrap()).unwrap();
                        Command::Toggle(start, end)
                    }
                    "turn" => match s.next().unwrap() {
                        "on" => {
                            let start = Point::<_>::from_str(s.next().unwrap()).unwrap();
                            s.next();
                            let end = Point::<_>::from_str(s.next().unwrap()).unwrap();
                            Command::On(start, end)
                        }
                        "off" => {
                            let start = Point::<_>::from_str(s.next().unwrap()).unwrap();
                            s.next();
                            let end = Point::<_>::from_str(s.next().unwrap()).unwrap();
                            Command::Off(start, end)
                        }
                        _ => panic!("Invalid input"),
                    },
                    _ => panic!("Invalid input"),
                }
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut set = [[false; 1000]; 1000];
        for command in &self.commands {
            #[allow(clippy::needless_range_loop)]
            match command {
                Command::On(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            set[x][y] = true;
                        }
                    }
                }
                Command::Off(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            set[x][y] = false;
                        }
                    }
                }
                Command::Toggle(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            set[x][y] = !set[x][y];
                        }
                    }
                }
            }
        }
        output!(set
            .iter()
            .map(|row| row.iter().filter(|val| **val).count())
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut map = vec![[0usize; 1000]; 1000];
        for command in &self.commands {
            #[allow(clippy::needless_range_loop)]
            match command {
                Command::On(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            map[x][y] += 1;
                        }
                    }
                }
                Command::Off(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            map[x][y] = map[x][y].saturating_sub(1);
                        }
                    }
                }
                Command::Toggle(start, end) => {
                    for x in start.x..=end.x {
                        for y in start.y..=end.y {
                            map[x][y] += 2
                        }
                    }
                }
            }
        }
        output!(map
            .iter()
            .map(|row| row.iter().sum::<usize>())
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        6
    }
}
