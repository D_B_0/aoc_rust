use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day08 {
    list: Vec<String>,
}

impl Day for Day08 {
    fn initialize(&mut self, input: &str) {
        self.list = input.lines().map(str::to_owned).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .list
            .iter()
            .map(|s| {
                let chars = s.chars().collect::<Vec<_>>();
                let mut i = 1;
                let mut len = 0;
                while i < s.len() - 1 {
                    if chars[i] == '\\' {
                        if chars[i + 1] == '"' || chars[i + 1] == '\\' {
                            i += 2;
                        } else {
                            i += 4;
                        }
                        len += 1;
                        continue;
                    }
                    len += 1;
                    i += 1;
                }
                s.len() - len
            })
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .list
            .iter()
            .map(|s| {
                let len = s
                    .chars()
                    .map(|c| match c {
                        '\\' | '"' => 2,
                        _ => 1,
                    })
                    .sum::<usize>()
                    + 2;
                len - s.len()
            })
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        8
    }
}
