use aoc::{output, Day};

#[derive(Default)]
pub(crate) struct Day02 {
    data: Vec<Present>,
}

struct Present {
    sizes: [usize; 3],
}

impl Present {
    fn new(i_0: usize, i_1: usize, i_2: usize) -> Self {
        Self {
            sizes: [i_0, i_1, i_2],
        }
    }

    fn surface_area(&self) -> usize {
        2 * self.sizes[0] * self.sizes[1]
            + 2 * self.sizes[1] * self.sizes[2]
            + 2 * self.sizes[0] * self.sizes[2]
    }

    fn slack(&self) -> usize {
        self.sizes[0] * self.sizes[1]
    }

    fn ribbon(&self) -> usize {
        self.sizes[0] * 2 + self.sizes[1] * 2 + self.sizes[0] * self.sizes[1] * self.sizes[2]
    }
}

impl Day for Day02 {
    fn initialize(&mut self, input: &str) {
        self.data = input
            .lines()
            .map(|l| {
                let mut v = l
                    .split('x')
                    .map(|s| s.parse::<usize>().unwrap())
                    .collect::<Vec<_>>();
                v.sort();
                Present::new(v[0], v[1], v[2])
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .data
            .iter()
            .map(|p| p.surface_area() + p.slack())
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.data.iter().map(|p| p.ribbon()).sum::<usize>())
    }

    fn name(&self) -> usize {
        2
    }
}
