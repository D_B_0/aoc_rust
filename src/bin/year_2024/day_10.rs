use itertools::iproduct;

use aoc::{output, Day, Grid};

fn get_neighbors(u: (usize, usize), map: &Grid<u8>) -> Vec<(usize, usize)> {
    let mut neighbors = Vec::new();
    if u.0 != 0
        && map
            .get(u.1, u.0 - 1)
            .unwrap()
            .checked_sub(*map.get(u.1, u.0).unwrap())
            == Some(1)
    {
        neighbors.push((u.0 - 1, u.1));
    }
    if u.0 < map.width() - 1
        && map
            .get(u.1, u.0 + 1)
            .unwrap()
            .checked_sub(*map.get(u.1, u.0).unwrap())
            == Some(1)
    {
        neighbors.push((u.0 + 1, u.1));
    }
    if u.1 != 0
        && map
            .get(u.1 - 1, u.0)
            .unwrap()
            .checked_sub(*map.get(u.1, u.0).unwrap())
            == Some(1)
    {
        neighbors.push((u.0, u.1 - 1));
    }
    if u.1 < map.height() - 1
        && map
            .get(u.1 + 1, u.0)
            .unwrap()
            .checked_sub(*map.get(u.1, u.0).unwrap())
            == Some(1)
    {
        neighbors.push((u.0, u.1 + 1));
    }
    neighbors
}

fn count_trails(map: &Grid<u8>, start: (usize, usize), targets: &[(usize, usize)]) -> usize {
    targets
        .iter()
        .filter(|target| depth_first_wrapper(map, start, **target) > 0)
        .count()
}

fn depth_first(
    map: &Grid<u8>,
    visited: &mut Vec<(usize, usize)>,
    target: (usize, usize),
    paths: &mut usize,
) {
    let neighbors = get_neighbors(*visited.last().unwrap(), map);
    for neighbor in &neighbors {
        if visited.contains(neighbor) {
            continue;
        }
        if neighbor == &target {
            *paths += 1;
            break;
        }
    }
    for neighbor in &neighbors {
        if visited.contains(neighbor) || neighbor == &target {
            continue;
        }
        visited.push(*neighbor);
        depth_first(map, visited, target, paths);
        visited.pop();
    }
}

fn depth_first_wrapper(map: &Grid<u8>, start: (usize, usize), target: (usize, usize)) -> usize {
    let mut paths = 0;
    depth_first(map, &mut vec![start], target, &mut paths);
    paths
}

fn count_distinct_trails(
    map: &Grid<u8>,
    starts: &[(usize, usize)],
    targets: &[(usize, usize)],
) -> usize {
    starts
        .iter()
        .map(|start| {
            targets
                .iter()
                .map(|target| depth_first_wrapper(map, *start, *target))
                .sum::<usize>()
        })
        .sum()
}

#[derive(Default)]
pub struct Day10 {
    map: Grid<u8>,
    trailheads: Vec<(usize, usize)>,
    peaks: Vec<(usize, usize)>,
}

impl Day for Day10 {
    fn initialize(&mut self, input: &str) {
        self.map = Grid::from_iter(
            input
                .trim()
                .lines()
                .map(|line| line.chars().map(|ch| ch.to_digit(10).unwrap() as u8)),
        );
        for (x, y) in iproduct!(0..self.map.width(), 0..self.map.height()) {
            if self.map.get(y, x) == Some(&0) {
                self.trailheads.push((x, y));
            }
        }
        for (x, y) in iproduct!(0..self.map.width(), 0..self.map.height()) {
            if self.map.get(y, x) == Some(&9) {
                self.peaks.push((x, y));
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .trailheads
            .iter()
            .map(|trailhead| count_trails(&self.map, *trailhead, &self.peaks))
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(count_distinct_trails(
            &self.map,
            &self.trailheads,
            &self.peaks
        ))
    }

    fn name(&self) -> usize {
        10
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day10::default();
        day.initialize(
            "89010123\n78121874\n87430965\n96549874\n45678903\n32019012\n01329801\n10456732\n",
        );
        assert_eq!(day.part1(), vec!["36".to_owned()]);
        assert_eq!(day.part2(), vec!["81".to_owned()]);
    }
}
