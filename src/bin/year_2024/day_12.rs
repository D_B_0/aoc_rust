use aoc::{output, Day, Grid};
use itertools::iproduct;

fn floodfill(grid: &Grid<char>, start: (usize, usize)) -> Vec<(usize, usize)> {
    let mut ret = Vec::new();
    let mut to_visit = vec![start];
    while let Some(current) = to_visit.pop() {
        ret.push(current);
        for (dx, dy) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            let Some((new_x, new_y)) = current
                .0
                .checked_add_signed(dx)
                .zip(current.1.checked_add_signed(dy))
            else {
                continue;
            };
            if ret.contains(&(new_x, new_y)) || to_visit.contains(&(new_x, new_y)) {
                continue;
            }
            if grid.get(new_y, new_x) == grid.get(current.1, current.0) {
                to_visit.push((new_x, new_y));
            }
        }
    }
    ret
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Range {
    lower: usize,
    upper: usize,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct LayerBetween {
    lower_layer: isize,
    upper_layer: isize,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Side {
    range: Range,
    layer: LayerBetween,
}

#[derive(Default)]
pub struct Day12 {
    plots: Grid<char>,
    partitions: Vec<Vec<(usize, usize)>>,
}

impl Day12 {
    fn partition_plots(&self) -> Vec<Vec<(usize, usize)>> {
        let mut partitions: Vec<Vec<(usize, usize)>> = Vec::new();
        for y in 0..self.plots.height() {
            for x in 0..self.plots.width() {
                if partitions
                    .iter()
                    .any(|partition| partition.iter().any(|pos| *pos == (x, y)))
                {
                    continue;
                }
                partitions.push(floodfill(&self.plots, (x, y)));
            }
        }
        partitions
    }
}

fn calc_perimeter(partition: &[(usize, usize)]) -> usize {
    let mut count = 0;
    for current in partition {
        for (dx, dy) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            let Some((new_x, new_y)) = current
                .0
                .checked_add_signed(dx)
                .zip(current.1.checked_add_signed(dy))
            else {
                count += 1;
                continue;
            };
            if !partition.contains(&(new_x, new_y)) {
                count += 1;
            }
        }
    }
    count
}

fn calc_num_sides(partition: &[(usize, usize)]) -> usize {
    let mut vertical_sides = Vec::new();
    let mut horizontal_sides = Vec::new();
    for current in partition {
        for (dx, dy) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            let Some((new_x, new_y)) = current
                .0
                .checked_add_signed(dx)
                .zip(current.1.checked_add_signed(dy))
            else {
                if dx == 0 {
                    // horizontal side
                    extend_or_insert_side(
                        &mut horizontal_sides,
                        current.0.checked_add_signed(dx).unwrap(),
                        LayerBetween {
                            lower_layer: -1,
                            upper_layer: 0,
                        },
                    );
                    compat_sides(&mut horizontal_sides);
                } else {
                    // vertical side
                    extend_or_insert_side(
                        &mut vertical_sides,
                        current.1.checked_add_signed(dy).unwrap(),
                        LayerBetween {
                            lower_layer: -1,
                            upper_layer: 0,
                        },
                    );
                    compat_sides(&mut vertical_sides);
                }
                continue;
            };
            if !partition.contains(&(new_x, new_y)) {
                if dx == 0 {
                    // horizontal side
                    extend_or_insert_side(
                        &mut horizontal_sides,
                        new_x,
                        LayerBetween {
                            lower_layer: current.1 as isize,
                            upper_layer: new_y as isize,
                        },
                    );
                    compat_sides(&mut horizontal_sides);
                } else {
                    // vertical side
                    extend_or_insert_side(
                        &mut vertical_sides,
                        new_y,
                        LayerBetween {
                            lower_layer: current.0 as isize,
                            upper_layer: new_x as isize,
                        },
                    );
                    compat_sides(&mut vertical_sides);
                }
            }
        }
    }
    vertical_sides.len() + horizontal_sides.len()
}

fn extend_or_insert_side(sides: &mut Vec<Side>, extend_to: usize, layer: LayerBetween) {
    if let Some(side) = sides.iter_mut().find(|side| {
        side.layer == layer
            && ((extend_to < side.range.lower && side.range.lower - extend_to == 1)
                || (extend_to > side.range.upper && extend_to - side.range.upper == 1))
    }) {
        if extend_to < side.range.lower && side.range.lower - extend_to == 1 {
            side.range.lower = extend_to;
        } else if extend_to > side.range.upper && extend_to - side.range.upper == 1 {
            side.range.upper = extend_to;
        }
    } else {
        sides.push(Side {
            range: Range {
                lower: extend_to,
                upper: extend_to,
            },
            layer,
        });
    }
}

fn compat_sides(sides: &mut Vec<Side>) {
    let mut merged = true;
    while merged {
        merged = false;
        for (i, j) in iproduct!(0..sides.len(), 0..sides.len()) {
            if i == j {
                continue;
            }
            if sides[i].layer != sides[j].layer {
                continue;
            }
            if sides[i].range.lower.abs_diff(sides[j].range.upper) <= 1 {
                sides[i].range.lower = sides[j].range.lower;
                sides.remove(j);
                merged = true;
                break;
            }
            if sides[i].range.upper.abs_diff(sides[j].range.lower) <= 1 {
                sides[i].range.upper = sides[j].range.upper;
                sides.remove(j);
                merged = true;
                break;
            }
        }
    }
}

fn calc_price(partition: &Vec<(usize, usize)>) -> usize {
    partition.len() * calc_perimeter(partition)
}

fn calc_bulk_price(partition: &Vec<(usize, usize)>) -> usize {
    partition.len() * calc_num_sides(partition)
}

impl Day for Day12 {
    fn initialize(&mut self, input: &str) {
        self.plots = Grid::from_iter(input.trim().lines().map(|line| line.chars()));
        self.partitions = self.partition_plots();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.partitions.iter().map(calc_price).sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.partitions.iter().map(calc_bulk_price).sum::<usize>())
    }

    fn name(&self) -> usize {
        12
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day12::default();
        day.initialize("RRRRIICCFF\nRRRRIICCCF\nVVRRRCCFFF\nVVRCCCJFFF\nVVVVCJJCFE\nVVIVCCJJEE\nVVIIICJJEE\nMIIIIIJJEE\nMIIISIJEEE\nMMMISSJEEE\n");
        assert_eq!(day.part1(), vec!["1930".to_owned()]);
        assert_eq!(day.part2(), vec!["1206".to_owned()]);
        let mut day = Day12::default();
        day.initialize("EEEEE\nEXXXX\nEEEEE\nEXXXX\nEEEEE\n");
        assert_eq!(day.part2(), vec!["236".to_owned()]);
        println!("=================");
        let mut day = Day12::default();
        day.initialize("AAAAAA\nAAABBA\nAAABBA\nABBAAA\nABBAAA\nAAAAAA\n");
        assert_eq!(day.part2(), vec!["368".to_owned()]);
    }
}
