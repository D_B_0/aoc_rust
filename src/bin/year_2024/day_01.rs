use std::collections::HashMap;

use aoc::{output, Day};

#[derive(Default)]
pub struct Day01 {
    list1: Vec<i32>,
    list2: Vec<i32>,
}

impl Day for Day01 {
    fn initialize(&mut self, input: &str) {
        let _ = input
            .trim()
            .lines()
            .map(|line| {
                let mut line = line.split_whitespace();
                (line.next().unwrap(), line.next().unwrap())
            })
            .map(|(n1, n2)| (n1.parse::<i32>().unwrap(), n2.parse::<i32>().unwrap()))
            .fold(
                (&mut self.list1, &mut self.list2),
                |(list1, list2), (n1, n2)| {
                    list1.push(n1);
                    list2.push(n2);
                    (list1, list2)
                },
            );
    }

    fn part1(&mut self) -> Vec<String> {
        self.list1.sort();
        self.list2.sort();
        output!(self
            .list1
            .iter()
            .zip(self.list2.iter())
            .map(|(n1, n2)| (n1 - n2).abs())
            .sum::<i32>())
    }

    fn part2(&mut self) -> Vec<String> {
        let occurences = self
            .list2
            .iter()
            .fold(HashMap::<i32, usize>::new(), |mut map, n2| {
                *map.entry(*n2).or_insert(0) += 1;
                map
            });
        output!(self
            .list1
            .iter()
            .map(|n1| *occurences.get(n1).unwrap_or(&0) * *n1 as usize)
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        1
    }
}
