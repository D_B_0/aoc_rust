use std::str::FromStr;

use aoc::{output, Day};
use glam::I64Vec2;
use num::Integer;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct ClawMachine {
    prize: I64Vec2,
    claw_a: I64Vec2,
    claw_b: I64Vec2,
}

fn int_divide(num: i64, den: i64) -> Option<i64> {
    if num.abs() < den.abs() || num.gcd(&den) != den.abs() {
        return None;
    }
    Some(num / den)
}

impl ClawMachine {
    fn calculate_min_price(&self) -> Option<i64> {
        // A*a + B*b = p

        //       |
        //       v

        // B=(a^p)/(a^b)
        // A=(p_x-B*b_x)/a_x

        let b = int_divide(
            self.claw_a.perp_dot(self.prize),
            self.claw_a.perp_dot(self.claw_b),
        )?;
        let a = int_divide(self.prize.x - b * self.claw_b.x, self.claw_a.x)?;

        Some(3 * a + b)
    }
}

fn claw_machine_parse_line_helper(
    line: &str,
    prefix: &str,
    x_prefix: &str,
    y_prefix: &str,
) -> Result<I64Vec2, ()> {
    let (x, y) = line
        .strip_prefix(prefix)
        .ok_or(())?
        .split_once(", ")
        .ok_or(())?;
    Ok(I64Vec2::new(
        x.strip_prefix(x_prefix)
            .ok_or(())?
            .parse()
            .map_err(|_| ())?,
        y.strip_prefix(y_prefix)
            .ok_or(())?
            .parse()
            .map_err(|_| ())?,
    ))
}

impl FromStr for ClawMachine {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        Ok(ClawMachine {
            claw_a: claw_machine_parse_line_helper(
                lines.next().ok_or(())?,
                "Button A: ",
                "X+",
                "Y+",
            )?,
            claw_b: claw_machine_parse_line_helper(
                lines.next().ok_or(())?,
                "Button B: ",
                "X+",
                "Y+",
            )?,
            prize: claw_machine_parse_line_helper(lines.next().ok_or(())?, "Prize: ", "X=", "Y=")?,
        })
    }
}

#[derive(Default)]
pub struct Day13 {
    claw_machines: Vec<ClawMachine>,
}

impl Day for Day13 {
    fn initialize(&mut self, input: &str) {
        self.claw_machines = input
            .trim()
            .split("\n\n")
            .map(str::parse::<ClawMachine>)
            .map(Result::unwrap)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .claw_machines
            .iter()
            .filter_map(ClawMachine::calculate_min_price)
            .sum::<i64>())
    }

    fn part2(&mut self) -> Vec<String> {
        for claw_machine in self.claw_machines.iter_mut() {
            claw_machine.prize += I64Vec2::new(10000000000000, 10000000000000);
        }
        output!(self
            .claw_machines
            .iter()
            .filter_map(ClawMachine::calculate_min_price)
            .sum::<i64>())
    }

    fn name(&self) -> usize {
        13
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day13::default();
        day.initialize("Button A: X+94, Y+34\nButton B: X+22, Y+67\nPrize: X=8400, Y=5400\n\nButton A: X+26, Y+66\nButton B: X+67, Y+21\nPrize: X=12748, Y=12176\n\nButton A: X+17, Y+86\nButton B: X+84, Y+37\nPrize: X=7870, Y=6450\n\nButton A: X+69, Y+23\nButton B: X+27, Y+71\nPrize: X=18641, Y=10279\n");
        assert_eq!(day.part1(), vec!["480".to_owned()]);
        assert_eq!(day.part2(), vec!["875318608908".to_owned()]);
    }
}
