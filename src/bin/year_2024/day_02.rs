use aoc::{output, Day};

#[derive(Default)]
pub struct Day02 {
    reports: Vec<Vec<i32>>,
}

fn check_report(report: &&Vec<i32>) -> bool {
    let diffs = report.windows(2).map(|a| a[0] - a[1]);
    diffs.clone().all(|diff| (1..=3).contains(&diff.abs()))
        && (diffs.clone().all(|diff| diff > 0) || diffs.clone().all(|diff| diff < 0))
}

impl Day for Day02 {
    fn initialize(&mut self, input: &str) {
        self.reports = input
            .trim()
            .lines()
            .map(|line| {
                line.split_whitespace()
                    .map(str::parse::<i32>)
                    .collect::<Result<Vec<_>, _>>()
                    .unwrap()
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.reports.iter().filter(check_report).count())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .reports
            .iter()
            .filter(|report| {
                if check_report(report) {
                    return true;
                }
                for i in 0..report.len() {
                    let mut new_report = (*report).clone();
                    new_report.remove(i);
                    if check_report(&&new_report) {
                        return true;
                    }
                }
                false
            })
            .count())
    }

    fn name(&self) -> usize {
        2
    }
}
