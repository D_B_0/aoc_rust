use std::collections::{HashMap, HashSet};

use aoc::{output, Day};
use glam::IVec2;
use itertools::iproduct;

const DIRECTIONS: [IVec2; 4] = [
    IVec2::new(-1, 0),
    IVec2::new(1, 0),
    IVec2::new(0, -1),
    IVec2::new(0, 1),
];

fn successors(walls: &HashSet<IVec2>) -> impl Fn(&IVec2) -> Vec<(IVec2, u32)> + use<'_> {
    move |pos| {
        DIRECTIONS
            .iter()
            .filter_map(|dir| {
                if !walls.contains(&(pos + dir)) {
                    Some(((pos + dir), 1))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
    }
}

#[derive(Default, Debug)]
pub struct Day20 {
    start: IVec2,
    end: IVec2,
    walls: HashSet<IVec2>,
    start_distance_map: HashMap<IVec2, u32>,
    end_distance_map: HashMap<IVec2, u32>,
}

impl Day20 {
    fn shortcuts(&self, length: u32) -> Vec<(IVec2, IVec2, u32)> {
        let dims = self.walls.iter().cloned().reduce(IVec2::max).unwrap();
        let tracks = iproduct!(0..dims.x, 0..dims.y)
            .map(|(x, y)| IVec2::new(x, y))
            .filter(|pos| !self.walls.contains(pos))
            .collect::<HashSet<_>>();
        let mut ret = vec![];
        for start in &tracks {
            for end in &tracks {
                let diff = (start - end).abs();
                let dist = (diff.x + diff.y) as u32;
                if dist > 1 && dist <= length {
                    ret.push((*start, *end, (diff.x + diff.y) as u32));
                }
            }
        }
        ret
    }

    fn count_good_shortcuts(&mut self, length: u32) -> usize {
        let base_time = *self.start_distance_map.get(&self.end).unwrap();
        self.shortcuts(length)
            .into_iter()
            .filter(move |(start, end, length)| {
                let time = self
                    .start_distance_map
                    .get(start)
                    .zip(self.end_distance_map.get(end))
                    .map(|(time1, time2)| time1 + time2 + length)
                    .unwrap_or(base_time);
                base_time >= time + 100
            })
            .count()
    }
}

impl Day for Day20 {
    fn initialize(&mut self, input: &str) {
        for (x, y, ch) in input
            .trim()
            .lines()
            .enumerate()
            .flat_map(|(y, line)| line.char_indices().map(move |(x, ch)| (x, y, ch)))
        {
            let pos = IVec2::new(x as i32, y as i32);
            match ch {
                'S' => self.start = pos,
                'E' => self.end = pos,
                '#' => {
                    self.walls.insert(pos);
                }
                _ => {}
            }
        }
        self.start_distance_map =
            pathfinding::directed::dijkstra::dijkstra_all(&self.start, successors(&self.walls))
                .into_iter()
                .map(|(key, (_, val))| (key, val))
                .collect();
        self.start_distance_map.insert(self.start, 0);
        self.end_distance_map =
            pathfinding::directed::dijkstra::dijkstra_all(&self.end, successors(&self.walls))
                .into_iter()
                .map(|(key, (_, val))| (key, val))
                .collect();
        self.end_distance_map.insert(self.end, 0);
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.count_good_shortcuts(2))
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.count_good_shortcuts(20))
    }

    fn name(&self) -> usize {
        20
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day20::default();
        day.initialize("###############\n#...#...#.....#\n#.#.#.#.#.###.#\n#S#...#.#.#...#\n#######.#.#.###\n#######.#.#...#\n#######.#.###.#\n###..E#...#...#\n###.#######.###\n#...###...#...#\n#.#####.#.###.#\n#.#...#.#.#...#\n#.#.#.#.#.#.###\n#...#...#...###\n###############\n");
        assert_eq!(day.part1(), vec!["0".to_owned()]);
        assert_eq!(day.part2(), vec!["0".to_owned()]);
    }
}
