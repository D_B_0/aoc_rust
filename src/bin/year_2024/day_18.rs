use std::collections::HashSet;

use aoc::{output, Day};
use glam::IVec2;

#[derive(Default, Debug)]
pub struct Day18 {
    bytes: Vec<IVec2>,
    dims: IVec2,
    part_1_config: usize,
}

impl Day18 {
    fn get_path(&self, last_byte_to_include: usize) -> Option<(Vec<IVec2>, i32)> {
        let corrupted = self.bytes[0..=last_byte_to_include]
            .iter()
            .collect::<HashSet<_>>();
        pathfinding::directed::astar::astar(
            &IVec2::new(0, 0),
            |pos| {
                let mut v = vec![];
                for d in [
                    IVec2::new(1, 0),
                    IVec2::new(-1, 0),
                    IVec2::new(0, 1),
                    IVec2::new(0, -1),
                ] {
                    let new_pos = pos + d;
                    if !corrupted.contains(&new_pos)
                        && (0..=self.dims.x).contains(&new_pos.x)
                        && (0..=self.dims.y).contains(&new_pos.y)
                    {
                        v.push((new_pos, 1));
                    }
                }
                v
            },
            |pos| {
                let diff = (pos - self.dims).abs();
                diff.x + diff.y
            },
            |&pos| pos == self.dims,
        )
    }
}

impl Day for Day18 {
    fn initialize(&mut self, input: &str) {
        self.bytes = input
            .trim()
            .lines()
            .map(|line| {
                let (left, right) = line.split_once(',').unwrap();
                IVec2::new(left.parse().unwrap(), right.parse().unwrap())
            })
            .collect();
        self.dims = self.bytes.iter().cloned().reduce(IVec2::max).unwrap();
        self.part_1_config = 1024;
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.get_path(self.part_1_config - 1).unwrap().1)
    }

    fn part2(&mut self) -> Vec<String> {
        let pos = self.bytes[(0..self.bytes.len())
            .find(|&i| self.get_path(i).is_none())
            .unwrap()];
        output!(format!("{},{}", pos.x, pos.y))
    }

    fn name(&self) -> usize {
        18
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day18::default();
        day.initialize("5,4\n4,2\n4,5\n3,0\n2,1\n6,3\n2,4\n1,5\n0,6\n3,3\n2,6\n5,1\n1,2\n5,5\n2,5\n6,5\n1,4\n0,4\n6,4\n1,1\n6,1\n1,0\n0,5\n1,6\n2,0");
        day.part_1_config = 12;
        assert_eq!(day.part1(), vec!["22".to_owned()]);
        assert_eq!(day.part2(), vec!["6,1".to_owned()]);
    }
}
