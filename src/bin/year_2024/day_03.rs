use aoc::{output, Day};

#[derive(Default)]
pub struct Day03 {
    input: String,
}

impl Day for Day03 {
    fn initialize(&mut self, input: &str) {
        self.input = input.to_owned();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut input: &str = &self.input;
        let mut res = 0;
        while let Some(pos) = input.find("mul(") {
            input = &input[pos + 4..];
            let Some((arguments, _)) = input.split_once(')') else {
                continue;
            };
            let Some((first, second)) = arguments.split_once(',') else {
                continue;
            };
            if first.len() > 3
                || first.is_empty()
                || second.len() > 3
                || second.is_empty()
                || !first.chars().all(|c| c.is_ascii_digit())
                || !first.chars().all(|c| c.is_ascii_digit())
            {
                continue;
            }
            res += first.parse::<u32>().unwrap() * second.parse::<u32>().unwrap();
        }
        output!(res)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut input: &str = &self.input;
        let mut res = 0;
        let mut enabled = true;
        loop {
            let mul_pos = input.find("mul(").unwrap_or(usize::MAX);
            let do_pos = input.find("do()").unwrap_or(usize::MAX);
            let dont_pos = input.find("don't()").unwrap_or(usize::MAX);
            if do_pos < mul_pos && do_pos < dont_pos {
                input = &input[do_pos + 4..];
                enabled = true;
                continue;
            }
            if dont_pos < mul_pos {
                input = &input[dont_pos + 7..];
                enabled = false;
                continue;
            }
            if mul_pos == usize::MAX {
                break;
            }
            input = &input[mul_pos + 4..];
            if !enabled {
                continue;
            }
            let Some((arguments, _)) = input.split_once(')') else {
                continue;
            };
            let Some((first, second)) = arguments.split_once(',') else {
                continue;
            };
            if first.len() > 3
                || first.is_empty()
                || second.len() > 3
                || second.is_empty()
                || !first.chars().all(|c| c.is_ascii_digit())
                || !second.chars().all(|c| c.is_ascii_digit())
            {
                continue;
            }
            res += first.parse::<u32>().unwrap() * second.parse::<u32>().unwrap();
        }
        output!(res)
    }

    fn name(&self) -> usize {
        3
    }
}
