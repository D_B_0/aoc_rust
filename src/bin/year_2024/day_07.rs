use aoc::{output, Day};

fn n_dimentional_grid(n: usize, max: usize) -> Vec<Vec<usize>> {
    let mut ret = Vec::new();
    loop {
        let mut row = ret.last().unwrap_or(&vec![0].repeat(n)).clone();
        if row.iter().all(|&c| c == max) {
            return ret;
        }
        for i in (0..row.len()).rev() {
            if row[i] == max {
                row[i] = 0;
            } else {
                row[i] += 1;
                break;
            }
        }
        ret.push(row);
    }
}

fn is_equation_satisfiable(
    (result, operands): &&(usize, Vec<usize>),
    operators: &[fn(usize, usize) -> usize],
) -> bool {
    'choice_loop: for choices in n_dimentional_grid(operands.len(), operators.len() - 1) {
        let mut res = operands[0];
        for (i, operand) in operands.iter().skip(1).enumerate() {
            res = operators[choices[i]](res, *operand);
            if res > *result {
                // ASSUMPTION: all the operators are strictly increasing
                // so if we have already overshot the target, so we don't
                // need to contiune this combination of operators.
                continue 'choice_loop;
            }
        }
        if res == *result {
            return true;
        }
    }
    false
}

#[memoize::memoize]
fn concatenation(mut lhs: usize, rhs: usize) -> usize {
    lhs *= 10usize.pow(format!("{rhs}").len() as u32);
    lhs + rhs
}

#[derive(Default)]
pub struct Day07 {
    equations: Vec<(usize, Vec<usize>)>,
}

impl Day for Day07 {
    fn initialize(&mut self, input: &str) {
        self.equations = input
            .trim()
            .lines()
            .map(|line| {
                let (first, rest) = line.split_once(": ").unwrap();
                (
                    first.parse().unwrap(),
                    rest.split(' ').map(|num| num.parse().unwrap()).collect(),
                )
            })
            .collect()
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .equations
            .iter()
            .filter(|arg| is_equation_satisfiable(
                arg,
                &[usize::saturating_add, usize::saturating_mul]
            ))
            .map(|(res, _)| res)
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .equations
            .iter()
            .filter(|arg| is_equation_satisfiable(
                arg,
                &[usize::saturating_add, usize::saturating_mul, concatenation]
            ))
            .map(|(res, _)| res)
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        7
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day07::default();
        day.initialize("190: 10 19\n3267: 81 40 27\n83: 17 5\n156: 15 6\n7290: 6 8 6 15\n161011: 16 10 13\n192: 17 8 14\n21037: 9 7 18 13\n292: 11 6 16 20\n");
        assert_eq!(day.part1(), vec!["3749".to_owned()]);
        assert_eq!(day.part2(), vec!["11387".to_owned()]);
    }

    #[test]
    fn concat() {
        assert_eq!(concatenation(12, 13), 1213);
        assert_eq!(concatenation(123456789, 123456789), 123456789123456789);
    }
}
