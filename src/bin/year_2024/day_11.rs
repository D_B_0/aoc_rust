use std::collections::HashMap;

use aoc::{output, Day};

#[derive(Default)]
pub struct Day11 {
    stones: HashMap<usize, usize>,
}

impl Day11 {
    fn blink(&mut self) {
        let mut new_stones = HashMap::new();
        for (stone, &count) in &self.stones {
            if *stone == 0 {
                *new_stones.entry(1).or_default() += count;
                continue;
            }
            let stone_str = format!("{}", stone);
            if stone_str.len() % 2 == 0 {
                let left = &stone_str[..stone_str.len() / 2];
                let right = &stone_str[stone_str.len() / 2..];
                *new_stones.entry(left.parse().unwrap()).or_default() += count;
                *new_stones.entry(right.parse().unwrap()).or_default() += count;
                continue;
            }
            *new_stones.entry(stone * 2024).or_default() += count;
        }
        self.stones = new_stones;
    }
}

impl Day for Day11 {
    fn initialize(&mut self, input: &str) {
        input
            .trim()
            .split_whitespace()
            .map(|stone| stone.parse().unwrap())
            .for_each(|stone| *self.stones.entry(stone).or_insert(0) += 1);
    }

    fn part1(&mut self) -> Vec<String> {
        for _ in 0..25 {
            self.blink();
        }
        output!(self.stones.iter().map(|(_, count)| count).sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        for _ in 0..50 {
            self.blink();
        }
        output!(self.stones.iter().map(|(_, count)| count).sum::<usize>())
    }

    fn name(&self) -> usize {
        11
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day11::default();
        day.initialize("125 17");
        assert_eq!(day.part1(), vec!["55312".to_owned()]);
        assert_eq!(day.part2(), vec!["65601038650482".to_owned()]);
    }
}
