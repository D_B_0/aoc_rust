use glam::IVec2;
use std::str::FromStr;

use aoc::{output, Day};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Bot {
    pos: IVec2,
    vel: IVec2,
}

impl FromStr for Bot {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (pos, vel) = s.split_once(' ').ok_or(())?;
        let pos = pos.strip_prefix("p=").ok_or(())?;
        let vel = vel.strip_prefix("v=").ok_or(())?;
        let (px, py) = pos.split_once(',').ok_or(())?;
        let (vx, vy) = vel.split_once(',').ok_or(())?;
        Ok(Bot {
            pos: IVec2::new(px.parse().map_err(|_| ())?, py.parse().map_err(|_| ())?),
            vel: IVec2::new(vx.parse().map_err(|_| ())?, vy.parse().map_err(|_| ())?),
        })
    }
}

impl Bot {
    fn pos_after(&self, time: usize, width: usize, height: usize) -> IVec2 {
        (self.pos + time as i32 * self.vel).rem_euclid(IVec2::new(width as i32, height as i32))
    }
}

#[derive(Default)]
pub struct Day14 {
    bots: Vec<Bot>,
    width: usize,
    height: usize,
}

impl Day14 {
    fn count_quadrants(&self, mut counts: [usize; 4], pos: IVec2) -> [usize; 4] {
        if pos.x < (self.width as i32 - 1) / 2 {
            if pos.y < (self.height as i32 - 1) / 2 {
                counts[0] += 1;
            } else if pos.y > (self.height as i32 - 1) / 2 {
                counts[1] += 1;
            }
        } else if pos.x > (self.width as i32 - 1) / 2 {
            if pos.y < (self.height as i32 - 1) / 2 {
                counts[2] += 1;
            } else if pos.y > (self.height as i32 - 1) / 2 {
                counts[3] += 1;
            }
        }
        counts
    }
}

impl Day for Day14 {
    fn initialize(&mut self, input: &str) {
        self.bots = input
            .trim()
            .lines()
            .map(str::parse)
            .collect::<Result<_, _>>()
            .unwrap();
        self.width = 101;
        self.height = 103;
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .bots
            .iter()
            .map(|bot| bot.pos_after(100, self.width, self.height))
            .fold([0; 4], |counts, pos| self.count_quadrants(counts, pos))
            .iter()
            .product::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(
            // ASSUMPTION: width and height are prime and different
            (0..self.width * self.height)
                .map(|sec| (
                    sec,
                    self.bots
                        .iter()
                        .map(|bot| bot.pos_after(sec, self.width, self.height))
                        .fold([0; 4], |counts, pos| self.count_quadrants(counts, pos))
                        .iter()
                        .product::<usize>()
                ))
                .min_by_key(|(_, danger)| *danger)
                .unwrap()
                .0
        )
    }

    fn name(&self) -> usize {
        14
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day14::default();
        day.initialize("p=0,4 v=3,-3\np=6,3 v=-1,-3\np=10,3 v=-1,2\np=2,0 v=2,-1\np=0,0 v=1,3\np=3,0 v=-2,-2\np=7,6 v=-1,-3\np=3,0 v=-1,-2\np=9,3 v=2,3\np=7,3 v=-1,2\np=2,4 v=2,-3\np=9,5 v=-3,-3\n");
        day.width = 11;
        day.height = 7;
        assert_eq!(day.part1(), vec!["12".to_owned()]);
    }
}
