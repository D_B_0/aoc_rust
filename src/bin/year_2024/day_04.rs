use std::{fmt::Display, ops::Add};

use aoc::{output, Day};
use itertools::iproduct;

#[derive(Default, Clone)]
struct Grid {
    elements: Vec<Option<char>>,
    _width: usize,
}

impl Add for Grid {
    type Output = Grid;

    fn add(self, rhs: Self) -> Self::Output {
        let mut res = self.clone();
        for x in 0..rhs.width() {
            for y in 0..rhs.height() {
                if let Some(ch) = rhs.at((x, y)) {
                    if let Some(res_ch) = res.at_mut((x, y)) {
                        *res_ch = Some(ch)
                    }
                }
            }
        }
        res
    }
}

impl Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..self.height() {
            for x in 0..self.width() {
                write!(
                    f,
                    "{}",
                    match self.at((x, y)) {
                        Some(ch) => ch,
                        None => ' ',
                    }
                )?
            }
            if y != self.height() - 1 {
                writeln!(f, "")?
            }
        }
        Ok(())
    }
}

impl Grid {
    fn at(&self, (x, y): (usize, usize)) -> Option<char> {
        if x > self._width {
            return None;
        }
        *self.elements.get(x + y * self._width)?
    }

    fn at_mut(&mut self, (x, y): (usize, usize)) -> Option<&mut Option<char>> {
        if x > self._width {
            return None;
        }
        self.elements.get_mut(x + y * self._width)
    }

    fn width(&self) -> usize {
        self._width
    }

    fn height(&self) -> usize {
        self.elements.len() / self._width
    }

    fn overlaps(&self, other: &Grid, offset: (usize, usize)) -> bool {
        iproduct!(0..other.width(), 0..other.height()).all(|pos| {
            match (self.at((offset.0 + pos.0, offset.1 + pos.1)), other.at(pos)) {
                (Some(self_char), Some(other_char)) => self_char == other_char,
                (None, Some(_)) => false,
                (_, None) => true,
            }
        })
    }

    fn count_overlaps(&self, other: &Grid) -> usize {
        iproduct!(
            0..self.width() - other.width() + 1,
            0..self.height() - other.height() + 1
        )
        .filter(|&offset| self.overlaps(&other, offset))
        .count()
    }
}

fn word_rotations(word: &str) -> [Grid; 8] {
    [
        // XMAS
        Grid {
            elements: word.chars().map(Option::Some).collect(),
            _width: word.len(),
        },
        // SAMX
        Grid {
            elements: word.chars().rev().map(Option::Some).collect(),
            _width: word.len(),
        },
        // X
        // M
        // A
        // S
        Grid {
            elements: word.chars().map(Option::Some).collect(),
            _width: 1,
        },
        // S
        // A
        // M
        // X
        Grid {
            elements: word.chars().rev().map(Option::Some).collect(),
            _width: 1,
        },
        // X
        //  M
        //   A
        //    S
        {
            let mut elements = Vec::new();
            for (i, ch) in word.char_indices() {
                for _ in 0..i {
                    elements.push(None);
                }
                elements.push(Some(ch));
                for _ in 0..word.len() - 1 - i {
                    elements.push(None);
                }
            }
            Grid {
                elements,
                _width: word.len(),
            }
        },
        // S
        //  A
        //   M
        //    X
        {
            let mut elements = Vec::new();
            for (i, ch) in word.chars().rev().enumerate() {
                for _ in 0..i {
                    elements.push(None);
                }
                elements.push(Some(ch));
                for _ in 0..word.len() - 1 - i {
                    elements.push(None);
                }
            }
            Grid {
                elements,
                _width: word.len(),
            }
        },
        //    X
        //   M
        //  A
        // S
        {
            let mut elements = Vec::new();
            for (i, ch) in word.char_indices() {
                for _ in 0..word.len() - 1 - i {
                    elements.push(None);
                }
                elements.push(Some(ch));
                for _ in 0..i {
                    elements.push(None);
                }
            }
            Grid {
                elements,
                _width: word.len(),
            }
        },
        //    S
        //   A
        //  M
        // X
        {
            let mut elements = Vec::new();
            for (i, ch) in word.chars().rev().enumerate() {
                for _ in 0..word.len() - 1 - i {
                    elements.push(None);
                }
                elements.push(Some(ch));
                for _ in 0..i {
                    elements.push(None);
                }
            }
            Grid {
                elements,
                _width: word.len(),
            }
        },
    ]
}

fn word_xs(word: &str) -> [Grid; 4] {
    let [_, _, _, _, down_diag, down_diag_rev, up_diag, up_diag_rev] = word_rotations(word);
    [
        down_diag.clone() + up_diag.clone(),
        down_diag + up_diag_rev.clone(),
        down_diag_rev.clone() + up_diag,
        down_diag_rev + up_diag_rev,
    ]
}

#[derive(Default)]
pub struct Day04 {
    input: Grid,
}

impl Day for Day04 {
    fn initialize(&mut self, input: &str) {
        self.input = Grid {
            elements: input
                .trim()
                .lines()
                .flat_map(|line| line.chars().map(Option::Some))
                .collect(),
            _width: input.trim().lines().next().unwrap().len(),
        };
    }

    fn part1(&mut self) -> Vec<String> {
        output!(word_rotations("XMAS")
            .iter()
            .map(|word| self.input.count_overlaps(&word))
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(word_xs("MAS")
            .iter()
            .map(|word| self.input.count_overlaps(&word))
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        4
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day04::default();
        day.initialize("MMMSXXMASM\nMSAMXMSMSA\nAMXSXMAAMM\nMSAMASMSMX\nXMASAMXAMM\nXXAMMXXAMA\nSMSMSASXSS\nSAXAMASAAA\nMAMMMXMMMM\nMXMXAXMASX\n");
        assert_eq!(day.part1(), vec!["18".to_owned()]);
        assert_eq!(day.part2(), vec!["9".to_owned()]);
    }

    #[test]
    fn rotations() {
        let rotations = word_rotations("XMAS");
        assert_eq!(format!("{}", rotations[0]), "XMAS".to_owned());
        assert_eq!(format!("{}", rotations[1]), "SAMX".to_owned());
        assert_eq!(format!("{}", rotations[2]), "X\nM\nA\nS".to_owned());
        assert_eq!(format!("{}", rotations[3]), "S\nA\nM\nX".to_owned());
        assert_eq!(
            format!("{}", rotations[4]),
            "X   \n M  \n  A \n   S".to_owned()
        );
        assert_eq!(
            format!("{}", rotations[5]),
            "S   \n A  \n  M \n   X".to_owned()
        );
        assert_eq!(
            format!("{}", rotations[6]),
            "   X\n  M \n A  \nS   ".to_owned()
        );
        assert_eq!(
            format!("{}", rotations[7]),
            "   S\n  A \n M  \nX   ".to_owned()
        );
    }

    #[test]
    fn xs() {
        let xs = word_xs("MAS");
        assert_eq!(format!("{}", xs[0]), "M M\n A \nS S".to_owned());
        assert_eq!(format!("{}", xs[1]), "M S\n A \nM S".to_owned());
        assert_eq!(format!("{}", xs[2]), "S M\n A \nS M".to_owned());
        assert_eq!(format!("{}", xs[3]), "S S\n A \nM M".to_owned());
    }

    #[test]
    fn overlaps() {
        let input = Grid {
            elements: "..X...\n.SAMX.\n.A..A.\nXMAS.S\n.X....\n"
                .trim()
                .lines()
                .flat_map(|line| line.chars().map(Option::Some))
                .collect(),
            _width: 6,
        };
        assert!(input.overlaps(&word_rotations("XMAS")[0], (0, 3)))
    }
}
