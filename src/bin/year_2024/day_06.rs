use std::collections::HashSet;

use aoc::{output, Day};
use itertools::iproduct;

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
enum Orientation {
    Up,
    Down,
    Left,
    Right,
}

impl Orientation {
    fn rotate_right(&self) -> Orientation {
        match self {
            Orientation::Up => Orientation::Right,
            Orientation::Down => Orientation::Left,
            Orientation::Left => Orientation::Up,
            Orientation::Right => Orientation::Down,
        }
    }

    fn go_in_direction(&self, position: (usize, usize)) -> Option<(usize, usize)> {
        Some(match self {
            Orientation::Up => (position.0, position.1.checked_sub(1)?),
            Orientation::Down => (position.0, position.1.checked_add(1)?),
            Orientation::Left => (position.0.checked_sub(1)?, position.1),
            Orientation::Right => (position.0.checked_add(1)?, position.1),
        })
    }
}

#[derive(Default)]
pub struct Day06 {
    guard_start_pos: (usize, usize),
    obstacles: HashSet<(usize, usize)>,
    width: usize,
    height: usize,
}

fn get_char_pos(string: &str, find_ch: char) -> HashSet<(usize, usize)> {
    string
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.char_indices()
                .filter_map(move |(x, string_ch)| match string_ch {
                    _ if string_ch == find_ch => Some((x, y)),
                    _ => None,
                })
        })
        .collect()
}

fn calculate_path(
    mut pos: (usize, usize),
    mut orientation: Orientation,
    obstacles: &HashSet<(usize, usize)>,
    width: usize,
    height: usize,
) -> Option<usize> {
    let mut history = HashSet::new();
    history.insert((pos, orientation));
    loop {
        pos = if let Some(pos) = orientation.go_in_direction(pos) {
            if obstacles.contains(&pos) {
                orientation = orientation.rotate_right();
                continue;
            }
            if pos.0 > width || pos.1 > height {
                break;
            }
            if history.contains(&(pos, orientation)) {
                return None;
            }
            pos
        } else {
            break;
        };
        history.insert((pos, orientation));
    }
    Some(
        history
            .iter()
            .map(|(pos, _)| *pos)
            .collect::<HashSet<_>>()
            .len(),
    )
}

impl Day for Day06 {
    fn initialize(&mut self, input: &str) {
        self.guard_start_pos = *get_char_pos(input.trim(), '^').iter().next().unwrap();
        self.obstacles = get_char_pos(input.trim(), '#');
        self.width = self.obstacles.iter().max_by_key(|(x, _)| x).unwrap().0;
        self.height = self.obstacles.iter().max_by_key(|(_, y)| y).unwrap().1;
    }

    fn part1(&mut self) -> Vec<String> {
        output!(calculate_path(
            self.guard_start_pos,
            Orientation::Up,
            &self.obstacles,
            self.width,
            self.height
        )
        .unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(iproduct!(0..=self.width, 0..=self.height)
            .filter(|new_obstacle_pos| {
                let mut obstacles = self.obstacles.clone();
                obstacles.insert(*new_obstacle_pos);
                matches!(
                    calculate_path(
                        self.guard_start_pos,
                        Orientation::Up,
                        &obstacles,
                        self.width,
                        self.height,
                    ),
                    None
                )
            })
            .count())
    }

    fn name(&self) -> usize {
        6
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let mut day = Day06::default();
        day.initialize("....#.....\n.........#\n..........\n..#.......\n.......#..\n..........\n.#..^.....\n........#.\n#.........\n......#...\n");
        assert_eq!(day.part1(), vec!["41".to_owned()]);
    }
}
