use aoc::{Day, Year};

use proc_macro::{days, import_days};

import_days!(1..=20);

#[derive(Default)]
pub(crate) struct Year2024 {}

impl Year for Year2024 {
    fn days(&self) -> Vec<Box<dyn Day>> {
        days!(1..=20)
    }

    fn name(&self) -> usize {
        2024
    }
}
