use aoc::{output, Day};

#[derive(Debug, Clone, Copy)]
struct Block {
    size: usize,
    id: Option<usize>,
}

#[allow(dead_code)]
fn print_blocks(blocks: &[Block]) {
    for block in blocks {
        for _ in 0..block.size {
            match block.id {
                Some(id) => print!("{}", id),
                None => print!("."),
            }
        }
    }
    println!("");
}

fn checksum(blocks: &[Block]) -> usize {
    blocks
        .iter()
        .flat_map(|block| (0..block.size).map(|_| block.id))
        .enumerate()
        .map(|(pos, id)| id.map(|id| id * pos))
        .filter_map(|check| check)
        .sum::<usize>()
}

#[derive(Default)]
pub struct Day09 {
    blocks: Vec<Block>,
}

impl Day for Day09 {
    fn initialize(&mut self, input: &str) {
        self.blocks = input
            .trim()
            .char_indices()
            .map(|(pos, char)| Block {
                size: char.to_digit(10).unwrap() as usize,
                id: if pos % 2 == 0 { Some(pos / 2) } else { None },
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut blocks = self.blocks.clone();
        loop {
            let last_full_block = blocks
                .iter()
                .enumerate()
                .rev()
                .find(|(_, block)| {
                    matches!(
                        block,
                        &&Block {
                            size: _,
                            id: Some(_)
                        }
                    )
                })
                .unwrap()
                .0;
            let first_empty_block = blocks
                .iter()
                .position(|block| matches!(block, &Block { size: _, id: None }))
                .unwrap();
            if last_full_block < first_empty_block {
                break;
            }
            if blocks[first_empty_block].size == blocks[last_full_block].size {
                blocks[first_empty_block].id = blocks[last_full_block].id;
                blocks[last_full_block].id = None;
            } else if blocks[first_empty_block].size < blocks[last_full_block].size {
                blocks[last_full_block].size -= blocks[first_empty_block].size;
                blocks.insert(
                    last_full_block + 1,
                    Block {
                        size: blocks[first_empty_block].size,
                        id: None,
                    },
                );
                blocks[first_empty_block].id = blocks[last_full_block].id;
            } else {
                let block_size_diff = blocks[first_empty_block].size - blocks[last_full_block].size;
                blocks[first_empty_block].id = blocks[last_full_block].id;
                blocks[first_empty_block].size -= block_size_diff;
                blocks[last_full_block].id = None;
                blocks.insert(
                    first_empty_block + 1,
                    Block {
                        size: block_size_diff,
                        id: None,
                    },
                );
            }
        }
        output!(checksum(&blocks))
    }

    fn part2(&mut self) -> Vec<String> {
        let mut blocks = self.blocks.clone();
        for id in (0..=blocks.iter().filter_map(|block| block.id).max().unwrap()).rev() {
            let block_pos = blocks
                .iter()
                .position(|block| block.id == Some(id))
                .unwrap();
            let first_available_free_block = blocks
                .iter()
                .position(|block| block.id.is_none() && block.size >= blocks[block_pos].size)
                .unwrap_or(usize::MAX);
            if first_available_free_block > block_pos {
                continue;
            }
            if blocks[first_available_free_block].size == blocks[block_pos].size {
                blocks[first_available_free_block].id = blocks[block_pos].id;
                blocks[block_pos].id = None;
            } else {
                let block_size_diff =
                    blocks[first_available_free_block].size - blocks[block_pos].size;
                blocks[first_available_free_block].id = blocks[block_pos].id;
                blocks[first_available_free_block].size -= block_size_diff;
                blocks[block_pos].id = None;
                blocks.insert(
                    first_available_free_block + 1,
                    Block {
                        size: block_size_diff,
                        id: None,
                    },
                );
            }
        }
        output!(checksum(&blocks))
    }

    fn name(&self) -> usize {
        9
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day09::default();
        day.initialize("2333133121414131402");
        assert_eq!(day.part1(), vec!["1928".to_owned()]);
        assert_eq!(day.part2(), vec!["2858".to_owned()]);
    }
}
