use std::collections::HashSet;

use glam::IVec2;

use aoc::{output, Day};

#[derive(PartialEq, Eq, Clone, Copy, Debug, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn to_vec(&self) -> IVec2 {
        match self {
            Direction::Up => IVec2::NEG_Y,
            Direction::Down => IVec2::Y,
            Direction::Left => IVec2::NEG_X,
            Direction::Right => IVec2::X,
        }
    }

    fn rotate_clockwise(&self) -> Direction {
        match self {
            Direction::Up => Direction::Right,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Right => Direction::Down,
        }
    }

    fn rotate_counter_clockwise(&self) -> Direction {
        match self {
            Direction::Up => Direction::Left,
            Direction::Down => Direction::Right,
            Direction::Left => Direction::Down,
            Direction::Right => Direction::Up,
        }
    }
}

#[derive(Default, Debug)]
pub struct Day16 {
    tiles: HashSet<IVec2>,
    start: IVec2,
    end: IVec2,
}

impl Day16 {
    fn successors(&self, &(pos, dir): &(IVec2, Direction)) -> Vec<((IVec2, Direction), usize)> {
        let mut v = vec![
            ((pos, dir.rotate_clockwise()), 1000),
            ((pos, dir.rotate_counter_clockwise()), 1000),
        ];
        if self.tiles.contains(&(pos + dir.to_vec())) {
            v.push(((pos + dir.to_vec(), dir), 1));
        }
        v
    }

    fn is_end(&self, &(pos, _): &(IVec2, Direction)) -> bool {
        pos == self.end
    }

    fn heuristic(&self, &(pos, _): &(IVec2, Direction)) -> usize {
        ((pos - self.end).x.abs() + (pos - self.end).y.abs()) as usize
    }

    fn start_node(&self) -> (IVec2, Direction) {
        (self.start, Direction::Right)
    }
}

impl Day for Day16 {
    fn initialize(&mut self, input: &str) {
        for (x, y, ch) in input.trim().lines().enumerate().flat_map(|(y, line)| {
            line.char_indices()
                .filter_map(move |(x, ch)| if ch != '#' { Some((x, y, ch)) } else { None })
        }) {
            self.tiles.insert(IVec2::new(x as i32, y as i32));
            if ch == 'S' {
                self.start = IVec2::new(x as i32, y as i32);
            } else if ch == 'E' {
                self.end = IVec2::new(x as i32, y as i32);
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        output!(
            pathfinding::directed::dijkstra::dijkstra(
                &self.start_node(),
                |node| self.successors(node),
                |node| self.is_end(node),
            )
            .unwrap()
            .1
        )
    }

    fn part2(&mut self) -> Vec<String> {
        output!(pathfinding::directed::astar::astar_bag(
            &self.start_node(),
            |node| self.successors(node),
            |node| self.heuristic(node),
            |node| self.is_end(node),
        )
        .unwrap()
        .0
        .flat_map(|path| path.into_iter().map(|(pos, _)| pos))
        .collect::<HashSet<_>>()
        .len())
    }

    fn name(&self) -> usize {
        16
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day16::default();
        day.initialize("###############\n#.......#....E#\n#.#.###.#.###.#\n#.....#.#...#.#\n#.###.#####.#.#\n#.#.#.......#.#\n#.#.#####.###.#\n#...........#.#\n###.#.#####.#.#\n#...#.....#.#.#\n#.#.#.###.#.#.#\n#.....#...#.#.#\n#.###.#.#.#.#.#\n#S..#.....#...#\n###############\n");
        assert_eq!(day.part1(), vec!["7036".to_owned()]);
        assert_eq!(day.part2(), vec!["45".to_owned()]);
        let mut day = Day16::default();
        day.initialize("#################\n#...#...#...#..E#\n#.#.#.#.#.#.#.#.#\n#.#.#.#...#...#.#\n#.#.#.#.###.#.#.#\n#...#.#.#.....#.#\n#.#.#.#.#.#####.#\n#.#...#.#.#.....#\n#.#.#####.#.###.#\n#.#.#.......#...#\n#.#.###.#####.###\n#.#.#...#.....#.#\n#.#.#.#####.###.#\n#.#.#.........#.#\n#.#.#.#########.#\n#S#.............#\n#################\n");
        assert_eq!(day.part1(), vec!["11048".to_owned()]);
        assert_eq!(day.part2(), vec!["64".to_owned()]);
    }
}
