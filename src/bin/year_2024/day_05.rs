use std::cmp::Ordering;

use aoc::{output, Day};

#[derive(Default)]
pub struct Day05 {
    rules: Vec<(usize, usize)>,
    updates: Vec<Vec<usize>>,
}

fn update_comply(update: &[usize], rules: &[(usize, usize)]) -> bool {
    rules.iter().all(|(left, right)| {
        match (
            update.iter().position(|num| num == left),
            update.iter().position(|num| num == right),
        ) {
            (None, _) | (_, None) => true,
            (Some(left_pos), Some(right_pos)) => left_pos < right_pos,
        }
    })
}

fn order_update(update: &mut [usize], rules: &[(usize, usize)]) {
    update.sort_by(|left, right| {
        if rules.contains(&(*left, *right)) {
            Ordering::Less
        } else if rules.contains(&(*right, *left)) {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });
}

impl Day for Day05 {
    fn initialize(&mut self, input: &str) {
        let (rules, updates) = input.trim().split_once("\n\n").unwrap();
        self.rules = rules
            .lines()
            .map(|line| {
                let (left, right) = line.split_once('|').unwrap();
                (
                    left.parse::<usize>().unwrap(),
                    right.parse::<usize>().unwrap(),
                )
            })
            .collect();
        self.updates = updates
            .lines()
            .map(|line| {
                line.split(',')
                    .map(|num| num.parse::<usize>().unwrap())
                    .collect()
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .updates
            .iter()
            .filter(|update| update_comply(update, &self.rules))
            .map(|update| update[(update.len() - 1) / 2])
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .updates
            .iter_mut()
            .filter(|update| !update_comply(update, &self.rules))
            .map(|update| {
                order_update(update, &self.rules);
                update[(update.len() - 1) / 2]
            })
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        5
    }
}
