use glam::IVec2;
use std::collections::HashSet;

use aoc::{output, Day};

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn to_vec(&self) -> IVec2 {
        match self {
            Direction::Up => IVec2::NEG_Y,
            Direction::Down => IVec2::Y,
            Direction::Left => IVec2::NEG_X,
            Direction::Right => IVec2::X,
        }
    }
}

impl TryFrom<char> for Direction {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '^' => Ok(Direction::Up),
            '>' => Ok(Direction::Right),
            '<' => Ok(Direction::Left),
            'v' => Ok(Direction::Down),
            _ => Err(()),
        }
    }
}

#[derive(Default, Debug)]
pub struct Day15 {
    boxes: HashSet<IVec2>,
    walls: HashSet<IVec2>,
    moves: Vec<Direction>,
    bot: IVec2,
}

#[allow(dead_code)]
fn print_map(bot: IVec2, boxes: &HashSet<IVec2>, walls: &HashSet<IVec2>) {
    let map_size = walls.iter().cloned().reduce(IVec2::max).unwrap() + IVec2::ONE;
    for y in 0..map_size.y {
        for x in 0..map_size.x {
            if bot == IVec2::new(x, y) {
                print!("@");
            } else if walls.contains(&IVec2::new(x, y)) {
                print!("#");
            } else if boxes.contains(&IVec2::new(x, y)) {
                print!("O");
            } else {
                print!(" ");
            }
        }
        println!("");
    }
    println!("");
}

#[allow(dead_code)]
fn print_map2(bot: IVec2, boxes: &HashSet<IVec2>, walls: &HashSet<IVec2>) {
    let map_size = walls.iter().cloned().reduce(IVec2::max).unwrap() + IVec2::ONE;
    for y in 0..map_size.y {
        for x in 0..map_size.x {
            if bot == IVec2::new(x, y) {
                print!("@");
            } else if walls.contains(&IVec2::new(x, y)) {
                print!("#");
            } else if boxes.contains(&IVec2::new(x, y)) {
                print!("[");
            } else if boxes.contains(&IVec2::new(x - 1, y)) {
                print!("]");
            } else {
                print!(".");
            }
        }
        println!("");
    }
    println!("");
}

fn process_moves(
    mut bot_pos: IVec2,
    moves: &[Direction],
    mut boxes: HashSet<IVec2>,
    walls: &HashSet<IVec2>,
) -> HashSet<IVec2> {
    for &mov in moves {
        let mut new_pos = bot_pos + mov.to_vec();
        while boxes.contains(&new_pos) {
            new_pos += mov.to_vec();
        }
        if walls.contains(&new_pos) {
            continue;
        }
        bot_pos += mov.to_vec();
        if bot_pos != new_pos {
            boxes.remove(&bot_pos);
            boxes.insert(new_pos);
        }
    }
    boxes
}

fn process_moves2(
    mut bot_pos: IVec2,
    moves: &[Direction],
    boxes: &HashSet<IVec2>,
    walls: &HashSet<IVec2>,
) -> HashSet<IVec2> {
    bot_pos.x *= 2;
    let mut boxes = {
        let mut temp = HashSet::new();
        for box_pos in boxes {
            temp.insert(box_pos.with_x(box_pos.x * 2));
        }
        temp
    };
    let walls = {
        let mut temp = HashSet::new();
        for wall in walls {
            temp.insert(wall.with_x(wall.x * 2));
            temp.insert(wall.with_x(wall.x * 2 + 1));
        }
        temp
    };
    'next_move: for &mov in moves {
        let mut boxes_to_move = HashSet::new();
        let mut positions_to_check = Vec::new();
        positions_to_check.push(bot_pos + mov.to_vec());
        while let Some(pos) = positions_to_check.pop() {
            if walls.contains(&pos) {
                continue 'next_move;
            }
            match mov {
                Direction::Up | Direction::Down => {
                    if boxes.contains(&pos) {
                        boxes_to_move.insert(pos);
                        positions_to_check.push(pos + mov.to_vec());
                        positions_to_check.push(pos + mov.to_vec() + IVec2::X);
                    }
                    if boxes.contains(&(pos - IVec2::X)) {
                        boxes_to_move.insert(pos - IVec2::X);
                        positions_to_check.push(pos + mov.to_vec());
                        positions_to_check.push(pos + mov.to_vec() - IVec2::X);
                    }
                }
                Direction::Right => {
                    if boxes.contains(&pos) {
                        boxes_to_move.insert(pos);
                        positions_to_check.push(pos + 2 * IVec2::X);
                    }
                }
                Direction::Left => {
                    if boxes.contains(&(pos + IVec2::NEG_X)) {
                        boxes_to_move.insert(pos + IVec2::NEG_X);
                        positions_to_check.push(pos + 2 * IVec2::NEG_X);
                    }
                }
            }
        }
        for box_pos in &boxes_to_move {
            boxes.remove(box_pos);
        }
        for box_pos in boxes_to_move {
            boxes.insert(box_pos + mov.to_vec());
        }
        bot_pos += mov.to_vec();
    }
    boxes
}

impl Day for Day15 {
    fn initialize(&mut self, input: &str) {
        let (map, moves) = input.trim().split_once("\n\n").unwrap();
        for (x, y, ch) in map
            .lines()
            .enumerate()
            .flat_map(|(y, line)| line.char_indices().map(move |(x, ch)| (x, y, ch)))
        {
            match ch {
                '.' => {}
                '#' => {
                    self.walls.insert(IVec2::new(x as i32, y as i32));
                }
                'O' => {
                    self.boxes.insert(IVec2::new(x as i32, y as i32));
                }
                '@' => {
                    self.bot = IVec2::new(x as i32, y as i32);
                }
                _ => panic!("invalid input char"),
            }
        }
        self.moves = moves
            .chars()
            .map(Direction::try_from)
            .filter_map(Result::ok)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(
            process_moves(self.bot, &self.moves, self.boxes.clone(), &self.walls)
                .iter()
                .map(|pos| pos.x + 100 * pos.y)
                .sum::<i32>()
        )
    }

    fn part2(&mut self) -> Vec<String> {
        output!(
            process_moves2(self.bot, &self.moves, &self.boxes, &self.walls)
                .iter()
                .map(|pos| pos.x + 100 * pos.y)
                .sum::<i32>()
        )
    }

    fn name(&self) -> usize {
        15
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day15::default();
        day.initialize("########\n#..O.O.#\n##@.O..#\n#...O..#\n#.#.O..#\n#...O..#\n#......#\n########\n\n<^^>>>vv<v>>v<<\n");
        assert_eq!(day.part1(), vec!["2028".to_owned()]);
        let mut day = Day15::default();
        day.initialize("##########\n#..O..O.O#\n#......O.#\n#.OO..O.O#\n#..O@..O.#\n#O#..O...#\n#O..O..O.#\n#.OO.O.OO#\n#....O...#\n##########\n\n<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^\nvvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v\n><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<\n<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^\n^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><\n^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^\n>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^\n<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>\n^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>\nv^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^\n");
        assert_eq!(day.part1(), vec!["10092".to_owned()]);
        assert_eq!(day.part2(), vec!["9021".to_owned()]);
    }
}
