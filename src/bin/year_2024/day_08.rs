use glam::IVec2;
use itertools::iproduct;
use std::collections::{HashMap, HashSet};

use aoc::{output, Day};

#[derive(Default)]
pub struct Day08 {
    antennas: HashMap<char, Vec<IVec2>>,
    width: usize,
    height: usize,
}

impl Day for Day08 {
    fn initialize(&mut self, input: &str) {
        input.trim().lines().enumerate().for_each(|(y, line)| {
            self.height = self.height.max(y);
            line.char_indices().for_each(|(x, ch)| {
                self.width = self.width.max(x);
                match ch {
                    '.' => {}
                    ch => {
                        self.antennas
                            .entry(ch)
                            .or_default()
                            .push(IVec2::from_array([x as i32, y as i32]));
                    }
                }
            })
        });
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .antennas
            .iter()
            .fold(&mut HashSet::new(), |antinodes, (_, antennas)| {
                iproduct!(antennas.iter(), antennas.iter()).for_each(|(antenna1, antenna2)| {
                    if antenna1 == antenna2 {
                        return;
                    }
                    let diff = antenna2 - antenna1;
                    for candidate in [antenna1 - diff, antenna2 + diff] {
                        if candidate.x >= 0
                            && candidate.y >= 0
                            && candidate.x <= self.width as i32
                            && candidate.y <= self.height as i32
                        {
                            antinodes.insert(candidate);
                        }
                    }
                });
                return antinodes;
            })
            .len())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .antennas
            .iter()
            .fold(&mut HashSet::new(), |antinodes, (_, antennas)| {
                iproduct!(antennas.iter(), antennas.iter()).for_each(|(antenna1, antenna2)| {
                    if antenna1 == antenna2 {
                        return;
                    }
                    let diff = antenna2 - antenna1;
                    for multiplier in 0.. {
                        let mut inserted = false;
                        for candidate in
                            [antenna1 - multiplier * diff, antenna2 + multiplier * diff]
                        {
                            if candidate.x >= 0
                                && candidate.y >= 0
                                && candidate.x <= self.width as i32
                                && candidate.y <= self.height as i32
                            {
                                antinodes.insert(candidate);
                                inserted = true;
                            }
                        }
                        if !inserted {
                            break;
                        }
                    }
                });
                return antinodes;
            })
            .len())
    }

    fn name(&self) -> usize {
        8
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day08::default();
        day.initialize("............\n........0...\n.....0......\n.......0....\n....0.......\n......A.....\n............\n............\n........A...\n.........A..\n............\n............\n");
        assert_eq!(day.part1(), vec!["14".to_owned()]);
        assert_eq!(day.part2(), vec!["34".to_owned()]);
    }
}
