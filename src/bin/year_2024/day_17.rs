use aoc::{output, Day};
use itertools::Itertools;

#[derive(Default, Debug, Clone)]
struct Program {
    program: Vec<u8>,
    pc: usize,
    reg_a: i64,
    reg_b: i64,
    reg_c: i64,
    output: Vec<u8>,
}

impl Program {
    fn step(&mut self) -> bool {
        let Some(&op) = self.program.get(self.pc) else {
            return false;
        };
        self.pc += 1;
        match op {
            0 => self.reg_a >>= self.combo_operand(),
            1 => self.reg_b ^= self.literal_operand() as i64,
            2 => self.reg_b = self.combo_operand() as i64 % 8,
            3 => {
                let jmp_loc = self.literal_operand() as usize;
                if self.reg_a != 0 {
                    self.pc = jmp_loc;
                }
            }
            4 => {
                let _ = self.literal_operand();
                self.reg_b ^= self.reg_c
            }
            5 => {
                let out = (self.combo_operand() % 8) as u8;
                self.output.push(out);
            }
            6 => self.reg_b = self.reg_a >> self.combo_operand(),
            7 => self.reg_c = self.reg_a >> self.combo_operand(),
            _ => panic!("invalid opcode {op}"),
        }
        true
    }

    fn combo_operand(&mut self) -> i64 {
        let ret = match self.program[self.pc] {
            0..=3 => self.program[self.pc] as i64,
            4 => self.reg_a,
            5 => self.reg_b,
            6 => self.reg_c,
            _ => panic!("invalid combo operand {}", self.program[self.pc]),
        };
        self.pc += 1;
        ret
    }

    fn literal_operand(&mut self) -> u8 {
        let ret = self.program[self.pc];
        self.pc += 1;
        ret
    }

    fn try_a(&mut self, a: i64) {
        self.reg_a = a;
        self.reg_b = 0;
        self.reg_c = 0;
        self.pc = 0;
        self.output = vec![];
        while self.step() {}
    }
}

#[derive(Default, Debug)]
pub struct Day17 {
    program: Program,
}

impl Day for Day17 {
    fn initialize(&mut self, input: &str) {
        let mut lines = input.trim().lines();
        self.program.reg_a = lines
            .next()
            .unwrap()
            .strip_prefix("Register A: ")
            .unwrap()
            .parse()
            .unwrap();
        self.program.reg_b = lines
            .next()
            .unwrap()
            .strip_prefix("Register B: ")
            .unwrap()
            .parse()
            .unwrap();
        self.program.reg_c = lines
            .next()
            .unwrap()
            .strip_prefix("Register C: ")
            .unwrap()
            .parse()
            .unwrap();
        self.program.program = lines
            .skip(1)
            .next()
            .unwrap()
            .strip_prefix("Program: ")
            .unwrap()
            .split(',')
            .map(str::parse)
            .collect::<Result<_, _>>()
            .unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut p = self.program.clone();
        while p.step() {}
        output!(p.output.iter().join(","))
    }

    fn part2(&mut self) -> Vec<String> {
        output!(pathfinding::directed::dijkstra::dijkstra(
            &0,
            {
                let mut p = self.program.clone();
                move |a| {
                    let mut v = vec![];
                    for a_tail in 0..8 {
                        p.try_a((a << 3) + a_tail);
                        if p.output == p.program[p.program.len() - p.output.len()..] {
                            v.push(((a << 3) + a_tail, (a << 3) + a_tail));
                        }
                    }
                    v
                }
            },
            {
                let mut p = self.program.clone();
                move |&a| {
                    p.try_a(a);
                    p.program == p.output
                }
            },
        )
        .unwrap()
        .0
        .last()
        .unwrap())
    }

    fn name(&self) -> usize {
        17
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day17::default();
        day.initialize("Register A: 729\nRegister B: 0\nRegister C: 0\n\nProgram: 0,1,5,4,3,0\n");
        assert_eq!(day.part1(), vec!["4,6,3,5,6,3,5,2,1,0".to_owned()]);
        let mut day = Day17::default();
        day.initialize("Register A: 2024\nRegister B: 0\nRegister C: 0\n\nProgram: 0,3,5,4,3,0\n");
        assert_eq!(day.part2(), vec!["117440".to_owned()]);
    }
}
