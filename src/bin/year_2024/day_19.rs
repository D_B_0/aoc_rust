use aoc::{output, Day};

#[derive(Default, Debug)]
pub struct Day19 {
    towels: Vec<String>,
    patterns: Vec<String>,
}

impl Day19 {
    fn can_make(&self, start_pat: &str) -> bool {
        pathfinding::directed::astar::astar(
            &start_pat,
            |pat| {
                self.towels.iter().filter_map(|towel| {
                    if pat.starts_with(towel) {
                        Some((&pat[towel.len()..], towel.len()))
                    } else {
                        None
                    }
                })
            },
            |pat| pat.len(),
            |pat| pat.is_empty(),
        )
        .is_some()
    }

    fn num_arrangements(&self, pat: &str) -> usize {
        pathfinding::directed::count_paths::count_paths(
            pat,
            |pat| {
                self.towels.iter().filter_map(|towel| {
                    if pat.starts_with(towel) {
                        Some(&pat[towel.len()..])
                    } else {
                        None
                    }
                })
            },
            |pat| pat.is_empty(),
        )
    }
}

impl Day for Day19 {
    fn initialize(&mut self, input: &str) {
        let (towels, patterns) = input.split_once("\n\n").unwrap();
        self.towels = towels.split(", ").map(str::to_owned).collect();
        self.patterns = patterns.lines().map(str::to_owned).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .patterns
            .iter()
            .filter(|pat| self.can_make(pat))
            .count())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .patterns
            .iter()
            .map(|pat| self.num_arrangements(pat))
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        19
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let mut day = Day19::default();
        day.initialize("r, wr, b, g, bwu, rb, gb, br\n\nbrwrr\nbggr\ngbbr\nrrbgbr\nubwu\nbwurrg\nbrgr\nbbrgwb\n");
        assert_eq!(day.part1(), vec!["6".to_owned()]);
        assert_eq!(day.part2(), vec!["16".to_owned()]);
    }
}
