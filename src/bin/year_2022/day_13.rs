use std::{cmp::Ordering, str::FromStr};

use aoc::{output, Day};

#[derive(Debug, PartialEq, Clone)]
enum Packet {
    Int(usize),
    List(Vec<Packet>),
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Packet::Int(i1), Packet::Int(i2)) => match i1.cmp(i2) {
                Ordering::Equal => None,
                Ordering::Less => Some(Ordering::Less),
                Ordering::Greater => Some(Ordering::Greater),
            },
            (Packet::List(l1), Packet::List(l2)) => {
                for i in 0..l1.len().min(l2.len()) {
                    if l1[i] < l2[i] {
                        return Some(Ordering::Less);
                    } else if l1[i] > l2[i] {
                        return Some(Ordering::Greater);
                    }
                }
                match l1.len().cmp(&l2.len()) {
                    Ordering::Less => Some(Ordering::Less),
                    Ordering::Equal => None,
                    Ordering::Greater => Some(Ordering::Greater),
                }
            }
            (Packet::Int(i), Packet::List(_)) => {
                let l1 = Packet::List(vec![Packet::Int(*i)]);
                l1.partial_cmp(other)
            }
            (Packet::List(_), Packet::Int(i)) => {
                let l2 = Packet::List(vec![Packet::Int(*i)]);
                self.partial_cmp(&l2)
            }
        }
    }
}

fn group_parens(s: &str) -> Vec<&str> {
    let mut res = vec![];
    let mut start = 0;
    let mut end = 0;
    while end != s.len() {
        if s.chars().nth(start) == Some('[') {
            let mut count_parens = 0;
            loop {
                let ch = s.chars().nth(end).unwrap();
                if ch == '[' {
                    count_parens += 1;
                } else if ch == ']' {
                    count_parens -= 1;
                }
                end += 1;
                if count_parens == 0 {
                    break;
                }
            }
        } else {
            while s.chars().nth(end) != Some(',') && end != s.len() {
                end += 1;
            }
        }
        res.push(&s[start..end]);
        if s.chars().nth(end) == Some(',') {
            start = end + 1;
            end = start;
        } else {
            assert_eq!(end, s.len());
        }
    }

    res
}

impl FromStr for Packet {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.chars().next().ok_or_else(|| "empty string".to_string())? == '[' {
            Ok(Packet::List(
                group_parens(&s[1..s.len() - 1])
                    .iter()
                    .copied()
                    .map(Packet::from_str)
                    .collect::<Result<Vec<_>, _>>()?,
            ))
        } else {
            Ok(Packet::Int(s.parse().map_err(|e| format!("{e}"))?))
        }
    }
}

#[derive(Default)]
pub struct Day13 {
    pairs: Vec<(Packet, Packet)>,
}

impl Day for Day13 {
    fn initialize(&mut self, input: &str) {
        self.pairs = input
            .trim()
            .split("\n\n")
            .map(|s| {
                let (left, right) = s.split_once('\n').unwrap();
                (left.parse().unwrap(), right.parse().unwrap())
            })
            .collect::<Vec<_>>();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .pairs
            .iter()
            .map(|(p1, p2)| p1.partial_cmp(p2))
            .enumerate()
            .filter_map(|(i, ord)| match ord {
                Some(Ordering::Less) => Some(i + 1),
                Some(Ordering::Equal) => None,
                Some(Ordering::Greater) => None,
                None => None,
            })
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut packets = Vec::with_capacity(self.pairs.len() * 2);
        self.pairs
            .iter()
            .cloned()
            .fold(&mut packets, |acc, (p1, p2)| {
                acc.push(p1);
                acc.push(p2);
                acc
            });
        let div1 = Packet::from_str("[[2]]").unwrap();
        let div2 = Packet::from_str("[[6]]").unwrap();
        packets.push(div1.clone());
        packets.push(div2.clone());
        packets.sort_by(|a, b| a.partial_cmp(b).unwrap());
        output!(
            (packets.iter().position(|p| p == &div1).unwrap() + 1)
                * (packets.iter().position(|p| p == &div2).unwrap() + 1)
        )
    }

    fn name(&self) -> usize {
        13
    }
}
