use std::collections::HashSet;

use aoc::{output, Day};

struct RuckSack {
    left: HashSet<char>,
    right: HashSet<char>,
    all: HashSet<char>,
}

#[derive(Default)]
pub struct Day03 {
    sacks: Vec<RuckSack>,
}

fn get_priority(ch: char) -> usize {
    if ch.is_ascii_lowercase() {
        return 1 + ch as usize - 'a' as usize;
    }
    if ch.is_ascii_uppercase() {
        return 27 + ch as usize - 'A' as usize;
    }
    panic!()
}

impl Day for Day03 {
    fn initialize(&mut self, input: &str) {
        self.sacks = input
            .lines()
            .map(|l| {
                let size = l.len() / 2;
                let left = l.chars().take(size).collect();
                let right = l.chars().skip(size).collect();
                let all = l.chars().collect();
                RuckSack { left, right, all }
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .sacks
            .iter()
            .map(|sack| get_priority(*sack.left.intersection(&sack.right).next().unwrap()))
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut sum = 0;
        let mut iter = self.sacks.iter();
        for _ in 0..(self.sacks.len() / 3) {
            let sack1 = iter.next().unwrap();
            let sack2 = iter.next().unwrap();
            let sack3 = iter.next().unwrap();
            let mut commons = sack1.all.intersection(&sack2.all).copied().collect();
            commons = sack3.all.intersection(&commons).copied().collect();
            sum += get_priority(*commons.iter().next().unwrap());
        }
        output!(sum)
    }

    fn name(&self) -> usize {
        3
    }
}
