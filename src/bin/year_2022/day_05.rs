use std::str::FromStr;

use aoc::{output, Day};

#[derive(Debug, Clone, Copy)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (left, right) = s.split_once(" from ").ok_or(())?;
        let (from, to) = right.split_once(" to ").ok_or(())?;
        let (_, count) = left.split_once(' ').ok_or(())?;
        Ok(Instruction {
            count: count.parse().map_err(|_| ())?,
            from: from.parse().map_err(|_| ())?,
            to: to.parse().map_err(|_| ())?,
        })
    }
}

#[derive(Default)]
pub struct Day05 {
    stacks: [Vec<char>; 9],
    instructions: Vec<Instruction>,
}

impl Day for Day05 {
    fn initialize(&mut self, input: &str) {
        let mut lines = input
            .lines()
            .take_while(|l| l.contains('['))
            .collect::<Vec<_>>();
        lines.reverse();
        for i in 0..9 {
            let index = i * 4 + 1;
            for line in &lines {
                let ch = line.chars().nth(index).unwrap();
                if ch != ' ' {
                    self.stacks[i].push(ch);
                }
            }
        }

        let mut lines = input.lines().skip_while(|l| !l.is_empty());
        lines.next();
        self.instructions = lines
            .map(Instruction::from_str)
            .map(Result::unwrap)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut stacks = self.stacks.clone();
        for Instruction { count, to, from } in &self.instructions {
            for _ in 0..*count {
                let ch = stacks[*from - 1].pop().unwrap();
                stacks[*to - 1].push(ch);
            }
        }
        output!(stacks.iter().map(|s| s.last().unwrap()).collect::<String>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut stacks = self.stacks.clone();
        for Instruction { count, to, from } in &self.instructions {
            let mut chars = vec![];
            for _ in 0..*count {
                chars.push(stacks[*from - 1].pop().unwrap());
            }
            chars.reverse();
            stacks[*to - 1].extend_from_slice(&chars);
        }
        output!(stacks.iter().map(|s| s.last().unwrap()).collect::<String>())
    }

    fn name(&self) -> usize {
        5
    }
}
