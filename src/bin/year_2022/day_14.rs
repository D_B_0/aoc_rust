use std::collections::HashSet;

use aoc::{output, point::Point, Day};

#[derive(Default)]
pub struct Day14 {
    rock_bits: HashSet<Point<usize>>,
    sand_bits: HashSet<Point<usize>>,
    max_depth: usize,
}

impl Day14 {
    fn is_space_occupied(&self, p: &Point<usize>, part_2: bool) -> bool {
        if part_2 {
            p.y == self.max_depth + 2 || self.rock_bits.contains(p) || self.sand_bits.contains(p)
        } else {
            self.rock_bits.contains(p) || self.sand_bits.contains(p)
        }
    }

    #[allow(dead_code)]
    fn print_map(&self) {
        let min_x = self
            .rock_bits
            .union(&self.sand_bits)
            .fold(usize::MAX, |acc, p| acc.min(p.x));
        let max_x = self
            .rock_bits
            .union(&self.sand_bits)
            .fold(0, |acc, p| acc.max(p.x));
        let mut pt = Point::new(0, 0);
        for y in 0..=(self.max_depth + 2) {
            pt.y = y;
            for x in (min_x - 1)..=(max_x + 1) {
                pt.x = x;
                if self.rock_bits.contains(&pt) || y == self.max_depth + 2 {
                    print!("#");
                } else if self.sand_bits.contains(&pt) {
                    print!("o");
                } else {
                    print!(" ");
                }
            }
            println!();
        }
    }
}

impl Day for Day14 {
    fn initialize(&mut self, input: &str) {
        self.rock_bits = input
            .lines()
            .map(|l| {
                let points = l
                    .split(" -> ")
                    .map(|s| s.parse::<Point<usize>>().unwrap())
                    .collect::<Vec<_>>();
                let mut set = HashSet::new();
                for pts in points.windows(2) {
                    if pts[0].x == pts[1].x {
                        let min_y = pts[0].y.min(pts[1].y);
                        let max_y = pts[0].y.max(pts[1].y);
                        set.extend((min_y..=max_y).map(|y| Point::new(pts[0].x, y)));
                    } else {
                        let min_x = pts[0].x.min(pts[1].x);
                        let max_x = pts[0].x.max(pts[1].x);
                        set.extend((min_x..=max_x).map(|x| Point::new(x, pts[0].y)));
                    }
                }
                set
            })
            .flat_map(|set| set.into_iter())
            .collect::<HashSet<_>>();
        self.max_depth = self.rock_bits.iter().fold(0, |acc, p| acc.max(p.y));
    }

    fn part1(&mut self) -> Vec<String> {
        'outer: loop {
            let mut sand_p = Point::new(500, 0);
            loop {
                if sand_p.y > self.max_depth {
                    break 'outer;
                }
                // fall down
                sand_p.y += 1;
                if !self.is_space_occupied(&sand_p, false) {
                    // if free spot, continue falling
                    continue;
                }
                // else try falling left
                sand_p.x -= 1;
                if !self.is_space_occupied(&sand_p, false) {
                    // if free spot, continue falling
                    continue;
                }
                // else try falling right
                sand_p.x += 2;
                if !self.is_space_occupied(&sand_p, false) {
                    // if free spot, continue falling
                    continue;
                }
                // else reset sand, can't fall anymore
                sand_p.y -= 1;
                sand_p.x -= 1;
                break;
            }
            self.sand_bits.insert(sand_p);
        }
        output!(self.sand_bits.len())
    }

    fn part2(&mut self) -> Vec<String> {
        loop {
            let mut sand_p = Point::new(500, 0);
            if self.is_space_occupied(&sand_p, false) {
                break;
            }
            loop {
                // fall down
                sand_p.y += 1;
                if !self.is_space_occupied(&sand_p, true) {
                    // if free spot, continue falling
                    continue;
                }
                // else try falling left
                sand_p.x -= 1;
                if !self.is_space_occupied(&sand_p, true) {
                    // if free spot, continue falling
                    continue;
                }
                // else try falling right
                sand_p.x += 2;
                if !self.is_space_occupied(&sand_p, true) {
                    // if free spot, continue falling
                    continue;
                }
                // else reset sand, can't fall anymore
                sand_p.y -= 1;
                sand_p.x -= 1;
                break;
            }
            self.sand_bits.insert(sand_p);
        }
        output!(self.sand_bits.len())
    }

    fn name(&self) -> usize {
        14
    }
}
