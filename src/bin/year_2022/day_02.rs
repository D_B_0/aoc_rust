use aoc::{output, Day};

#[derive(Debug, Clone, Copy)]
enum Choice {
    Rock = 1,
    Paper = 2,
    Scissor = 3,
}

impl From<Sym> for Choice {
    fn from(s: Sym) -> Self {
        match s {
            Sym::A | Sym::X => Choice::Rock,
            Sym::B | Sym::Y => Choice::Paper,
            Sym::C | Sym::Z => Choice::Scissor,
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Outcome {
    Loss = 0,
    Draw = 3,
    Win = 6,
}

impl From<Sym> for Outcome {
    fn from(s: Sym) -> Self {
        match s {
            Sym::X => Outcome::Loss,
            Sym::Y => Outcome::Draw,
            Sym::Z => Outcome::Win,
            _ => panic!(),
        }
    }
}

impl Choice {
    fn outcome_against(&self, other: Choice) -> Outcome {
        match (self, other) {
            (Choice::Rock, Choice::Paper)
            | (Choice::Scissor, Choice::Rock)
            | (Choice::Paper, Choice::Scissor) => Outcome::Loss,

            (Choice::Rock, Choice::Rock)
            | (Choice::Paper, Choice::Paper)
            | (Choice::Scissor, Choice::Scissor) => Outcome::Draw,

            (Choice::Paper, Choice::Rock)
            | (Choice::Rock, Choice::Scissor)
            | (Choice::Scissor, Choice::Paper) => Outcome::Win,
        }
    }

    fn from_outcome(out: Outcome, ch: Choice) -> Choice {
        match (out, ch) {
            (Outcome::Loss, Choice::Paper)
            | (Outcome::Draw, Choice::Rock)
            | (Outcome::Win, Choice::Scissor) => Choice::Rock,

            (Outcome::Loss, Choice::Rock)
            | (Outcome::Draw, Choice::Scissor)
            | (Outcome::Win, Choice::Paper) => Choice::Scissor,

            (Outcome::Loss, Choice::Scissor)
            | (Outcome::Draw, Choice::Paper)
            | (Outcome::Win, Choice::Rock) => Choice::Paper,
        }
    }
}

#[derive(Clone, Copy)]
enum Sym {
    A,
    B,
    C,
    X,
    Y,
    Z,
}

struct Syms(Sym, Sym);

impl From<&str> for Syms {
    fn from(s: &str) -> Self {
        assert!(s.len() >= 3);
        let first = match s.chars().next().unwrap() {
            'A' => Sym::A,
            'B' => Sym::B,
            'C' => Sym::C,
            c => panic!("expected A, B or C, got {c}"),
        };
        let second = match s.chars().nth(2).unwrap() {
            'X' => Sym::X,
            'Y' => Sym::Y,
            'Z' => Sym::Z,
            c => panic!("expected X, Y or Z, got {c}"),
        };
        Syms(first, second)
    }
}

#[derive(Default)]
pub struct Day02 {
    instructions: Vec<Syms>,
}

impl Day for Day02 {
    fn initialize(&mut self, input: &str) {
        self.instructions = input.lines().map(Syms::from).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut sum = 0;
        for ss in &self.instructions {
            sum += Choice::from(ss.1) as u32
                + Choice::from(ss.1).outcome_against(Choice::from(ss.0)) as u32;
        }
        output!(sum)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut sum = 0;
        for ss in &self.instructions {
            let choice = Choice::from_outcome(Outcome::from(ss.1), Choice::from(ss.0));
            sum += choice as u32 + Outcome::from(ss.1) as u32;
        }
        output!(sum)
    }

    fn name(&self) -> usize {
        2
    }
}
