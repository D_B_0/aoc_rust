use std::{collections::HashMap, str::FromStr};

use aoc::{output, Day};

#[derive(Debug)]
enum Command {
    CD { arg: String },
    LS { output: Vec<Node> },
}

#[derive(Debug, Clone)]
#[allow(dead_code)]
enum Node {
    File { name: String, size: usize },
    Dir { name: String },
}

impl FromStr for Node {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("dir") {
            return Ok(Node::Dir {
                name: s[4..].to_owned(),
            });
        }
        let size = s
            .chars()
            .take_while(|c| char::is_numeric(*c))
            .collect::<String>()
            .parse::<usize>()
            .map_err(|e| format!("Node: Parsing `size` failed with error {e:?}"))?;
        let name = s
            .chars()
            .skip_while(|c| char::is_numeric(*c))
            .skip(1)
            .collect::<String>();
        Ok(Node::File { name, size })
    }
}

impl FromStr for Command {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("cd") {
            return Ok(Command::CD {
                arg: s[3..].to_owned(),
            });
        }
        if s.starts_with("ls") {
            return Ok(Command::LS {
                output: s
                    .lines()
                    .skip(1)
                    .map(Node::from_str)
                    .collect::<Result<Vec<_>, _>>()?,
            });
        }
        Err(format!(
            "Command: unrecognized starting pattern ({})",
            s[0..2].to_owned()
        ))
    }
}

#[derive(Debug)]
struct Directory {
    // name: String,
    files: Vec<Node>,
    dirs: HashMap<String, Directory>,
}

impl Directory {
    fn size(&self) -> usize {
        self.files
            .iter()
            .map(|f| match f {
                Node::File { name: _, size } => *size,
                Node::Dir { name: _ } => unreachable!(),
            })
            .sum::<usize>()
            + self.dirs.values().map(|v| v.size()).sum::<usize>()
    }

    fn sum_less_100000(d: &Directory) -> usize {
        let mut sum = 0;
        let size = d.size();
        if size < 100000 {
            sum += size;
        }
        for dir in d.dirs.values() {
            sum += Directory::sum_less_100000(dir);
        }
        sum
    }

    fn get_all_dir_sizes(&self) -> Vec<usize> {
        let mut res = vec![self.size()];
        for dir in self.dirs.values() {
            let mut sizes = Directory::get_all_dir_sizes(dir);
            res.append(&mut sizes);
        }
        res
    }
}

#[derive(Default)]
pub struct Day07 {
    file_system: Option<Directory>,
}

impl Day for Day07 {
    fn initialize(&mut self, input: &str) {
        let commands = input
            .split('$')
            .skip(1)
            .map(|s| {
                s.strip_prefix(' ')
                    .unwrap_or(s)
                    .strip_suffix('\n')
                    .unwrap_or(s)
            })
            .map(Command::from_str)
            .collect::<Result<Vec<_>, _>>()
            .unwrap();

        let mut current_path: Vec<String> = vec![];
        self.file_system = Some(Directory {
            files: vec![],
            dirs: HashMap::new(),
        });
        for command in &commands {
            let mut dir = self.file_system.as_mut().unwrap();
            for name in current_path.iter() {
                dir = dir.dirs.entry(name.clone()).or_insert(Directory {
                    files: vec![],
                    dirs: HashMap::new(),
                });
            }
            match command {
                Command::CD { arg } => {
                    if arg == "/" {
                        current_path = vec!["/".to_owned()];
                    } else if arg == ".." {
                        current_path.pop();
                    } else {
                        current_path.push(arg.clone());
                    }
                }
                Command::LS { output } => {
                    for node in output {
                        match node {
                            Node::File { name: _, size: _ } => dir.files.push(node.clone()),
                            Node::Dir { name } => {
                                if !dir.dirs.contains_key(name) {
                                    dir.dirs.insert(
                                        name.clone(),
                                        Directory {
                                            files: vec![],
                                            dirs: HashMap::new(),
                                        },
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        output!(Directory::sum_less_100000(
            self.file_system.as_ref().unwrap()
        ))
    }

    fn part2(&mut self) -> Vec<String> {
        const TOTAL_SPACE: usize = 70000000;
        const NEEDED_SPACE: usize = 30000000;
        let occupied_space = self.file_system.as_ref().unwrap().size();
        let space_currently_unused = TOTAL_SPACE - occupied_space;
        let space_to_free = NEEDED_SPACE - space_currently_unused;
        output!(self
            .file_system
            .as_ref()
            .unwrap()
            .get_all_dir_sizes()
            .iter()
            .filter(|&&s| s > space_to_free)
            .min()
            .unwrap())
    }

    fn name(&self) -> usize {
        7
    }
}
