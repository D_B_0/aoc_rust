use std::collections::HashSet;

use aoc::{output, Day};

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
struct Move {
    direction: Direction,
    ammount: usize,
}

impl From<&str> for Move {
    fn from(s: &str) -> Self {
        let (d, a) = s.split_once(' ').unwrap();
        let direction = match d.chars().next().unwrap() {
            'U' => Direction::Up,
            'D' => Direction::Down,
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => panic!(),
        };
        let ammount = a.parse::<usize>().unwrap();
        Move { direction, ammount }
    }
}

fn is_touching(head: (i64, i64), tail: (i64, i64)) -> bool {
    for dx in -1..=1 {
        for dy in -1..=1 {
            if tail == (head.0 + dx, head.1 + dy) {
                return true;
            }
        }
    }
    false
}

#[derive(Default)]
pub struct Day09 {
    moves: Vec<Move>,
}

impl Day for Day09 {
    fn initialize(&mut self, input: &str) {
        self.moves = input.lines().map(Move::from).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut positions = HashSet::new();
        let mut tail = (0, 0);
        let mut head = (0, 0);
        positions.insert(tail);
        for m in &self.moves {
            for _ in 0..m.ammount {
                match m.direction {
                    Direction::Up => head.1 += 1,
                    Direction::Down => head.1 -= 1,
                    Direction::Left => head.0 -= 1,
                    Direction::Right => head.0 += 1,
                }
                if !is_touching(head, tail) {
                    if tail.0 == head.0 {
                        if tail.1 > head.1 {
                            tail.1 -= 1;
                        } else {
                            tail.1 += 1;
                        }
                    } else if tail.1 == head.1 {
                        if tail.0 > head.0 {
                            tail.0 -= 1;
                        } else {
                            tail.0 += 1;
                        }
                    } else {
                        if tail.1 > head.1 {
                            tail.1 -= 1;
                        } else {
                            tail.1 += 1;
                        }
                        if tail.0 > head.0 {
                            tail.0 -= 1;
                        } else {
                            tail.0 += 1;
                        }
                    }
                }
                positions.insert(tail);
            }
        }
        output!(positions.len())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut positions = HashSet::new();
        let mut rope = [(0, 0); 10];
        positions.insert(rope[9]);
        for m in &self.moves {
            for _ in 0..m.ammount {
                match m.direction {
                    Direction::Up => rope[0].1 += 1,
                    Direction::Down => rope[0].1 -= 1,
                    Direction::Left => rope[0].0 -= 1,
                    Direction::Right => rope[0].0 += 1,
                }
                for i in 1..rope.len() {
                    if !is_touching(rope[i - 1], rope[i]) {
                        if rope[i].0 == rope[i - 1].0 {
                            if rope[i].1 > rope[i - 1].1 {
                                rope[i].1 -= 1;
                            } else {
                                rope[i].1 += 1;
                            }
                        } else if rope[i].1 == rope[i - 1].1 {
                            if rope[i].0 > rope[i - 1].0 {
                                rope[i].0 -= 1;
                            } else {
                                rope[i].0 += 1;
                            }
                        } else {
                            if rope[i].1 > rope[i - 1].1 {
                                rope[i].1 -= 1;
                            } else {
                                rope[i].1 += 1;
                            }
                            if rope[i].0 > rope[i - 1].0 {
                                rope[i].0 -= 1;
                            } else {
                                rope[i].0 += 1;
                            }
                        }
                    }
                }
                positions.insert(rope[9]);
            }
        }
        output!(positions.len())
    }

    fn name(&self) -> usize {
        9
    }
}
