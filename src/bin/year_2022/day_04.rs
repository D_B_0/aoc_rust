use std::{ops::RangeInclusive, str::FromStr};

use aoc::{output, Day};

struct Pair(RangeInclusive<usize>, RangeInclusive<usize>);

impl FromStr for Pair {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (left, right) = s.split_once(',').ok_or(())?;
        let (a1, b1) = left.split_once('-').ok_or(())?;
        let (a2, b2) = right.split_once('-').ok_or(())?;
        let (a1, b1) = (
            a1.parse::<usize>().map_err(|_| ())?,
            b1.parse::<usize>().map_err(|_| ())?,
        );
        let (a2, b2) = (
            a2.parse::<usize>().map_err(|_| ())?,
            b2.parse::<usize>().map_err(|_| ())?,
        );

        Ok(Pair(a1..=b1, a2..=b2))
    }
}

#[derive(Default)]
pub struct Day04 {
    pairs: Vec<Pair>,
}

fn ranges_included(p: &&Pair) -> bool {
    (p.0.contains(p.1.start()) && p.0.contains(p.1.end()))
        || (p.1.contains(p.0.start()) && p.1.contains(p.0.end()))
}

fn ranges_included_loose(p: &&Pair) -> bool {
    p.0.contains(p.1.start())
        || p.0.contains(p.1.end())
        || p.1.contains(p.0.start())
        || p.1.contains(p.0.end())
}

impl Day for Day04 {
    fn initialize(&mut self, input: &str) {
        self.pairs = input.lines().map(|l| Pair::from_str(l).unwrap()).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .pairs
            .iter()
            .filter(ranges_included)
            .fold(0, |acc, _| acc + 1))
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .pairs
            .iter()
            .filter(ranges_included_loose)
            .fold(0, |acc, _| acc + 1))
    }

    fn name(&self) -> usize {
        4
    }
}
