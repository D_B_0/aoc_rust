use std::collections::HashSet;

use aoc::{output, point::Point, Day};

#[derive(Default)]
pub struct Day15 {
    sensors: Vec<(Point<i32>, Point<i32>)>,
}

fn manhattan_distance(p1: &Point<i32>, p2: &Point<i32>) -> i32 {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

impl Day for Day15 {
    fn initialize(&mut self, input: &str) {
        self.sensors = input
            .lines()
            .map(|l| {
                let coords = l
                    .chars()
                    .skip_while(|c| c != &'x')
                    .take_while(|c| c != &':')
                    .collect::<String>();
                let (x, y) = coords.split_once(", ").unwrap();
                let sensor = Point::new(
                    x[2..].parse::<i32>().unwrap(),
                    y[2..].parse::<i32>().unwrap(),
                );
                let coords = l
                    .chars()
                    .skip_while(|c| c != &'x')
                    .skip(1)
                    .skip_while(|c| c != &'x')
                    .collect::<String>();
                let (x, y) = coords.split_once(", ").unwrap();
                let beacon = Point::new(
                    x[2..].parse::<i32>().unwrap(),
                    y[2..].parse::<i32>().unwrap(),
                );
                (sensor, beacon)
            })
            .collect::<_>();
    }

    fn part1(&mut self) -> Vec<String> {
        const Y: i32 = 2000000;
        let mut xs = HashSet::new();
        let mut x_to_exclude = None;
        for (s, b) in &self.sensors {
            let dist = manhattan_distance(s, b);
            if b.y == Y {
                x_to_exclude = Some(b.x);
            }
            let y_dist = (Y - s.y).abs();
            for dx in 0..=(dist - y_dist) {
                xs.insert(s.x + dx);
                xs.insert(s.x - dx);
            }
        }
        let adjust = if let Some(x) = x_to_exclude {
            usize::from(xs.contains(&x))
        } else {
            0
        };
        output!(xs.len() - adjust)
    }

    fn part2(&mut self) -> Vec<String> {
        let points_to_try = self.sensors.iter().flat_map(|(s, b)| {
            let dist = manhattan_distance(s, b) + 1;
            let mut pts = vec![];
            for y in s.y - dist..=s.y + dist {
                if !(0..=4000000).contains(&y) {
                    continue;
                }
                let x_dist = dist - (y - s.y).abs();
                if x_dist == 0 {
                    if s.x >= 0 && s.x <= 4000000 {
                        pts.push(Point::new(s.x, y));
                    }
                } else {
                    if s.x + x_dist >= 0 && s.x + x_dist <= 4000000 {
                        pts.push(Point::new(s.x + x_dist, y));
                    }
                    if s.x - x_dist >= 0 && s.x - x_dist <= 4000000 {
                        pts.push(Point::new(s.x - x_dist, y));
                    }
                }
            }
            pts.into_iter()
        });
        // println!("generated points to check");
        'next_point: for p in points_to_try {
            for (s, b) in &self.sensors {
                let dist = manhattan_distance(s, b);
                if manhattan_distance(&p, s) <= dist {
                    continue 'next_point;
                }
            }
            return output!(p.x as usize * 4000000 + p.y as usize);
        }
        output!("no solution found")
    }

    fn name(&self) -> usize {
        15
    }
}
