use aoc::{output, Day};

#[derive(Debug, Clone, Copy)]
enum Op {
    Noop,
    AddX(i64),
}

impl From<&str> for Op {
    fn from(s: &str) -> Self {
        let (op, args) = if let Some(pair) = s.split_once(' ') {
            pair
        } else {
            (s, "")
        };
        match op {
            "noop" => Op::Noop,
            "addx" => Op::AddX(args.parse().unwrap()),
            _ => panic!(),
        }
    }
}

impl Op {
    fn cycles_taken(&self) -> usize {
        match self {
            Op::Noop => 1,
            Op::AddX(_) => 2,
        }
    }

    fn execute(&self, cpu: &mut Cpu) {
        match self {
            Op::Noop => {}
            Op::AddX(val) => cpu.x += *val,
        }
    }
}

#[derive(Clone)]
struct Cpu {
    x: i64,
    ops: Vec<Op>,
    ip: usize,
    cycles_since_last_op: usize,
    cycles: usize,
}

impl Cpu {
    fn cycle(&mut self) -> Result<(), ()> {
        if self.has_finished() {
            return Err(());
        }
        self.cycles_since_last_op += 1;
        self.cycles += 1;
        if self.ops[self.ip].cycles_taken() <= self.cycles_since_last_op {
            let op = self.ops[self.ip];
            op.execute(self);
            self.cycles_since_last_op = 0;
            self.ip += 1;
        }
        Ok(())
    }

    fn has_finished(&self) -> bool {
        self.ip >= self.ops.len()
    }
}

impl Default for Cpu {
    fn default() -> Cpu {
        Cpu {
            x: 1,
            ops: Vec::new(),
            ip: 0,
            cycles_since_last_op: 0,
            cycles: 0,
        }
    }
}

#[derive(Default)]
pub struct Day10 {
    cpu: Cpu,
}

impl Day for Day10 {
    fn initialize(&mut self, input: &str) {
        self.cpu.ops = input.lines().map(Op::from).collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut sum = 0;
        let mut cpu = self.cpu.clone();
        while cpu.cycle().is_ok() {
            if (cpu.cycles + 20) % 40 == 39 {
                sum += i64::try_from(cpu.cycles + 1).unwrap() * cpu.x;
            }
        }
        output!(sum)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut crt = [' '; 240];
        let mut cpu = self.cpu.clone();
        loop {
            if cpu.cycles == 240 {
                break;
            }
            if (i64::try_from(cpu.cycles % 40).unwrap() - cpu.x).abs() <= 1 {
                crt[cpu.cycles] = '#';
            } else {
                crt[cpu.cycles] = '.';
            }
            if cpu.cycle().is_err() {
                break;
            }
        }
        let mut out = vec![String::new(); 6];
        for (i, c) in crt.iter().enumerate() {
            out[i / 40].push(*c);
        }
        out
    }

    fn name(&self) -> usize {
        10
    }
}
