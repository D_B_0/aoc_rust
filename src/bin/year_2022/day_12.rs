use std::collections::HashSet;

use aoc::{output, Day};

struct Node {
    position: (usize, usize),
    height: u8,
    connections: Vec<usize>,
}

fn neighbors(i: usize, j: usize, i_max: usize, j_max: usize) -> Vec<(usize, usize)> {
    let mut res = vec![];
    if i != 0 {
        res.push((i - 1, j));
    }
    if i != i_max - 1 {
        res.push((i + 1, j));
    }
    if j != 0 {
        res.push((i, j - 1));
    }
    if j != j_max - 1 {
        res.push((i, j + 1));
    }
    res
}

fn dijkstra(map: &[Node], start: (usize, usize), end: (usize, usize)) -> Option<usize> {
    let start_idx = map.iter().position(|n| n.position == start)?;
    let end_idx = map.iter().position(|n| n.position == end)?;
    let mut unvisited = (0..map.len())
        .filter(|&i| i != start_idx)
        .collect::<HashSet<_>>();
    let mut tentative_distance = vec![None; map.len()];
    tentative_distance[start_idx] = Some(0_usize);
    let mut current = start_idx;

    loop {
        if current == end_idx {
            return tentative_distance[end_idx];
        }
        for &neighbor in &map[current].connections {
            if tentative_distance[neighbor].is_none()
                || tentative_distance[current].unwrap() + 1 < tentative_distance[neighbor].unwrap()
            {
                tentative_distance[neighbor] = tentative_distance[current].map(|d| d + 1);
                // comes from...
            }
        }
        let mut min = usize::MAX;
        let mut new = None;
        for (index, distance) in tentative_distance.iter().enumerate() {
            if distance.is_some() && distance.unwrap() < min && unvisited.contains(&index) {
                min = distance.unwrap();
                new = Some(index);
            }
        }
        let new = new?;
        unvisited.remove(&new);
        current = new;
    }
}

#[derive(Default)]
pub struct Day12 {
    start_pos: (usize, usize),
    end_pos: (usize, usize),
    map: Vec<Node>,
    map_height: usize,
}

impl Day for Day12 {
    fn initialize(&mut self, input: &str) {
        let mut grid = input
            .lines()
            .map(|s| {
                s.chars()
                    .map(|c| match c {
                        'S' => 50,
                        'E' => 51,
                        c => c as u8 - b'a',
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        self.map_height = grid.len();
        for j in 0..grid.len() {
            for i in 0..grid[0].len() {
                if grid[j][i] == 50 {
                    self.start_pos = (i, j);
                    grid[j][i] = 0;
                } else if grid[j][i] == 51 {
                    self.end_pos = (i, j);
                    grid[j][i] = b'z' - b'a';
                }
            }
        }
        for j in 0..grid.len() {
            for i in 0..grid[0].len() {
                self.map.push(Node {
                    position: (i, j),
                    height: grid[j][i],
                    connections: vec![],
                });
            }
        }
        for j in 0..grid.len() {
            for i in 0..grid[0].len() {
                let pos = self.map.iter().position(|n| n.position == (i, j)).unwrap();
                for (i_n, j_n) in neighbors(i, j, grid[0].len(), grid.len()) {
                    let pos_n = self
                        .map
                        .iter()
                        .position(|n| n.position == (i_n, j_n))
                        .unwrap();
                    if self.map[pos].height >= self.map[pos_n].height
                        || self.map[pos_n].height - self.map[pos].height <= 1
                    {
                        self.map[pos].connections.push(pos_n);
                    }
                }
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        output!(dijkstra(&self.map, self.start_pos, self.end_pos).unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        output!((0..self.map_height)
            .filter_map(|j| dijkstra(&self.map, (0, j), self.end_pos))
            .min()
            .unwrap())
    }

    fn name(&self) -> usize {
        12
    }
}
