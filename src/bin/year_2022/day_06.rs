use aoc::{output, Day};

#[derive(Default)]
pub struct Day06 {
    stream: Vec<char>,
}

impl Day for Day06 {
    fn initialize(&mut self, input: &str) {
        self.stream = input.chars().collect();
    }

    fn part1(&mut self) -> Vec<String> {
        for (i, win) in self.stream.windows(4).enumerate() {
            if win[0] != win[1]
                && win[0] != win[2]
                && win[0] != win[3]
                && win[1] != win[2]
                && win[1] != win[3]
                && win[2] != win[3]
            {
                return output!(i + 4);
            }
        }
        output!("failed to solve")
    }

    fn part2(&mut self) -> Vec<String> {
        'outer: for (i, win) in self.stream.windows(14).enumerate() {
            for i in 0..13 {
                for j in (i + 1)..14 {
                    if win[i] == win[j] {
                        continue 'outer;
                    }
                }
            }
            return output!(i + 14);
        }
        output!("failed to solve")
    }

    fn name(&self) -> usize {
        6
    }
}
