use aoc::{output, Day};

#[derive(Default)]
pub struct Day01 {
    calories: Vec<usize>,
}

impl Day for Day01 {
    fn initialize(&mut self, input: &str) {
        self.calories = input.lines().fold(vec![0], |mut acc, s| {
            if s.is_empty() {
                acc.push(0);
            } else {
                let val = s.parse::<usize>().unwrap();
                *acc.last_mut().unwrap() += val;
            }
            acc
        });
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.calories.iter().max().unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        self.calories.sort();
        output!(self.calories.iter().rev().take(3).sum::<usize>())
    }

    fn name(&self) -> usize {
        1
    }
}
