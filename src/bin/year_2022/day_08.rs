use aoc::{output, Day, Grid};

#[derive(Default)]
pub struct Day08 {
    grid: Grid<i8>,
}

impl Day for Day08 {
    fn initialize(&mut self, input: &str) {
        for (j, l) in input.lines().enumerate() {
            for (i, c) in l.char_indices() {
                self.grid.insert(i, j, c as i8 - b'0' as i8)
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        let mut count = 0;
        for j in 0..self.grid.height() {
            for i in 0..self.grid.width() {
                let mut max_right = -1_i8;
                for di in (i + 1)..self.grid.width() {
                    max_right = max_right.max(*self.grid.get(di, j).unwrap())
                }
                let mut max_left = -1_i8;
                for di in 0..i {
                    max_left = max_left.max(*self.grid.get(di, j).unwrap())
                }
                let mut max_below = -1_i8;
                for dj in (j + 1)..self.grid.height() {
                    max_below = max_below.max(*self.grid.get(i, dj).unwrap());
                }
                let mut max_above = -1_i8;
                for dj in 0..j {
                    max_above = max_above.max(*self.grid.get(i, dj).unwrap());
                }
                let val = *self.grid.get(i, j).unwrap();
                if val > max_right || val > max_left || val > max_below || val > max_above {
                    count += 1;
                }
            }
        }
        output!(count)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut best = 0;
        for j in 0..self.grid.height() {
            for i in 0..self.grid.width() {
                let mut count_right = 0_usize;
                for di in (i + 1)..self.grid.width() {
                    count_right += 1;
                    if self.grid.get(i, j).unwrap() <= self.grid.get(di, j).unwrap() {
                        break;
                    }
                }
                let mut count_left = 0_usize;
                for di in (0..i).rev() {
                    count_left += 1;
                    if self.grid.get(i, j).unwrap() <= self.grid.get(di, j).unwrap() {
                        break;
                    }
                }
                let mut count_below = 0_usize;
                for dj in (j + 1)..self.grid.height() {
                    count_below += 1;
                    if self.grid.get(i, j).unwrap() <= self.grid.get(i, dj).unwrap() {
                        break;
                    }
                }
                let mut count_above = 0_usize;
                for dj in (0..j).rev() {
                    count_above += 1;
                    if self.grid.get(i, j).unwrap() <= self.grid.get(i, dj).unwrap() {
                        break;
                    }
                }
                let score = count_above * count_below * count_left * count_right;
                best = best.max(score);
            }
        }
        output!(best)
    }

    fn name(&self) -> usize {
        8
    }
}
