use std::str::FromStr;

use aoc::{output, Day};

#[derive(Debug, Clone)]
enum Operation {
    Plus,
    Times,
}

#[derive(Debug, Clone)]
enum Operands {
    Num(usize),
    Old,
}

#[derive(Debug, Clone)]
struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    operands: [Operands; 2],
    test_div: usize,
    test_true: usize,
    test_false: usize,
}

impl FromStr for Monkey {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        // Monkey n:
        lines.next().ok_or(())?;
        //   Starting items: n1, n2, ...
        let (_, starting_items) = lines.next().ok_or(())?.split_once(": ").ok_or(())?;
        let starting_items = starting_items
            .split(", ")
            .map(str::parse::<usize>)
            .collect::<Result<Vec<usize>, _>>()
            .map_err(|_| ())?;
        //   Operation: new = something * something
        let (_, operation) = lines.next().ok_or(())?.split_once("= ").ok_or(())?;
        let tokens = operation.split_whitespace().collect::<Vec<_>>();
        if tokens.len() != 3 {
            return Err(());
        }
        let operation = match tokens[1] {
            "+" => Operation::Plus,
            "*" => Operation::Times,
            _ => return Err(()),
        };
        let operands = [
            match tokens[0] {
                "old" => Operands::Old,
                s => Operands::Num(s.parse::<usize>().map_err(|_| ())?),
            },
            match tokens[2] {
                "old" => Operands::Old,
                s => Operands::Num(s.parse::<usize>().map_err(|_| ())?),
            },
        ];
        //   Test: divisible by n
        let (_, div) = lines
            .next()
            .ok_or(())?
            .split_once("divisible by ")
            .ok_or(())?;
        let test_div = div.parse::<usize>().map_err(|_| ())?;
        let (_, num) = lines
            .next()
            .ok_or(())?
            .split_once("throw to monkey ")
            .ok_or(())?;
        let test_true = num.parse::<usize>().map_err(|_| ())?;
        let (_, num) = lines
            .next()
            .ok_or(())?
            .split_once("throw to monkey ")
            .ok_or(())?;
        let test_false = num.parse::<usize>().map_err(|_| ())?;

        Ok(Monkey {
            items: starting_items,
            operation,
            operands,
            test_div,
            test_true,
            test_false,
        })
    }
}

#[derive(Default)]
pub struct Day11 {
    monkeys: Vec<Monkey>,
}

impl Day for Day11 {
    fn initialize(&mut self, input: &str) {
        self.monkeys = input
            .split("\n\n")
            .map(Monkey::from_str)
            .map(Result::unwrap)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut counts = vec![0; self.monkeys.len()];
        let mut monkeys = self.monkeys.clone();
        for _ in 0..20 {
            for i in 0..monkeys.len() {
                while let Some(worry) = monkeys[i].items.pop() {
                    counts[i] += 1;
                    let a = match monkeys[i].operands[0] {
                        Operands::Num(n) => n,
                        Operands::Old => worry,
                    };
                    let b = match monkeys[i].operands[1] {
                        Operands::Num(n) => n,
                        Operands::Old => worry,
                    };
                    let new_worry = match monkeys[i].operation {
                        Operation::Plus => a + b,
                        Operation::Times => a * b,
                    } / 3;
                    if new_worry % monkeys[i].test_div == 0 {
                        let next_monkey = monkeys[i].test_true;
                        monkeys[next_monkey].items.insert(0, new_worry);
                    } else {
                        let next_monkey = monkeys[i].test_false;
                        monkeys[next_monkey].items.insert(0, new_worry);
                    }
                }
            }
        }
        counts.sort();
        counts.reverse();
        output!(counts[0] * counts[1])
    }

    fn part2(&mut self) -> Vec<String> {
        let mut counts = vec![0; self.monkeys.len()];
        let mut monkeys = self.monkeys.clone();
        let common_multiple = self
            .monkeys
            .iter()
            .map(|m| m.test_div)
            .reduce(|acc, val| acc * val)
            .unwrap();
        for _ in 1..=10_000 {
            for i in 0..monkeys.len() {
                while let Some(worry) = monkeys[i].items.pop() {
                    counts[i] += 1;
                    let a = match monkeys[i].operands[0] {
                        Operands::Num(n) => n,
                        Operands::Old => worry,
                    };
                    let b = match monkeys[i].operands[1] {
                        Operands::Num(n) => n,
                        Operands::Old => worry,
                    };
                    let new_worry = match monkeys[i].operation {
                        Operation::Plus => a + b,
                        Operation::Times => a * b,
                    };
                    let next_monkey = if new_worry % monkeys[i].test_div == 0 {
                        monkeys[i].test_true
                    } else {
                        monkeys[i].test_false
                    };
                    monkeys[next_monkey]
                        .items
                        .insert(0, new_worry % common_multiple);
                }
            }
        }
        counts.sort();
        counts.reverse();
        output!(counts[0] as u128 * counts[1] as u128)
    }

    fn name(&self) -> usize {
        11
    }
}
