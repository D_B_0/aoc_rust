use std::collections::{HashMap, HashSet};

use aoc::{output, Day};
use regex::Regex;

fn distances(from: &str, connections: &HashMap<String, Vec<String>>) -> HashMap<String, usize> {
    let mut dist = HashMap::new();
    dist.insert(from.to_owned(), 0);
    let mut nodes = connections.keys().collect::<HashSet<_>>();
    while !nodes.is_empty() {
        let (u, &dist_u) = dist
            .iter()
            .filter(|(k, _)| nodes.contains(*k))
            .min_by(|(_, d1), (_, d2)| d1.cmp(d2))
            .unwrap();
        nodes.remove(u);
        for n in &connections[u] {
            let new_dist = dist_u + 1;
            if new_dist < *dist.get(n).unwrap_or(&usize::MAX) {
                dist.insert(n.clone(), new_dist);
            }
        }
    }
    dist
}

#[derive(Default)]
pub struct Day16 {
    valves: Vec<String>,
    important_valves: Vec<String>,
    rates: HashMap<String, usize>,
    distance_matrix: HashMap<String, HashMap<String, usize>>,
}

impl Day16 {
    fn max_flow(&self, pos: &str, time: usize, open_valves: &mut HashSet<String>) -> usize {
        let possible_destinations = self
            .important_valves
            .iter()
            .filter(|v| !open_valves.contains(*v))
            .collect::<Vec<_>>();
        let mut max = 0;

        for v in possible_destinations {
            let dist = *self
                .distance_matrix
                .get(pos)
                .unwrap_or(&HashMap::new())
                .get(v)
                .unwrap_or(&usize::MAX);
            if dist < time - 1 {
                let new_time = time - dist - 1;
                let mut flow = new_time * self.rates.get(v).unwrap();
                // i have time to go there and open the valve
                open_valves.insert(v.clone());
                // now where do i go?
                flow += self.max_flow(v, new_time, open_valves);
                // was this good?
                max = max.max(flow);
                // ok, now i try going somewhere else, i have to close this valve
                open_valves.remove(v);
            }
        }
        max
    }

    #[allow(dead_code)]
    fn max_flow_2(
        &self,
        pos1: &str,
        pos2: &str,
        time_1: usize,
        time_2: usize,
        open_valves: &mut HashSet<String>,
    ) -> usize {
        let possible_destinations = self
            .important_valves
            .iter()
            .filter(|v| !open_valves.contains(*v))
            .collect::<Vec<_>>();
        let mut max = 0;

        for &v1 in &possible_destinations {
            let dist_1 = *self
                .distance_matrix
                .get(pos1)
                .unwrap_or(&HashMap::new())
                .get(v1)
                .unwrap_or(&usize::MAX);
            let mut new_time_1 = 0;
            let mut flow = 0;
            if dist_1 < time_1.saturating_sub(1) {
                new_time_1 = time_1 - dist_1 - 1;
                flow = new_time_1 * self.rates.get(v1).unwrap();
                // i have time to go there and open the valve
                open_valves.insert(v1.clone());
            }
            let mut max_inner = 0;
            for &v2 in &possible_destinations {
                let dist_2 = *self
                    .distance_matrix
                    .get(pos2)
                    .unwrap_or(&HashMap::new())
                    .get(v2)
                    .unwrap_or(&usize::MAX);
                if dist_2 >= time_2 - 1 {
                    continue;
                }
                let new_time_2 = time_2 - dist_2 - 1;
                let mut flow = new_time_2 * self.rates.get(v2).unwrap();
                // the elephant has time to go there and open the valve
                open_valves.insert(v2.clone());
                // now where do we go?
                flow += self.max_flow_2(v1, v2, new_time_1, new_time_2, open_valves);
                // was this good?
                max_inner = max_inner.max(flow);
                // ok, now the elephant tries to go somewhere else, they have to close this valve
                open_valves.remove(v2);
            }
            // was this good?
            max = max.max(flow + max_inner);
            // ok, now i try to go somewhere else, i have to close this valve
            open_valves.remove(v1);
        }
        max
    }
}

impl Day for Day16 {
    fn initialize(&mut self, input: &str) {
        let re = Regex::new(
            "Valve ([A-Z]{2}) has flow rate=(\\d+); tunnels? leads? to valves? ((?:[A-Z]{2},? ?)+)",
        )
        .unwrap();
        let mut connections = HashMap::new();
        for cap in re.captures_iter(input) {
            let valve_name = cap.get(1).unwrap().as_str().to_owned();
            let flow_rate = cap.get(2).unwrap().as_str().parse::<usize>().unwrap();
            let connected_valves = cap
                .get(3)
                .unwrap()
                .as_str()
                .split(", ")
                .map(|s| s.to_owned())
                .collect::<Vec<_>>();
            self.valves.push(valve_name.clone());
            self.rates.insert(valve_name.clone(), flow_rate);
            connections.insert(valve_name, connected_valves);
        }
        for val1 in &self.valves {
            let dist = distances(val1, &connections);
            self.distance_matrix.insert(val1.clone(), dist);
        }
        self.important_valves = self
            .valves
            .iter()
            .filter(|&v| self.rates[v] != 0)
            .cloned()
            .collect::<Vec<_>>();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.max_flow("AA", 30, &mut HashSet::new()))
    }

    fn part2(&mut self) -> Vec<String> {
        output!("unsolved :(")
        // output!(self.max_flow_2("AA", "AA", 26, 26, &mut HashSet::new()))
    }

    fn name(&self) -> usize {
        16
    }
}
