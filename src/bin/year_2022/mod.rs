use aoc::{Day, Year};

use proc_macro::{days, import_days};

import_days!(1..=16);

#[derive(Default)]
pub(crate) struct Year2022 {}

impl Year for Year2022 {
    fn days(&self) -> Vec<Box<dyn Day>> {
        days!(1..=16)
    }

    fn name(&self) -> usize {
        2022
    }
}
