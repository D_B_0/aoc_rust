use aoc::{output, Day};

#[derive(Debug)]
enum Command {
    Insert(String, usize),
    Remove(String),
}

impl TryFrom<&str> for Command {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let label = value
            .chars()
            .take_while(char::is_ascii_alphabetic)
            .collect();
        let mut stream = value.chars().skip_while(char::is_ascii_alphabetic);
        match stream.next() {
            Some('=') => Ok(Command::Insert(
                label,
                stream
                    .collect::<String>()
                    .parse::<usize>()
                    .map_err(|_| ())?,
            )),
            Some('-') => Ok(Command::Remove(label)),
            Some(_) | None => Err(()),
        }
    }
}

struct MyHashMap {
    bins: [Vec<(String, usize)>; 256],
}

impl MyHashMap {
    fn new() -> MyHashMap {
        MyHashMap {
            bins: std::array::from_fn(|_| Vec::new()),
        }
    }

    fn insert(&mut self, key: String, value: usize) {
        let idx = hash(&key) as usize;
        if let Some(found_idx) = self.bins[idx].iter().position(|(k, _)| k == &key) {
            self.bins[idx][found_idx].1 = value;
        } else {
            self.bins[idx].push((key, value));
        }
    }

    fn remove(&mut self, key: String) {
        let idx = hash(&key) as usize;
        if let Some(found_idx) = self.bins[idx].iter().position(|(k, _)| k == &key) {
            self.bins[idx].remove(found_idx);
        }
    }
}

#[derive(Default)]
pub struct Day15 {
    steps_1: Vec<String>,
    steps_2: Vec<Command>,
}

impl Day for Day15 {
    fn initialize(&mut self, input: &str) {
        self.steps_1 = input.trim().split(',').map(str::to_owned).collect();
        self.steps_2 = input
            .trim()
            .split(',')
            .map(Command::try_from)
            .collect::<Result<_, _>>()
            .unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .steps_1
            .iter()
            .map(hash)
            .map(|v| v as usize)
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut map = MyHashMap::new();
        for command in &self.steps_2 {
            match command {
                Command::Insert(key, value) => map.insert(key.clone(), *value),
                Command::Remove(key) => map.remove(key.clone()),
            }
        }

        output!(map
            .bins
            .iter()
            .enumerate()
            .flat_map(|(bin_idx, bin)| bin
                .iter()
                .enumerate()
                .map(move |(idx, (_, value))| (bin_idx + 1) * (idx + 1) * value))
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        15
    }
}

fn hash(string: impl ToString) -> u8 {
    let mut value = 0u8;
    for ch in string.to_string().chars() {
        value = value.wrapping_add(ch as u8).wrapping_mul(17);
    }
    value
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn hash_func() {
        assert_eq!(hash("HASH"), 52);
    }

    #[test]
    fn example_input() {
        let mut day = Day15::default();
        day.initialize("rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7\n");
        assert_eq!(day.part1(), vec!["1320"]);
        assert_eq!(day.part2(), vec!["145"]);
    }
}
