use aoc::{output, Day};

#[derive(Default)]
pub struct Day06 {
    times: Vec<i64>,
    records: Vec<i64>,
}

#[allow(dead_code)]
const TEST: &str = "Time:      7  15   30
Distance:  9  40  200";

impl Day for Day06 {
    fn initialize(&mut self, input: &str) {
        let mut input = input.lines();
        self.times = input
            .next()
            .unwrap()
            .strip_prefix("Time:")
            .unwrap()
            .split_whitespace()
            .map(str::parse::<i64>)
            .map(Result::unwrap)
            .collect();
        self.records = input
            .next()
            .unwrap()
            .strip_prefix("Distance:")
            .unwrap()
            .split_whitespace()
            .map(str::parse::<i64>)
            .map(Result::unwrap)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .times
            .iter()
            .zip(self.records.iter())
            .map(get_count)
            .fold(1, |acc, v| v * acc))
    }

    fn part2(&mut self) -> Vec<String> {
        let time = self
            .times
            .iter()
            .map(i64::to_string)
            .collect::<String>()
            .parse::<i64>()
            .unwrap();
        let record = self
            .records
            .iter()
            .map(i64::to_string)
            .collect::<String>()
            .parse::<i64>()
            .unwrap();
        output!(get_count((&time, &record)))
    }

    fn name(&self) -> usize {
        6
    }
}

fn get_count((&time, &record): (&i64, &i64)) -> usize {
    let mut count = 0;
    for t in 0..time {
        if t * (time - t) > record {
            count += 1;
        }
    }
    count
}
