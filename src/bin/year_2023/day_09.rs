use aoc::{output, Day};

#[derive(Default)]
pub struct Day09 {
    data: Vec<Vec<i64>>,
}

fn calc_next(data: &[i64]) -> i64 {
    let mut lasts = Vec::new();
    let mut data = data.to_vec();
    while !data.iter().all(|&d| d == 0) {
        lasts.push(*data.last().unwrap());
        data = deltas(&data);
    }
    // dbg!(&lasts);
    lasts.iter().sum()
}

fn calc_prev(data: &[i64]) -> i64 {
    let mut firsts = Vec::new();
    let mut data = data.to_vec();
    while !data.iter().all(|&d| d == 0) {
        firsts.push(*data.first().unwrap());
        data = deltas(&data);
    }
    firsts.iter().rev().fold(0, |acc, e| e - acc)
}

fn deltas(data: &[i64]) -> Vec<i64> {
    data.windows(2).map(|w| w[1] - w[0]).collect()
}

impl Day for Day09 {
    fn initialize(&mut self, input: &str) {
        self.data = input
            .lines()
            .map(|line| {
                line.split_whitespace()
                    .map(str::parse)
                    .map(Result::unwrap)
                    .collect()
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.data.iter().map(|d| calc_next(d)).sum::<i64>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self.data.iter().map(|d| calc_prev(d)).sum::<i64>())
    }

    fn name(&self) -> usize {
        9
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_calc_next() {
        assert_eq!(calc_next(&[0, 3, 6, 9, 12, 15]), 18);
        assert_eq!(calc_next(&[1, 3, 6, 10, 15, 21]), 28);
        assert_eq!(calc_next(&[10, 13, 16, 21, 30, 45]), 68);
    }

    #[test]
    fn test_calc_prev() {
        assert_eq!(calc_prev(&[0, 3, 6, 9, 12, 15]), -3);
        assert_eq!(calc_prev(&[1, 3, 6, 10, 15, 21]), 0);
        assert_eq!(calc_prev(&[10, 13, 16, 21, 30, 45]), 5);
    }
}
