use std::collections::HashMap;

use aoc::{output, Day};

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy, PartialOrd, Ord)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

impl Card {
    fn maybe_from_char(ch: char) -> Option<Card> {
        match ch {
            'A' => Some(Card::Ace),
            'K' => Some(Card::King),
            'Q' => Some(Card::Queen),
            'J' => Some(Card::Jack),
            'T' => Some(Card::Ten),
            '9' => Some(Card::Nine),
            '8' => Some(Card::Eight),
            '7' => Some(Card::Seven),
            '6' => Some(Card::Six),
            '5' => Some(Card::Five),
            '4' => Some(Card::Four),
            '3' => Some(Card::Three),
            '2' => Some(Card::Two),
            _ => None,
        }
    }

    fn jack_to_joker(&mut self) {
        if *self == Card::Jack {
            *self = Card::Joker;
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum Type {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug, PartialEq, Eq)]
struct Hand([Card; 5]);

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.0 == other.0 {
            return std::cmp::Ordering::Equal;
        }
        if self.get_type() != other.get_type() {
            return self.get_type().cmp(&other.get_type());
        }
        for (left, right) in self.0.iter().zip(other.0.iter()) {
            if left != right {
                return left.cmp(right);
            }
        }
        unreachable!()
    }
}

impl Hand {
    fn maybe_from_str(s: &str) -> Option<Hand> {
        let s = s.trim();
        let mut chars = s.chars();
        Some(Hand([
            Card::maybe_from_char(chars.next()?)?,
            Card::maybe_from_char(chars.next()?)?,
            Card::maybe_from_char(chars.next()?)?,
            Card::maybe_from_char(chars.next()?)?,
            Card::maybe_from_char(chars.next()?)?,
        ]))
    }

    fn get_type(&self) -> Type {
        let mut map = HashMap::new();
        for card in &self.0 {
            *map.entry(*card).or_insert(0u8) += 1;
        }
        let mut count_2s = 0u8;
        let mut count_3s = 0u8;
        let mut count_4s = 0u8;
        let mut count_5s = 0u8;
        let mut count_jokers = 0;
        for (card, count) in map {
            if card != Card::Joker {
                if count == 5 {
                    count_5s += 1;
                } else if count == 4 {
                    count_4s += 1;
                } else if count == 3 {
                    count_3s += 1;
                } else if count == 2 {
                    count_2s += 1;
                }
            } else {
                count_jokers = count;
            }
        }
        while count_jokers > 0 {
            if count_4s > 0 {
                count_4s -= 1;
                count_5s += 1;
            } else if count_3s > 0 {
                count_3s -= 1;
                count_4s += 1;
            } else if count_2s > 0 {
                count_2s -= 1;
                count_3s += 1;
            } else {
                count_2s += 1;
            }
            count_jokers -= 1;
        }
        if count_5s > 0 {
            return Type::FiveOfAKind;
        }
        if count_4s > 0 {
            return Type::FourOfAKind;
        }
        match (count_2s, count_3s) {
            (0, 0) => Type::HighCard,
            (1, 0) => Type::OnePair,
            (2, 0) => Type::TwoPair,
            (1, 1) => Type::FullHouse,
            (_, 1) => Type::ThreeOfAKind,
            (_, _) => Type::HighCard,
        }
    }

    fn jack_to_joker(&mut self) {
        self.0.iter_mut().for_each(Card::jack_to_joker);
    }
}

#[derive(Default)]
pub struct Day07 {
    hands: Vec<(Hand, u16)>,
}

impl Day07 {
    fn get_wins(&mut self) -> usize {
        self.hands
            .sort_by(|(hand1, _), (hand2, _)| hand1.cmp(hand2));
        self.hands
            .iter()
            .enumerate()
            .map(|(i, (_, bet))| *bet as usize * (i + 1))
            .sum::<usize>()
    }
}

#[allow(dead_code)]
const TEST: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

impl Day for Day07 {
    fn initialize(&mut self, input: &str) {
        self.hands = input
            .lines()
            .map(|line| {
                let mut split = line.split_whitespace();
                (
                    Hand::maybe_from_str(split.next().unwrap()).unwrap(),
                    split.next().unwrap().parse::<u16>().unwrap(),
                )
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.get_wins())
    }

    fn part2(&mut self) -> Vec<String> {
        self.hands
            .iter_mut()
            .for_each(|(hand, _)| hand.jack_to_joker());
        output!(self.get_wins())
    }

    fn name(&self) -> usize {
        7
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct_types() {
        assert_eq!(
            Hand::maybe_from_str("32T3K").unwrap().get_type(),
            Type::OnePair
        );
        assert_eq!(
            Hand::maybe_from_str("T55J5").unwrap().get_type(),
            Type::ThreeOfAKind
        );
        assert_eq!(
            Hand::maybe_from_str("KK677").unwrap().get_type(),
            Type::TwoPair
        );
        assert_eq!(
            Hand::maybe_from_str("KTJJT").unwrap().get_type(),
            Type::TwoPair
        );
        assert_eq!(
            Hand::maybe_from_str("QQQJA").unwrap().get_type(),
            Type::ThreeOfAKind
        );
    }

    #[test]
    fn correct_order() {
        let one_pair = Hand::maybe_from_str("32T3K").unwrap();
        let three_of_a_kind = Hand::maybe_from_str("T55J5").unwrap();
        assert!(three_of_a_kind == three_of_a_kind);
        assert!(three_of_a_kind > one_pair);
        let two_pair1 = Hand::maybe_from_str("KK677").unwrap();
        let two_pair2 = Hand::maybe_from_str("KTJJT").unwrap();
        assert!(two_pair1 > two_pair2);
        let three_of_a_kind1 = three_of_a_kind;
        let three_of_a_kind2 = Hand::maybe_from_str("QQQJA").unwrap();
        assert!(three_of_a_kind1 < three_of_a_kind2)
    }

    #[test]
    fn correct_types_with_jokers() {
        let mut card = Hand::maybe_from_str("32T3K").unwrap();
        card.jack_to_joker();
        assert_eq!(card.get_type(), Type::OnePair);

        let mut card = Hand::maybe_from_str("KK677").unwrap();
        card.jack_to_joker();
        assert_eq!(card.get_type(), Type::TwoPair);

        let mut card = Hand::maybe_from_str("T55J5").unwrap();
        card.jack_to_joker();
        assert_eq!(card.get_type(), Type::FourOfAKind);

        let mut card = Hand::maybe_from_str("KTJJT").unwrap();
        card.jack_to_joker();
        assert_eq!(card.get_type(), Type::FourOfAKind);

        let mut card = Hand::maybe_from_str("QQQJA").unwrap();
        card.jack_to_joker();
        assert_eq!(card.get_type(), Type::FourOfAKind);
    }

    #[test]
    fn correct_order_with_jokers() {
        let mut no1 = Hand::maybe_from_str("T55J5").unwrap();
        no1.jack_to_joker();
        let mut no2 = Hand::maybe_from_str("QQQJA").unwrap();
        no2.jack_to_joker();
        let mut no3 = Hand::maybe_from_str("KTJJT").unwrap();
        no3.jack_to_joker();
        assert!(no1 < no2);
        assert!(no2 < no3);
    }
}
