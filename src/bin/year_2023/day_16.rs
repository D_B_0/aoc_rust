use std::collections::{HashMap, HashSet, VecDeque};

use aoc::{output, Day};

enum Object {
    SplitterUpDown,
    SplitterLeftRight,
    MirrorDiagonal,
    MirrorAntidiagonal,
}

impl Object {
    fn _char(&self) -> char {
        match self {
            Object::SplitterUpDown => '|',
            Object::SplitterLeftRight => '-',
            Object::MirrorDiagonal => '\\',
            Object::MirrorAntidiagonal => '/',
        }
    }

    fn trasform(
        &self,
        (dir, (x, y)): (Direction, (isize, isize)),
    ) -> Vec<(Direction, (isize, isize))> {
        match (self, dir) {
            (Object::SplitterUpDown, Direction::Left)
            | (Object::SplitterUpDown, Direction::Right) => {
                vec![(Direction::Up, (x, y)), (Direction::Down, (x, y))]
            }

            (Object::SplitterUpDown, Direction::Up) | (Object::SplitterUpDown, Direction::Down) => {
                vec![(dir, dir.transform((x, y)))]
            }

            (Object::SplitterLeftRight, Direction::Left)
            | (Object::SplitterLeftRight, Direction::Right) => {
                vec![(dir, dir.transform((x, y)))]
            }

            (Object::SplitterLeftRight, Direction::Up)
            | (Object::SplitterLeftRight, Direction::Down) => {
                vec![(Direction::Left, (x, y)), (Direction::Right, (x, y))]
            }

            (Object::MirrorDiagonal, Direction::Left) => vec![(Direction::Up, (x, y - 1))],
            (Object::MirrorDiagonal, Direction::Right) => vec![(Direction::Down, (x, y + 1))],
            (Object::MirrorDiagonal, Direction::Up) => vec![(Direction::Left, (x - 1, y))],
            (Object::MirrorDiagonal, Direction::Down) => vec![(Direction::Right, (x + 1, y))],

            (Object::MirrorAntidiagonal, Direction::Left) => vec![(Direction::Down, (x, y + 1))],
            (Object::MirrorAntidiagonal, Direction::Right) => vec![(Direction::Up, (x, y - 1))],
            (Object::MirrorAntidiagonal, Direction::Up) => vec![(Direction::Right, (x + 1, y))],
            (Object::MirrorAntidiagonal, Direction::Down) => vec![(Direction::Left, (x - 1, y))],
        }
    }
}

impl TryFrom<char> for Object {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '|' => Ok(Self::SplitterUpDown),
            '-' => Ok(Self::SplitterLeftRight),
            '\\' => Ok(Self::MirrorDiagonal),
            '/' => Ok(Self::MirrorAntidiagonal),
            _ => Err(()),
        }
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    fn transform(&self, (x, y): (isize, isize)) -> (isize, isize) {
        match self {
            Direction::Left => (x - 1, y),
            Direction::Right => (x + 1, y),
            Direction::Up => (x, y - 1),
            Direction::Down => (x, y + 1),
        }
    }
}

#[derive(Default)]
pub struct Day16 {
    objects: HashMap<(isize, isize), Object>,
    width: isize,
    height: isize,
}

impl Day16 {
    fn _print(&self) {
        for y in 0..self.height {
            for x in 0..self.width {
                print!(
                    "{}",
                    match self.objects.get(&(x, y)) {
                        Some(obj) => obj._char(),
                        None => ' ',
                    }
                )
            }
            println!()
        }
    }

    fn is_inbounds(&self, (x, y): (isize, isize)) -> bool {
        x >= 0 && x < self.width && y >= 0 && y < self.height
    }

    fn count_energized(&self, beam: (Direction, (isize, isize))) -> usize {
        let mut beams = VecDeque::new();
        beams.push_back(beam);
        let mut configurations = HashSet::new();
        while let Some(beam) = beams.pop_front() {
            configurations.insert(beam);
            let new_beams = if let Some(obj) = self.objects.get(&beam.1) {
                obj.trasform(beam)
            } else {
                vec![(beam.0, beam.0.transform(beam.1))]
            };
            for new_beam in new_beams {
                if !configurations.contains(&new_beam) && self.is_inbounds(new_beam.1) {
                    beams.push_back(new_beam);
                }
            }
        }
        configurations
            .iter()
            .map(|(_, dir)| dir)
            .collect::<HashSet<_>>()
            .len()
    }
}

impl Day for Day16 {
    fn initialize(&mut self, input: &str) {
        self.height = input.lines().count() as isize;
        self.width = input.lines().next().unwrap().chars().count() as isize;
        self.objects = input
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars().enumerate().filter_map(move |(x, ch)| {
                    Object::try_from(ch)
                        .ok()
                        .map(|obj| ((x as isize, y as isize), obj))
                })
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.count_energized((Direction::Right, (0, 0))))
    }

    fn part2(&mut self) -> Vec<String> {
        output!((0..self.width)
            .map(|x| (Direction::Down, (x, 0)))
            .chain((0..self.width).map(|x| (Direction::Up, (x, self.height - 1))))
            .chain((0..self.height).map(|y| (Direction::Right, (0, y))))
            .chain((0..self.height).map(|y| (Direction::Left, (self.width - 1, y))))
            .map(|beam| self.count_energized(beam))
            .max()
            .unwrap())
    }

    fn name(&self) -> usize {
        16
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example_input() {
        let mut day = Day16::default();
        day.initialize(".|...\\....\n|.-.\\.....\n.....|-...\n........|.\n..........\n.........\\\n..../.\\\\..\n.-.-/..|..\n.|....-|.\\\n..//.|....\n");
        assert_eq!(day.part1(), vec!["46"]);
        assert_eq!(day.part2(), vec!["51"]);
    }
}
