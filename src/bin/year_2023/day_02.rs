use aoc::{output, Day};

#[derive(Debug)]
struct Set {
    r: u8,
    g: u8,
    b: u8,
}

impl Set {
    fn contains(&self, other: &Set) -> bool {
        self.r >= other.r && self.g >= other.g && self.b >= other.b
    }

    fn power(&self) -> usize {
        self.r as usize * self.g as usize * self.b as usize
    }
}

impl TryFrom<&str> for Set {
    type Error = ();

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let mut set = Set { r: 0, g: 0, b: 0 };
        for cubes in s.split(", ") {
            let (ammt, col) = cubes.split_once(' ').ok_or(())?;
            let ammt = ammt.parse::<u8>().map_err(|_| ())?;
            match col {
                "red" => set.r = ammt,
                "green" => set.g = ammt,
                "blue" => set.b = ammt,
                _ => return Err(()),
            }
        }
        Ok(set)
    }
}

#[derive(Debug)]
struct Game(Vec<Set>);

impl Game {
    fn is_possible_given_superset(&self, superset: &Set) -> bool {
        for set in &self.0 {
            if !superset.contains(set) {
                return false;
            }
        }
        true
    }

    fn minimal_set(&self) -> Set {
        let mut min_set = Set { r: 0, g: 0, b: 0 };
        for set in &self.0 {
            min_set.r = min_set.r.max(set.r);
            min_set.g = min_set.g.max(set.g);
            min_set.b = min_set.b.max(set.b);
        }
        min_set
    }
}

impl TryFrom<&str> for Game {
    type Error = ();

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let (_, mut rest) = s.split_once(':').ok_or(())?;
        rest = rest.strip_prefix(' ').ok_or(())?;
        let mut sets = Vec::new();
        for set in rest.split("; ") {
            sets.push(Set::try_from(set)?);
        }
        Ok(Game(sets))
    }
}

#[derive(Default)]
pub struct Day02 {
    games: Vec<Game>,
}

impl Day for Day02 {
    fn initialize(&mut self, input: &str) {
        self.games = input
            .lines()
            .map(Game::try_from)
            .map(Result::unwrap)
            .collect::<Vec<_>>();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .games
            .iter()
            .enumerate()
            .filter(|(_, game)| game.is_possible_given_superset(&Set {
                r: 12,
                g: 13,
                b: 14,
            }))
            .map(|(i, _)| i + 1)
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .games
            .iter()
            .map(|g| g.minimal_set().power())
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        2
    }
}
