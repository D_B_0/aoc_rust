use aoc::{output, Day};

#[derive(Default)]
pub struct Day01 {
    calibration_strings: Vec<String>,
}

impl Day for Day01 {
    fn initialize(&mut self, input: &str) {
        self.calibration_strings = input.lines().map(String::from).collect::<Vec<_>>();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .calibration_strings
            .iter()
            .map(extract_calibration_num)
            .sum::<u32>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .calibration_strings
            .iter()
            .map(convert_string_numbers_to_digits)
            .map(extract_calibration_num)
            .sum::<u32>())
    }

    fn name(&self) -> usize {
        1
    }
}

fn extract_calibration_num(s: impl ToString) -> u32 {
    let mut calibration_value = 0;
    let mut first_found = false;
    let mut last_digit = 0;
    for c in s.to_string().chars() {
        if let Some(d) = c.to_digit(10) {
            if !first_found {
                first_found = true;
                calibration_value = d * 10
            }
            last_digit = d;
        }
    }
    calibration_value += last_digit;
    calibration_value
}

fn convert_string_numbers_to_digits(s: impl ToString) -> String {
    let s = s.to_string();
    let nums = [
        ("one", '1'),
        ("two", '2'),
        ("three", '3'),
        ("four", '4'),
        ("five", '5'),
        ("six", '6'),
        ("seven", '7'),
        ("eight", '8'),
        ("nine", '9'),
    ];
    let mut ret = String::new();
    let mut i = 0;
    'outer: while i < s.len() {
        for (num, dig) in nums {
            if i + num.len() <= s.len() && &s[i..i + num.len()] == num {
                ret.push(dig);
                i += num.len() - 1;
                continue 'outer;
            }
        }
        ret.push(s.chars().nth(i).unwrap());
        i += 1;
    }
    ret
}
