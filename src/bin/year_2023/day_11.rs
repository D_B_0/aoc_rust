use aoc::{output, Day};

#[derive(Default)]
pub struct Day11 {
    stars: Vec<(usize, usize)>,
}

impl Day11 {
    fn sub_empty_rows(&self, n: usize) -> Vec<(usize, usize)> {
        let mut stars = self.stars.clone();
        let mut max_x = stars.iter().fold(0, |acc, (x, _)| acc.max(*x));
        let mut max_y = stars.iter().fold(0, |acc, (_, y)| acc.max(*y));
        let mut y = 0;
        while y < max_y {
            if stars.iter().any(|(_, sy)| sy == &y) {
                y += 1;
                continue;
            }
            max_y += n - 1;
            for star in stars.iter_mut().filter(|(_, sy)| sy > &y) {
                star.1 += n - 1;
            }
            y += n;
        }
        let mut x = 0;
        while x < max_x {
            if stars.iter().any(|(sx, _)| sx == &x) {
                x += 1;
                continue;
            }
            max_x += n - 1;
            for star in stars.iter_mut().filter(|(sx, _)| sx > &x) {
                star.0 += n - 1;
            }
            x += n;
        }
        stars
    }
}

fn calc_distances(stars: &[(usize, usize)]) -> isize {
    let mut sum = 0;
    for (i, s1) in stars.iter().enumerate() {
        for s2 in stars.iter().skip(i + 1) {
            sum += (s1.0 as isize - s2.0 as isize).abs() + (s1.1 as isize - s2.1 as isize).abs()
        }
    }
    sum
}

impl Day for Day11 {
    fn initialize(&mut self, input: &str) {
        self.stars = input
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter_map(move |(x, ch)| match ch {
                        '.' => None,
                        '#' => Some((x, y)),
                        _ => unreachable!(),
                    })
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(calc_distances(&self.sub_empty_rows(2)))
    }

    fn part2(&mut self) -> Vec<String> {
        output!(calc_distances(&self.sub_empty_rows(1000000)))
    }

    fn name(&self) -> usize {
        11
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example_input() {
        let mut day = Day11::default();
        day.initialize(
            "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
",
        );
        assert_eq!(day.part1(), vec!["374"]);
        assert_eq!(calc_distances(&day.sub_empty_rows(10)), 1030);
        assert_eq!(calc_distances(&day.sub_empty_rows(100)), 8410);
    }
}
