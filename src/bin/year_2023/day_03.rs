use std::collections::HashMap;

use aoc::{output, Day};

#[derive(PartialEq, Eq, Hash)]
enum Element {
    Symbol { gear: bool },
    Digit(u32),
}

impl TryFrom<char> for Element {
    type Error = ();

    fn try_from(ch: char) -> Result<Self, Self::Error> {
        match ch {
            dig if dig.is_ascii_digit() => Ok(Element::Digit(dig.to_digit(10).unwrap())),
            '.' => Err(()),
            '*' => Ok(Element::Symbol { gear: true }),
            _ => Ok(Element::Symbol { gear: false }),
        }
    }
}

#[derive(Default)]
pub struct Day03 {
    schematic: HashMap<(usize, usize), Element>,
    width: usize,
    height: usize,
}

impl Day for Day03 {
    fn initialize(&mut self, input: &str) {
        for (j, line) in input.lines().enumerate() {
            for (i, ch) in line.char_indices() {
                if let Ok(element) = Element::try_from(ch) {
                    self.width = self.width.max(i + 1);
                    self.height = self.height.max(j + 1);
                    self.schematic.insert((i, j), element);
                }
            }
        }
    }

    fn part1(&mut self) -> Vec<String> {
        let mut sum = 0;
        for j in 0..self.height {
            let mut curr_num = None;
            let mut found_sym = false;
            for i in 0..self.width {
                match self.schematic.get(&(i, j)) {
                    None | Some(Element::Symbol { gear: _ }) => {
                        if found_sym {
                            if let Some(num) = curr_num {
                                sum += num;
                            }
                        }
                        curr_num = None;
                        found_sym = false;
                    }
                    Some(Element::Digit(d)) => {
                        if let Some(curr) = curr_num {
                            curr_num = Some(curr * 10 + d);
                        } else {
                            curr_num = Some(*d);
                        }
                        for neighbor in neighbors((i, j)) {
                            if matches!(
                                self.schematic.get(&neighbor),
                                Some(Element::Symbol { gear: _ })
                            ) {
                                found_sym = true;
                            }
                        }
                    }
                }
            }
            if found_sym {
                if let Some(num) = curr_num {
                    sum += num;
                }
            }
        }
        output!(sum)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut map = HashMap::new();
        for j in 0..self.height {
            let mut curr_num = None;
            let mut found_sym = None;
            for i in 0..self.width {
                match self.schematic.get(&(i, j)) {
                    None | Some(Element::Symbol { gear: _ }) => {
                        if let Some(sym) = found_sym {
                            if let Some(num) = curr_num {
                                map.entry(sym).or_insert(Vec::new()).push(num);
                            }
                        }
                        curr_num = None;
                        found_sym = None;
                    }
                    Some(Element::Digit(d)) => {
                        if let Some(curr) = curr_num {
                            curr_num = Some(curr * 10 + d);
                        } else {
                            curr_num = Some(*d);
                        }
                        for neighbor in neighbors((i, j)) {
                            let elem = self.schematic.get(&neighbor);
                            if let Some(elem) = elem {
                                if matches!(elem, Element::Symbol { gear: true }) {
                                    found_sym = Some(neighbor);
                                }
                            }
                        }
                    }
                }
            }
            if let Some(sym) = found_sym {
                if let Some(num) = curr_num {
                    map.entry(sym).or_insert(Vec::new()).push(num);
                }
            }
        }
        output!(map
            .iter()
            .filter_map(|(_, vec)| {
                if vec.len() == 2 {
                    Some(vec[0] * vec[1])
                } else {
                    None
                }
            })
            .sum::<u32>())
    }

    fn name(&self) -> usize {
        3
    }
}

fn neighbors((i, j): (usize, usize)) -> Vec<(usize, usize)> {
    let mut res = vec![];
    if i != 0 {
        res.push((i - 1, j));
        res.push((i - 1, j + 1));
    }
    if j != 0 {
        res.push((i, j - 1));
        res.push((i + 1, j - 1));
    }
    if i != 0 && j != 0 {
        res.push((i - 1, j - 1));
    }
    res.push((i + 1, j));
    res.push((i + 1, j + 1));
    res.push((i, j + 1));
    res
}
