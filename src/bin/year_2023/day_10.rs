use std::collections::{HashMap, HashSet, VecDeque};

use aoc::{output, Day};

enum Pipe {
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest,
    NorthSouth,
    EastWest,
    Start,
}

impl Pipe {
    fn try_from_char(ch: char) -> Option<Pipe> {
        match ch {
            '|' => Some(Pipe::NorthSouth),
            '-' => Some(Pipe::EastWest),
            'L' => Some(Pipe::NorthEast),
            'J' => Some(Pipe::NorthWest),
            '7' => Some(Pipe::SouthWest),
            'F' => Some(Pipe::SouthEast),
            'S' => Some(Pipe::Start),
            _ => None,
        }
    }

    fn to_box_char(&self) -> char {
        match self {
            Pipe::NorthEast => '┗',
            Pipe::NorthWest => '┛',
            Pipe::SouthEast => '┏',
            Pipe::SouthWest => '┓',
            Pipe::NorthSouth => '┃',
            Pipe::EastWest => '━',
            Pipe::Start => panic!("should not call `connectto_box_charions` on `Start`"),
        }
    }

    fn connects_to_north(&self) -> bool {
        matches!(self, Pipe::NorthEast | Pipe::NorthWest | Pipe::NorthSouth)
    }

    fn connects_to_south(&self) -> bool {
        matches!(self, Pipe::SouthEast | Pipe::SouthWest | Pipe::NorthSouth)
    }

    fn connects_to_east(&self) -> bool {
        matches!(self, Pipe::NorthEast | Pipe::SouthEast | Pipe::EastWest)
    }

    fn connects_to_west(&self) -> bool {
        matches!(self, Pipe::NorthWest | Pipe::SouthWest | Pipe::EastWest)
    }

    fn connections(&self, i: usize, j: usize) -> Vec<(usize, usize)> {
        // this is prone to underflow panic
        match self {
            Pipe::NorthEast => vec![(i + 1, j), (i, j - 1)],
            Pipe::NorthWest => vec![(i - 1, j), (i, j - 1)],
            Pipe::SouthEast => vec![(i + 1, j), (i, j + 1)],
            Pipe::SouthWest => vec![(i - 1, j), (i, j + 1)],
            Pipe::NorthSouth => vec![(i, j - 1), (i, j + 1)],
            Pipe::EastWest => vec![(i + 1, j), (i - 1, j)],
            Pipe::Start => panic!("should not call `connections` on `Start`"),
        }
    }
}

fn neighbors(i: usize, j: usize, i_max: usize, j_max: usize) -> Vec<(usize, usize)> {
    let mut res = vec![];
    if i != 0 {
        res.push((i - 1, j));
    }
    if i != i_max - 1 {
        res.push((i + 1, j));
    }
    if j != 0 {
        res.push((i, j - 1));
    }
    if j != j_max - 1 {
        res.push((i, j + 1));
    }
    res
}

#[derive(Default)]
pub struct Day10 {
    network: Vec<Vec<Option<Pipe>>>,
    start: (usize, usize),
    path: HashSet<(usize, usize)>,
}

impl Day10 {
    fn get(&self, i: usize, j: usize) -> Option<&Pipe> {
        self.network.get(j)?.get(i)?.as_ref()
    }

    #[allow(dead_code)]
    fn print_network(&self) {
        for row in &self.network {
            for pipe in row {
                print!(
                    "{}",
                    match pipe {
                        None => ' ',
                        Some(pipe) => pipe.to_box_char(),
                    }
                );
            }
            println!();
        }
    }

    fn substitute_start(&mut self) {
        let mut north = false;
        let mut south = false;
        let mut east = false;
        let mut west = false;
        for (i, j) in neighbors(
            self.start.0,
            self.start.1,
            self.network[0].len(),
            self.network.len(),
        ) {
            let Some(pipe) = self.get(i, j) else {
                continue;
            };
            if j < self.start.1 && pipe.connects_to_south() {
                north = true;
            }
            if j > self.start.1 && pipe.connects_to_north() {
                south = true;
            }
            if i < self.start.0 && pipe.connects_to_east() {
                west = true;
            }
            if i > self.start.0 && pipe.connects_to_west() {
                east = true;
            }
        }
        self.network[self.start.1][self.start.0] = Some(match (north, south, east, west) {
            (true, true, false, false) => Pipe::NorthSouth,
            (true, false, true, false) => Pipe::NorthEast,
            (true, false, false, true) => Pipe::NorthWest,
            (false, true, true, false) => Pipe::SouthEast,
            (false, true, false, true) => Pipe::SouthWest,
            (false, false, true, true) => Pipe::EastWest,
            _ => unreachable!("{north}, {south}, {east}, {west}"),
        });
    }
}

impl Day for Day10 {
    fn initialize(&mut self, input: &str) {
        self.network = input
            .lines()
            .map(|line| line.chars().map(Pipe::try_from_char).collect())
            .collect();
        self.start = self
            .network
            .iter()
            .enumerate()
            .map(|(y, row)| {
                row.iter()
                    .position(|pipe| matches!(pipe, Some(Pipe::Start)))
                    .map(|x| (x, y))
            })
            .find(Option::is_some)
            .unwrap()
            .unwrap();
        self.substitute_start();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut stack = VecDeque::new();
        let mut distance_map = HashMap::new();
        stack.push_back(self.start);
        distance_map.insert(self.start, 0);
        while let Some((i, j)) = stack.pop_back() {
            let Some(pipe) = self.get(i, j) else {
                continue;
            };
            for conn in pipe.connections(i, j).iter() {
                if !distance_map.contains_key(conn) {
                    stack.push_front(*conn);
                    distance_map.insert(*conn, distance_map.get(&(i, j)).unwrap() + 1);
                } else {
                    distance_map.insert(
                        *conn,
                        usize::min(
                            *distance_map.get(conn).unwrap(),
                            distance_map.get(&(i, j)).unwrap() + 1,
                        ),
                    );
                }
            }
        }
        self.path = distance_map.keys().cloned().collect();
        output!(distance_map.values().max().unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut inside_count = 0;
        for (j, row) in self.network.iter().enumerate() {
            let mut south_count = 0;
            for (i, pipe) in row.iter().enumerate() {
                if self.path.contains(&(i, j)) {
                    if pipe.as_ref().unwrap().connects_to_south() {
                        south_count += 1;
                    }
                } else if south_count % 2 == 1 {
                    inside_count += 1;
                }
            }
        }
        output!(inside_count)
    }

    fn name(&self) -> usize {
        10
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_example_part_1() {
        let mut day = Day10::default();
        day.initialize(
            "-L|F7
7S-7|
L|7||
-L-J|
L|-JF
",
        );
        assert_eq!(day.part1(), vec!["4"]);
        let mut day = Day10::default();
        day.initialize(
            "7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ",
        );
        assert_eq!(day.part1(), vec!["8"]);
    }

    #[test]
    fn test_example_part_2() {
        let mut day = Day10::default();
        day.initialize(
            ".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
",
        );
        day.part1();
        assert_eq!(day.part2(), vec!["8"]);
        let mut day = Day10::default();
        day.initialize(
            "FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
",
        );
        day.part1();
        assert_eq!(day.part2(), vec!["10"]);
    }
}
