use aoc::{Day, Year};

use proc_macro::{days, import_days};

import_days!(1..=17);

#[derive(Default)]
pub(crate) struct Year2023 {}

impl Year for Year2023 {
    fn days(&self) -> Vec<Box<dyn Day>> {
        days!(1..=17)
    }

    fn name(&self) -> usize {
        2023
    }
}
