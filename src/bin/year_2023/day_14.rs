use std::collections::{HashMap, HashSet};

use aoc::{output, Day};

#[derive(Default)]
pub struct Day14 {
    height: usize,
    width: usize,
    movers: Vec<(usize, usize)>,
    blockers: HashSet<(usize, usize)>,
}

impl Day14 {
    fn move_north(&mut self) {
        self.movers.sort_by(|(_, y1), (_, y2)| y1.cmp(y2));
        for idx in 0..self.movers.len() {
            if let Some(hit) = self
                .movers
                .iter()
                .filter(|(x, y)| x == &self.movers[idx].0 && y < &self.movers[idx].1)
                .chain(
                    self.blockers
                        .iter()
                        .filter(|(x, y)| x == &self.movers[idx].0 && y < &self.movers[idx].1),
                )
                .max_by(|(_, y1), (_, y2)| y1.cmp(y2))
            {
                self.movers[idx].1 = hit.1 + 1;
            } else {
                self.movers[idx].1 = 0;
            }
        }
    }

    fn move_south(&mut self) {
        self.movers.sort_by(|(_, y1), (_, y2)| y2.cmp(y1));
        for idx in 0..self.movers.len() {
            if let Some(hit) = self
                .movers
                .iter()
                .filter(|(x, y)| x == &self.movers[idx].0 && y > &self.movers[idx].1)
                .chain(
                    self.blockers
                        .iter()
                        .filter(|(x, y)| x == &self.movers[idx].0 && y > &self.movers[idx].1),
                )
                .min_by(|(_, y1), (_, y2)| y1.cmp(y2))
            {
                self.movers[idx].1 = hit.1 - 1;
            } else {
                self.movers[idx].1 = self.height - 1;
            }
        }
    }

    fn move_west(&mut self) {
        self.movers.sort_by(|(x1, _), (x2, _)| x1.cmp(x2));
        for idx in 0..self.movers.len() {
            if let Some(hit) = self
                .movers
                .iter()
                .filter(|(x, y)| x < &self.movers[idx].0 && y == &self.movers[idx].1)
                .chain(
                    self.blockers
                        .iter()
                        .filter(|(x, y)| x < &self.movers[idx].0 && y == &self.movers[idx].1),
                )
                .max_by(|(x1, _), (x2, _)| x1.cmp(x2))
            {
                self.movers[idx].0 = hit.0 + 1;
            } else {
                self.movers[idx].0 = 0;
            }
        }
    }

    fn move_east(&mut self) {
        self.movers.sort_by(|(x1, _), (x2, _)| x2.cmp(x1));
        for idx in 0..self.movers.len() {
            if let Some(hit) = self
                .movers
                .iter()
                .filter(|(x, y)| x > &self.movers[idx].0 && y == &self.movers[idx].1)
                .chain(
                    self.blockers
                        .iter()
                        .filter(|(x, y)| x > &self.movers[idx].0 && y == &self.movers[idx].1),
                )
                .min_by(|(x1, _), (x2, _)| x1.cmp(x2))
            {
                self.movers[idx].0 = hit.0 - 1;
            } else {
                self.movers[idx].0 = self.width - 1;
            }
        }
    }

    fn cycle(&mut self) {
        self.move_north();
        self.move_west();
        self.move_south();
        self.move_east();
    }

    fn load(&self) -> usize {
        self.movers.iter().map(|(_, y)| self.height - y).sum()
    }

    fn _print(&self) {
        for y in 0..self.height {
            for x in 0..self.width {
                if self.movers.contains(&(x, y)) {
                    print!("O");
                } else if self.blockers.contains(&(x, y)) {
                    print!("#");
                } else {
                    print!(" ");
                }
            }
            println!();
        }
    }
}

impl Day for Day14 {
    fn initialize(&mut self, input: &str) {
        self.height = input.lines().count();
        self.width = input.lines().next().unwrap().chars().count();
        self.movers = input
            .lines()
            .enumerate()
            .flat_map(|(y, l)| {
                l.chars().enumerate().filter_map(move |(x, ch)| match ch {
                    'O' => Some((x, y)),
                    _ => None,
                })
            })
            .collect();
        self.blockers = input
            .lines()
            .enumerate()
            .flat_map(|(y, l)| {
                l.chars().enumerate().filter_map(move |(x, ch)| match ch {
                    '#' => Some((x, y)),
                    _ => None,
                })
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let old = self.movers.clone();
        self.move_north();
        let res = self.load();
        self.movers = old;
        output!(res)
    }

    fn part2(&mut self) -> Vec<String> {
        let mut states_observed = HashMap::new();
        for i in 0..1000000000 {
            if let Some(start) = states_observed.insert(self.movers.clone(), i) {
                let cycle_len = i - start;
                for _ in 0..(1000000000 - start) % cycle_len {
                    self.cycle()
                }
                return output!(self.load());
            }
            self.cycle();
        }
        output!(self.load())
    }

    fn name(&self) -> usize {
        14
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example_input() {
        let mut day = Day14::default();
        day.initialize(
            "O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
",
        );
        assert_eq!(day.part1(), vec!["136"]);
        assert_eq!(day.part2(), vec!["64"]);
    }
}
