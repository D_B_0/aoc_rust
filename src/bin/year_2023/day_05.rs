use std::ops::Range;

use aoc::{output, Day};

#[derive(Debug, Default)]
struct Map(Vec<(Range<i64>, i64)>);

impl Map {
    fn parse(s: &str) -> Map {
        Map(s
            .lines()
            .map(|s| {
                let mut nums = s
                    .split_whitespace()
                    .map(str::parse::<i64>)
                    .map(Result::unwrap);
                let to_start = nums.next().unwrap();
                let from_start = nums.next().unwrap();
                let len = nums.next().unwrap();
                (from_start..(from_start + len), to_start - from_start)
            })
            .collect())
    }

    fn get(&self, from: i64) -> i64 {
        for (range, delta) in &self.0 {
            if range.contains(&from) {
                return from + delta;
            }
        }
        from
    }
}

#[derive(Default)]
pub struct Day05 {
    starting_seeds: Vec<i64>,
    maps: Vec<Map>,
}

impl Day for Day05 {
    fn initialize(&mut self, input: &str) {
        let mut input = input.split("\n\n").map(str::trim);
        self.starting_seeds = input
            .next() // first element of the iterator is the first line
            .unwrap()
            .strip_prefix("seeds: ") // it starts like this
            .unwrap()
            .split_whitespace() // it is a space separated list of ints representing seeds
            .map(str::parse::<i64>)
            .map(Result::unwrap)
            .collect();
        self.maps = input
            .map(|s| s.split_once('\n').unwrap().1)
            .map(Map::parse)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .starting_seeds
            .iter()
            .cloned()
            .map(|val| self.maps.iter().fold(val, |val, map| map.get(val)))
            .min()
            .unwrap())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut ranges = self
            .starting_seeds
            .chunks(2)
            .map(|seed_range| seed_range[0]..(seed_range[0] + seed_range[1]))
            .collect::<Vec<_>>();
        for map in self.maps.iter() {
            let mut new_ranges = Vec::new();
            'outer: while let Some(seed_range) = ranges.pop() {
                for (range, _) in &map.0 {
                    let is_start_containted = seed_range.contains(&range.start);
                    let is_end_containted = seed_range.contains(&(range.end - 1));
                    match (is_start_containted, is_end_containted) {
                        (false, true) => {
                            new_ranges.push(map.get(seed_range.start)..map.get(range.end - 1) + 1);
                            ranges.push(range.end..seed_range.end);
                            continue 'outer;
                        }
                        (true, true) => {
                            new_ranges.push(map.get(range.start)..map.get(range.end - 1) + 1);
                            ranges.push(seed_range.start..range.start);
                            ranges.push(range.end..seed_range.end);
                            continue 'outer;
                        }
                        (true, false) => {
                            new_ranges.push(map.get(range.start)..map.get(seed_range.end));
                            ranges.push(seed_range.start..range.start);
                            continue 'outer;
                        }
                        (false, false) => {
                            if range.start < seed_range.start && range.end > seed_range.end {
                                new_ranges.push(map.get(seed_range.start)..map.get(seed_range.end));
                                continue 'outer;
                            }
                        }
                    }
                }
                new_ranges.push(seed_range);
            }
            ranges = new_ranges;
        }
        output!(ranges.iter().map(|range| range.start).min().unwrap())
    }

    fn name(&self) -> usize {
        5
    }
}
