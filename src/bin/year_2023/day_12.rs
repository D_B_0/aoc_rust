use std::collections::HashMap;

use aoc::{output, Day};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum Status {
    Operational,
    Broken,
    Unknown,
}

impl TryFrom<char> for Status {
    type Error = ();

    fn try_from(ch: char) -> Result<Self, Self::Error> {
        match ch {
            '.' => Ok(Status::Operational),
            '#' => Ok(Status::Broken),
            '?' => Ok(Status::Unknown),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
struct Row {
    springs: Vec<Status>,
    counts: Vec<usize>,
}

impl TryFrom<&str> for Row {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let (springs, counts) = value.split_once(' ').ok_or(())?;

        Ok(Row {
            springs: springs
                .chars()
                .map(Status::try_from)
                .collect::<Result<_, _>>()?,
            counts: counts
                .split(',')
                .map(str::parse::<usize>)
                .collect::<Result<_, _>>()
                .map_err(|_| ())?,
        })
    }
}

impl Row {
    fn get_arrangements(&self) -> usize {
        Row::arrangements_recursive(&self.springs, &self.counts, &mut HashMap::new())
    }

    fn arrangements_recursive(
        springs: &[Status],
        counts: &[usize],
        cache: &mut HashMap<(usize, usize), usize>,
    ) -> usize {
        let key = (springs.len(), counts.len());
        if cache.contains_key(&key) {
            return cache[&key];
        }
        if counts.is_empty() {
            if springs.contains(&Status::Broken) {
                return 0;
            }
            return 1;
        }
        if springs.len() < counts.iter().sum() {
            return 0;
        }
        let res = match springs[0] {
            Status::Operational => Row::arrangements_recursive(&springs[1..], counts, cache),
            Status::Broken => Row::arrangements_recursive_broken(springs, counts, cache),
            Status::Unknown => {
                Row::arrangements_recursive(&springs[1..], counts, cache)
                    + Row::arrangements_recursive_broken(springs, counts, cache)
            }
        };
        cache.insert(key, res);
        res
    }

    fn arrangements_recursive_broken(
        springs: &[Status],
        counts: &[usize],
        cache: &mut HashMap<(usize, usize), usize>,
    ) -> usize {
        if springs.len() < counts[0] {
            return 0;
        }
        if springs[0..counts[0]].contains(&Status::Operational) {
            return 0;
        }
        if springs.len() == counts[0] && counts.len() == 1 {
            return 1;
        }
        if springs[counts[0]] == Status::Broken {
            return 0;
        }
        Row::arrangements_recursive(&springs[counts[0] + 1..], &counts[1..], cache)
    }

    fn unfold(&mut self) {
        self.counts = self.counts.repeat(5).to_vec();
        self.springs.push(Status::Unknown);
        self.springs = self.springs.repeat(5).to_vec();
        self.springs.pop();
    }
}

#[derive(Default)]
pub struct Day12 {
    rows: Vec<Row>,
}

impl Day for Day12 {
    fn initialize(&mut self, input: &str) {
        self.rows = input
            .lines()
            .map(Row::try_from)
            .collect::<Result<_, _>>()
            .unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self.rows.iter().map(Row::get_arrangements).sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        self.rows.iter_mut().for_each(Row::unfold);
        output!(self.rows.iter().map(Row::get_arrangements).sum::<usize>())
    }

    fn name(&self) -> usize {
        12
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn singles() {
        assert_eq!(
            Row::try_from("???.### 1,1,3").unwrap().get_arrangements(),
            1,
        );
        assert_eq!(
            Row::try_from(".??..??...?##. 1,1,3")
                .unwrap()
                .get_arrangements(),
            4,
        );
    }

    #[test]
    fn example_input() {
        let mut day = Day12::default();
        day.initialize(
            "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
",
        );
        assert_eq!(day.part1(), vec!["21"]);
        assert_eq!(day.part2(), vec!["525152"]);
    }
}
