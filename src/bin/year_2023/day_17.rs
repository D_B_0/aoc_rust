use std::collections::{HashMap, HashSet};

use aoc::{output, Day};

fn _dump_path(grid: &Vec<Vec<u32>>, path: Vec<(usize, usize)>) {
    let max_x = grid[0].len();
    let max_y = grid.len();
    for y in 0..max_y {
        for x in 0..max_x {
            print!(
                "{}",
                if path.contains(&(x, y)) {
                    "#".to_owned()
                } else {
                    " ".to_owned()
                    // grid[y][x].to_string()
                }
            );
        }
        println!()
    }
    println!()
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Node {
    pos: (usize, usize),
    prev_dir: Option<Direction>,
    streak: u8,
}

fn dijkstra_1(grid: &Vec<Vec<u32>>, start: (usize, usize), end: (usize, usize)) -> u32 {
    let mut current = Node {
        pos: start,
        prev_dir: None,
        streak: 0,
    };
    let mut visited = HashSet::new();
    visited.insert(current);
    let mut tentative_distance = HashMap::new();
    tentative_distance.insert(current, 0);
    let mut comes_from = HashMap::new();
    comes_from.insert(current, current);

    loop {
        if current.pos == end {
            let mut last = current;
            let mut sum = grid[current.pos.1][current.pos.0];
            loop {
                match comes_from.get(&last) {
                    Some(c) => {
                        if c.pos == start {
                            break;
                        }
                        sum += grid[c.pos.1][c.pos.0];
                        last = *c;
                    }
                    None => {
                        break;
                    }
                }
            }
            return sum;
        }
        for neighbor_pos in get_neighbors(current.pos, grid[0].len(), grid.len()) {
            let dir = get_direction(current.pos, neighbor_pos);
            if (dir == current.prev_dir && current.streak == 3)
                || are_directions_opposite(dir, current.prev_dir)
            {
                continue;
            }
            let neighbor = Node {
                pos: neighbor_pos,
                prev_dir: dir,
                streak: if dir == current.prev_dir {
                    current.streak + 1
                } else {
                    1
                },
            };
            if visited.contains(&neighbor) {
                continue;
            }
            let new_dist = tentative_distance[&current] + grid[neighbor.pos.1][neighbor.pos.0];
            if tentative_distance.get(&neighbor).unwrap_or(&u32::MAX) > &new_dist {
                // comes from...
                tentative_distance.insert(neighbor, new_dist);
                comes_from.insert(neighbor, current);
            }
        }
        visited.insert(current);
        let (new, _) = tentative_distance
            .iter()
            .filter(|(pos, _)| !visited.contains(pos))
            .min_by_key(|(_, dist)| **dist)
            .unwrap();
        current = *new;
    }
}

fn dijkstra_2(grid: &Vec<Vec<u32>>, start: (usize, usize), end: (usize, usize)) -> u32 {
    let mut current = Node {
        pos: start,
        prev_dir: None,
        streak: 0,
    };
    let mut visited = HashSet::new();
    visited.insert(current);
    let mut tentative_distance = HashMap::new();
    tentative_distance.insert(current, 0);
    let mut comes_from = HashMap::new();
    comes_from.insert(current, current);

    loop {
        // if visited.len() % 1000 == 0 {
        //     println!(
        //         "explored {} nodes, currently at position {:?}",
        //         visited.len(),
        //         current.pos
        //     );
        // }
        if current.pos == end {
            // let mut path = vec![current.pos];
            let mut last = current;
            let mut sum = grid[current.pos.1][current.pos.0];
            loop {
                match comes_from.get(&last) {
                    Some(c) => {
                        if c.pos == start {
                            break;
                        }
                        sum += grid[c.pos.1][c.pos.0];
                        // path.push(c.pos);
                        last = *c;
                    }
                    None => {
                        break;
                    }
                }
            }
            // _dump_path(grid, path);
            return sum;
        }
        for neighbor_pos in get_neighbors(current.pos, grid[0].len(), grid.len()) {
            let dir = get_direction(current.pos, neighbor_pos);
            if current.prev_dir != None && current.streak < 4 && dir != current.prev_dir {
                continue;
            }
            if (dir == current.prev_dir && current.streak == 10)
                || are_directions_opposite(dir, current.prev_dir)
            {
                continue;
            }
            let neighbor = Node {
                pos: neighbor_pos,
                prev_dir: dir,
                streak: if dir == current.prev_dir {
                    current.streak + 1
                } else {
                    1
                },
            };
            if visited.contains(&neighbor) {
                continue;
            }
            let new_dist = if neighbor.pos == end && neighbor.streak < 4 {
                u32::MAX
            } else {
                tentative_distance[&current] + grid[neighbor.pos.1][neighbor.pos.0]
            };
            if tentative_distance.get(&neighbor).unwrap_or(&u32::MAX) > &new_dist {
                // comes from...
                tentative_distance.insert(neighbor, new_dist);
                comes_from.insert(neighbor, current);
            }
        }
        visited.insert(current);
        let (new, _) = tentative_distance
            .iter()
            .filter(|(pos, _)| !visited.contains(pos))
            .min_by_key(|(_, dist)| **dist)
            .unwrap();
        current = *new;
    }
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn are_directions_opposite(dir_1: Option<Direction>, dir_2: Option<Direction>) -> bool {
    match (dir_1, dir_2) {
        (Some(Direction::Up), Some(Direction::Down))
        | (Some(Direction::Down), Some(Direction::Up))
        | (Some(Direction::Left), Some(Direction::Right))
        | (Some(Direction::Right), Some(Direction::Left)) => true,
        (_, _) => false,
    }
}

fn get_direction(from: (usize, usize), to: (usize, usize)) -> Option<Direction> {
    if from.0 > to.0 {
        Some(Direction::Right)
    } else if from.0 < to.0 {
        Some(Direction::Left)
    } else if from.1 > to.1 {
        Some(Direction::Up)
    } else if from.1 < to.1 {
        Some(Direction::Down)
    } else {
        None
    }
}

fn get_neighbors((x, y): (usize, usize), max_x: usize, max_y: usize) -> Vec<(usize, usize)> {
    let mut res = Vec::new();
    if x > 0 {
        res.push((x - 1, y));
    }
    if y > 0 {
        res.push((x, y - 1));
    }
    if x < max_x - 1 {
        res.push((x + 1, y));
    }
    if y < max_y - 1 {
        res.push((x, y + 1));
    }
    res
}

#[derive(Default)]
pub struct Day17 {
    grid: Vec<Vec<u32>>,
}

impl Day17 {
    fn _print(&self) {
        for row in &self.grid {
            for num in row {
                print!("{num}");
            }
            println!()
        }
    }
}

impl Day for Day17 {
    fn initialize(&mut self, input: &str) {
        self.grid = input
            .lines()
            .map(|line| line.chars().map(|d| d.to_digit(10).unwrap()).collect())
            .collect()
    }

    fn part1(&mut self) -> Vec<String> {
        output!(dijkstra_1(
            &self.grid,
            (0, 0),
            (self.grid[0].len() - 1, self.grid.len() - 1)
        ))
        // output!("omitted for brevity")
    }

    fn part2(&mut self) -> Vec<String> {
        output!(dijkstra_2(
            &self.grid,
            (0, 0),
            (self.grid[0].len() - 1, self.grid.len() - 1)
        ))
    }

    fn name(&self) -> usize {
        17
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple_grid_1() {
        assert_eq!(dijkstra_1(&vec![vec![0, 10]], (0, 0), (1, 0)), 10);
    }

    #[test]
    fn simple_grid_2() {
        assert_eq!(
            dijkstra_1(&vec![vec![0, 10, 20, 30], vec![1, 2, 3, 4]], (0, 0), (3, 1)),
            10
        );
    }

    #[test]
    fn simple_grid_3() {
        assert_eq!(
            dijkstra_1(
                &vec![vec![0, 10, 20, 30, 40], vec![1, 2, 3, 4, 5]],
                (0, 0),
                (4, 1)
            ),
            24
        );
    }

    #[test]
    fn simple_grid_4() {
        let mut day = Day17::default();
        day.initialize("111111111111\n999999999991\n999999999991\n999999999991\n999999999991\n");
        assert_eq!(day.part2(), vec!["71"]);
    }

    #[test]
    fn example_input() {
        let mut day = Day17::default();
        day.initialize("2413432311323\n3215453535623\n3255245654254\n3446585845452\n4546657867536\n1438598798454\n4457876987766\n3637877979653\n4654967986887\n4564679986453\n1224686865563\n2546548887735\n4322674655533\n");
        assert_eq!(day.part1(), vec!["102"]);
        assert_eq!(day.part2(), vec!["94"]);
    }
}
