use std::num::ParseIntError;

use aoc::{output, Day};

#[derive(Debug)]
struct Card {
    numbers: Vec<u32>,
    winning: Vec<u32>,
}

impl Card {
    fn count_winning(&self) -> usize {
        self.numbers
            .iter()
            .filter(|num| self.winning.contains(num))
            .count()
    }
}

impl TryFrom<&str> for Card {
    type Error = ();

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let (_, rest) = s.split_once(": ").ok_or(())?;
        let (win, nums) = rest.split_once(" | ").ok_or(())?;
        Ok(Card {
            numbers: str_to_vec_of_num(nums).map_err(|_| ())?,
            winning: str_to_vec_of_num(win).map_err(|_| ())?,
        })
    }
}

fn str_to_vec_of_num(s: &str) -> Result<Vec<u32>, ParseIntError> {
    s.split_whitespace()
        .map(str::parse::<u32>)
        .collect::<Result<Vec<_>, _>>()
}

#[derive(Default)]
pub struct Day04 {
    cards: Vec<Card>,
}

impl Day for Day04 {
    fn initialize(&mut self, input: &str) {
        self.cards = input
            .lines()
            .map(Card::try_from)
            .map(Result::unwrap)
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .cards
            .iter()
            .map(Card::count_winning)
            .filter(|num| num != &0)
            .map(|num| 1 << (num - 1))
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        let mut pile = self.cards.iter().map(|card| (1, card)).collect::<Vec<_>>();
        let mut sum = 0;
        for i in 0..pile.len() {
            let wins = pile[i].1.count_winning();
            for j in i + 1..(i + 1 + wins).min(pile.len()) {
                pile[j].0 += pile[i].0;
            }
            sum += pile[i].0;
        }
        output!(sum)
    }

    fn name(&self) -> usize {
        4
    }
}
