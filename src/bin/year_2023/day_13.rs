use aoc::{output, Day};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Tile {
    Ash,
    Rock,
}

impl TryFrom<char> for Tile {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Tile::Ash),
            '#' => Ok(Tile::Rock),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
struct Pattern {
    rows: Vec<Vec<Tile>>,
}

impl TryFrom<&str> for Pattern {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(Pattern {
            rows: value
                .lines()
                .map(|line| line.chars().map(Tile::try_from).collect::<Result<_, _>>())
                .collect::<Result<_, _>>()?,
        })
    }
}

impl Pattern {
    fn horizontal_refl(rows: &Vec<Vec<Tile>>) -> Option<usize> {
        'outer: for i in 0..rows.len() - 1 {
            // we consider the reflection to be between row i and row i+1
            let range = if i < rows.len() / 2 {
                0..i + 1
            } else {
                i + 1..rows.len()
            };
            for j in range {
                if rows[j] != rows[2 * i - j + 1] {
                    continue 'outer;
                }
            }
            return Some(i + 1);
        }
        None
    }

    fn horizontal_refl_part_2(rows: &Vec<Vec<Tile>>) -> Option<usize> {
        'outer: for i in 0..rows.len() - 1 {
            // we consider the reflection to be between row i and row i+1
            let mut off_by_one = false;
            let range = if i < rows.len() / 2 {
                0..i + 1
            } else {
                i + 1..rows.len()
            };
            for j in range {
                if rows[j] == rows[2 * i - j + 1] {
                    continue;
                }
                if rows[j]
                    .iter()
                    .zip(rows[2 * i - j + 1].iter())
                    .filter(|(l, r)| l != r)
                    .count()
                    == 1
                {
                    if off_by_one {
                        continue 'outer;
                    }
                    off_by_one = true
                } else {
                    continue 'outer;
                }
            }

            if off_by_one {
                return Some(i + 1);
            }
        }
        None
    }

    fn horizontal_reflection(&self) -> Option<usize> {
        Pattern::horizontal_refl(&self.rows)
    }

    fn vertical_reflection(&self) -> Option<usize> {
        Pattern::horizontal_refl(&transpose(self.rows.clone()))
    }

    fn horizontal_reflection_part_2(&self) -> Option<usize> {
        Pattern::horizontal_refl_part_2(&self.rows)
    }

    fn vertical_reflection_part_2(&self) -> Option<usize> {
        Pattern::horizontal_refl_part_2(&transpose(self.rows.clone()))
    }
}

/// from [this](https://stackoverflow.com/a/64499219) [stackoverflow](https://stackoverflow.com/questions/64498617/how-to-transpose-a-vector-of-vectors-in-rust) answer
fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

#[derive(Default)]
pub struct Day13 {
    patterns: Vec<Pattern>,
}

impl Day for Day13 {
    fn initialize(&mut self, input: &str) {
        self.patterns = input
            .split("\n\n")
            .map(Pattern::try_from)
            .collect::<Result<_, _>>()
            .unwrap();
    }

    fn part1(&mut self) -> Vec<String> {
        output!(self
            .patterns
            .iter()
            .map(|p| (p.horizontal_reflection(), p.vertical_reflection()))
            .map(|refl| match refl {
                (None, Some(vert)) => vert,
                (Some(hor), None) => hor * 100,
                (Some(_), Some(_)) | (None, None) =>
                    panic!("pattern should always have one and only one reflection"),
            })
            .sum::<usize>())
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .patterns
            .iter()
            .map(|p| (
                p.horizontal_reflection_part_2(),
                p.vertical_reflection_part_2()
            ))
            .map(|refl| match refl {
                (None, Some(vert)) => vert,
                (Some(hor), None) => hor * 100,
                (Some(_), Some(_)) | (None, None) =>
                    panic!("pattern should always have one and only one reflection"),
            })
            .sum::<usize>())
    }

    fn name(&self) -> usize {
        13
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example_input() {
        let mut day = Day13::default();
        day.initialize(
            "#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
",
        );
        assert_eq!(day.patterns[0].horizontal_reflection(), None);
        assert_eq!(day.patterns[1].horizontal_reflection(), Some(4));
        assert_eq!(day.patterns[0].vertical_reflection(), Some(5));
        assert_eq!(day.patterns[1].vertical_reflection(), None);
        assert_eq!(day.part1(), vec!["405"]);
        assert_eq!(day.part2(), vec!["400"]);
    }
}
