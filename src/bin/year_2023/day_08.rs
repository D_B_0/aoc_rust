use std::{collections::HashMap, vec};

use aoc::{output, Day};

#[derive(Debug, PartialEq, Eq)]
enum Direction {
    Left,
    Right,
}

#[derive(Default)]
pub struct Day08 {
    directions: Vec<Direction>,
    map: HashMap<String, (String, String)>,
}

impl Day08 {
    fn get_steps_to_first_z(&self, node: &str) -> usize {
        let mut node = node.to_owned();
        for (steps, dir) in self.directions.iter().cycle().enumerate() {
            if node.ends_with('Z') {
                return steps;
            }
            let (l, r) = self.map.get(&node).unwrap();
            node = match dir {
                Direction::Left => l.clone(),
                Direction::Right => r.clone(),
            };
        }
        unreachable!();
    }
}

impl Day for Day08 {
    fn initialize(&mut self, input: &str) {
        let mut lines = input.lines();
        self.directions = lines
            .next()
            .unwrap()
            .chars()
            .map(|c| match c {
                'L' => Direction::Left,
                'R' => Direction::Right,
                _ => unreachable!(),
            })
            .collect();
        self.map = lines
            .skip(1)
            .map(|line| {
                let line = line.strip_suffix(')').unwrap();
                let (node, lr) = line.split_once(" = (").unwrap();
                let (left, right) = lr.split_once(", ").unwrap();
                (node.to_owned(), (left.to_owned(), right.to_owned()))
            })
            .collect();
    }

    fn part1(&mut self) -> Vec<String> {
        let mut node = "AAA".to_owned();
        for (steps, dir) in self.directions.iter().cycle().enumerate() {
            if node == "ZZZ" {
                return output!(steps);
            }
            let (l, r) = self.map.get(&node).unwrap();
            node = match dir {
                Direction::Left => l.clone(),
                Direction::Right => r.clone(),
            };
        }
        unreachable!()
    }

    fn part2(&mut self) -> Vec<String> {
        output!(self
            .map
            .keys()
            .filter(|s| s.ends_with('A'))
            .map(|node| self.get_steps_to_first_z(node))
            .fold(1, num::integer::lcm))
    }

    fn name(&self) -> usize {
        8
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example_input_parses_correctly() {
        let mut day = Day08::default();
        day.initialize(
            "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
",
        );
        assert_eq!(day.directions, vec![Direction::Right, Direction::Left]);
        for elem in day.map {
            assert!(vec![
                ("AAA".to_owned(), ("BBB".to_owned(), "CCC".to_owned())),
                ("BBB".to_owned(), ("DDD".to_owned(), "EEE".to_owned())),
                ("CCC".to_owned(), ("ZZZ".to_owned(), "GGG".to_owned())),
                ("DDD".to_owned(), ("DDD".to_owned(), "DDD".to_owned())),
                ("EEE".to_owned(), ("EEE".to_owned(), "EEE".to_owned())),
                ("GGG".to_owned(), ("GGG".to_owned(), "GGG".to_owned())),
                ("ZZZ".to_owned(), ("ZZZ".to_owned(), "ZZZ".to_owned())),
            ]
            .contains(&elem))
        }
    }

    #[test]
    fn example_input_part_1() {
        let mut day = Day08::default();
        day.initialize(
            "RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
",
        );
        assert_eq!(day.part1(), vec!["2"]);
        let mut day = Day08::default();
        day.initialize(
            "LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
",
        );
        assert_eq!(day.part1(), vec!["6"]);
    }

    #[test]
    fn example_input_part_2() {
        let mut day = Day08::default();
        day.initialize(
            "LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)",
        );
        assert_eq!(day.part2(), vec!["6".to_owned()]);
    }
}
