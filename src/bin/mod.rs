use std::num::NonZeroUsize;

use aoc::{AocErr, InputError, RunArgs, Year};
use clap::{Parser, Subcommand};

mod year_2015;
mod year_2019;
mod year_2022;
mod year_2023;
mod year_2024;

#[derive(Parser)]
struct Args {
    #[arg(short, long)]
    session: Option<String>,
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Latest,
    Run {
        #[arg(short, long)]
        year: Option<usize>,
        #[arg(short, long)]
        days: Option<Vec<NonZeroUsize>>,
    },
}

fn main() {
    let args = Args::parse();
    let years = vec![
        Box::<year_2015::Year2015>::default() as Box<dyn Year>,
        Box::<year_2019::Year2019>::default() as Box<dyn Year>,
        Box::<year_2022::Year2022>::default() as Box<dyn Year>,
        Box::<year_2023::Year2023>::default() as Box<dyn Year>,
        Box::<year_2024::Year2024>::default() as Box<dyn Year>,
    ];
    match args.command {
        Commands::Latest => {
            match years.last().unwrap().run(RunArgs::Last, args.session) {
                Err(AocErr::NoDays) => {
                    eprintln!("The specified year has no days");
                }
                Err(AocErr::Input(input_error)) => match input_error {
                    InputError::ReadFile => eprintln!("Could not read input file"),
                    InputError::WriteFile => eprintln!("Could not write to input file"),
                    InputError::Request(error) => eprintln!("Request error: {error:?}"),
                    InputError::Response => eprintln!("Could not read `adventofcode.com` response"),
                },
                Err(_) => {
                    unreachable!("`Year::run(Last)` should only return `NoDays` or `Input` errors")
                }
                Ok(()) => {}
            };
        }
        Commands::Run { year, days } => match (year, days) {
            (None, None) => {
                for year in years {
                    match year.run(RunArgs::All, args.session.clone()) {
                        Err(AocErr::Input(input_error)) => match input_error {
                            InputError::ReadFile => eprintln!("Could not read input file"),
                            InputError::WriteFile => eprintln!("Could not write to input file"),
                            InputError::Request(error) => eprintln!("Request error: {error:?}"),
                            InputError::Response => {
                                eprintln!("Could not read `adventofcode.com` response")
                            }
                        },
                        Err(_) => {
                            unreachable!("`Year::run(All)` should only return `Input` errors")
                        }
                        Ok(()) => {}
                    }
                }
            }
            (None, Some(days)) => {
                for day in days {
                    match years
                        .last()
                        .unwrap()
                        .run(RunArgs::One(day.get() - 1), args.session.clone())
                    {
                        Err(AocErr::IndexOutOfBounds) => {
                            eprintln!("Day {day} does not exist");
                        }
                        Err(AocErr::Input(input_error)) => match input_error {
                            InputError::ReadFile => eprintln!("Could not read input file"),
                            InputError::WriteFile => eprintln!("Could not write to input file"),
                            InputError::Request(error) => eprintln!("Request error: {error:?}"),
                            InputError::Response => {
                                eprintln!("Could not read `adventofcode.com` response")
                            }
                        },
                        Err(_) => unreachable!(
                            "`Year::run(One)` should only return `IndexOutOfBounds` or `Input` errors"
                        ),
                        Ok(()) => {}
                    }
                }
            }
            (Some(year), None) => match years.iter().position(|y| y.name() == year) {
                Some(idx) => match years.get(idx).unwrap().run(RunArgs::All, args.session) {
                    Err(AocErr::Input(input_error)) => match input_error {
                        InputError::ReadFile => eprintln!("Could not read input file"),
                        InputError::WriteFile => eprintln!("Could not write to input file"),
                        InputError::Request(error) => eprintln!("Request error: {error:?}"),
                        InputError::Response => {
                            eprintln!("Could not read `adventofcode.com` response")
                        }
                    },
                    Err(_) => unreachable!("`Year::run(All)` should only return `Input` errors"),
                    Ok(()) => {}
                },
                None => eprintln!("The specified year does not exist"),
            },
            (Some(year), Some(days)) => match years.iter().position(|y| y.name() == year) {
                Some(idx) => {
                    for day in days {
                        match years
                            .get(idx)
                            .unwrap()
                            .run(RunArgs::One(day.get() - 1), args.session.clone())
                        {
                            Err(AocErr::IndexOutOfBounds) => {
                                eprintln!("The specified day does not exist");
                            }
                            Err(AocErr::Input(input_error)) => match input_error {
                                InputError::ReadFile => eprintln!("Could not read input file"),
                                InputError::WriteFile => eprintln!("Could not write to input file"),
                                InputError::Request(error) => eprintln!("Request error: {error:?}"),
                                InputError::Response => {
                                    eprintln!("Could not read `adventofcode.com` response")
                                }
                            },
                            Err(_) => unreachable!(
                                "`Year::run(One)` should only return `IndexOutOfBounds` or `Input` errors"
                            ),
                            Ok(()) => {}
                        }
                    }
                }
                None => eprintln!("The specified year does not exist"),
            },
        },
    }
}
