pub struct Combo<T> {
    elems: Vec<T>,
    mask: usize,
    mask_max: usize,
}

impl<T: Clone> Combo<T> {
    pub fn new(arr: &[T]) -> Self {
        Self {
            elems: arr.to_vec(),
            mask: 1,
            mask_max: 2usize.pow(arr.len().try_into().expect(
                "The `Combinations` iterator can only be used on with less than `u32::MAX` items",
            )),
        }
    }
}

impl<T: Clone> Iterator for Combo<T> {
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.mask >= self.mask_max {
            return None;
        }

        let mut res = vec![];
        for i in 0..self.elems.len() {
            if self.mask & (1 << i) != 0 {
                res.push(self.elems[i].clone())
            }
        }
        self.mask += 1;
        Some(res)
    }
}

/// [Wikipedia](https://en.wikipedia.org/wiki/Heap%27s_algorithm)
pub trait Combinations<T: Clone> {
    fn combinations(self) -> Combo<T>;
}

impl<I: Iterator> Combinations<I::Item> for I
where
    I::Item: Clone,
{
    fn combinations(self) -> Combo<I::Item>
    where
        Self: Sized,
    {
        let arr = self.collect::<Vec<I::Item>>();
        Combo::new(&arr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn vec_equivalent<T: PartialEq>(v1: &[T], v2: &[T]) -> bool {
        for t in v1 {
            if !v2.contains(t) {
                return false;
            }
        }
        for t in v2 {
            if !v1.contains(t) {
                return false;
            }
        }
        true
    }

    #[test]
    fn combo_3_elems() {
        let arr = [1, 2, 3];
        assert!(vec_equivalent(
            &arr.iter().cloned().combinations().collect::<Vec<_>>(),
            &[
                vec![1],
                vec![2],
                vec![3],
                vec![1, 2],
                vec![1, 3],
                vec![2, 3],
                vec![1, 2, 3],
            ]
        ));
    }

    #[test]
    fn combo_2_elems() {
        let arr = [1, 2];
        assert!(vec_equivalent(
            &arr.iter().cloned().combinations().collect::<Vec<_>>(),
            &[vec![1], vec![2], vec![1, 2],]
        ));
    }

    #[test]
    fn combo_1_elem() {
        let arr = [1];
        assert!(vec_equivalent(
            &arr.iter().cloned().combinations().collect::<Vec<_>>(),
            &[vec![1]]
        ));
    }

    #[test]
    fn combo_0_elems() {
        let arr: Vec<()> = vec![];
        assert_eq!(arr.iter().combinations().next(), None);
    }
}
