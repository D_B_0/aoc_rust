mod combinations;
mod coprime;
mod day;
mod error;
mod file_io;
mod grid;
mod permutations;
pub mod point;
mod year;

pub use combinations::{Combinations, Combo};
pub use coprime::Coprime;
pub use day::Day;
pub use error::{AocErr, InputError};
pub use file_io::*;
pub use grid::Grid;
pub use permutations::{Perm, Permutations};
pub use year::{RunArgs, Year};

#[macro_export]
macro_rules! output {
    ($x:expr) => {
        vec![format!("{}", $x)]
    };
}
