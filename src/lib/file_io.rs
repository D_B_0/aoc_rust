use std::{fmt::Debug, path::Path, str::FromStr};

pub fn get_file_chars<T: AsRef<Path>>(path: T) -> Vec<char> {
    get_file_string(path).chars().collect()
}

pub fn get_file_string<T: AsRef<Path>>(path: T) -> String {
    std::fs::read_to_string(path).unwrap()
}

pub fn get_file_lines<T: AsRef<Path>>(path: T) -> Vec<String> {
    get_file_string(path)
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| l.to_string())
        .collect()
}

pub fn get_file_numbers<N, T>(path: T) -> Vec<N>
where
    N: FromStr,
    <N as FromStr>::Err: Debug,
    T: AsRef<Path>,
{
    get_file_string(path)
        .split_whitespace()
        .map(|s| s.parse::<N>())
        .collect::<Result<Vec<_>, _>>()
        .unwrap()
}
