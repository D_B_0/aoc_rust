use std::fmt::Display;

#[derive(Default)]
pub struct Grid<T> {
    g: Vec<Vec<Option<T>>>,
}

impl<T> Grid<T> {
    pub fn new() -> Grid<T> {
        Grid { g: Vec::new() }
    }

    pub fn from_iter(iter: impl Iterator<Item = impl Iterator<Item = T>>) -> Grid<T> {
        Grid {
            g: iter.map(|it| it.map(Option::Some).collect()).collect(),
        }
    }

    pub fn with_size(width: usize, height: usize) -> Grid<T> {
        Grid {
            g: (0..width)
                .map(|_| (0..height).map(|_| None).collect())
                .collect(),
        }
    }

    pub fn get(&self, i: usize, j: usize) -> Option<&T> {
        self.g.get(i)?.get(j)?.as_ref()
    }

    pub fn get_mut(&mut self, i: usize, j: usize) -> Option<&mut T> {
        self.g.get_mut(i)?.get_mut(j)?.as_mut()
    }

    pub fn insert(&mut self, i: usize, j: usize, val: T) {
        while self.g.len() <= i {
            self.g.push(Vec::new());
        }
        while self.g[i].len() <= j {
            self.g[i].push(None);
        }
        self.g[i][j] = Some(val);
    }

    pub fn width(&self) -> usize {
        self.g.len()
    }

    pub fn height(&self) -> usize {
        self.g.iter().map(|v| v.len()).max().unwrap()
    }
}

impl<T: Display> Display for Grid<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for line in &self.g {
            for elem in line {
                if let Some(elem) = elem {
                    write!(f, "{}", elem)?;
                } else {
                    write!(f, " ")?;
                }
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}
