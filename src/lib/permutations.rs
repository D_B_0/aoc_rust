pub struct Perm<T> {
    arr: Vec<T>,
    c: Vec<usize>,
    i: usize,
}

impl<T: Clone> Perm<T> {
    pub fn new(arr: &[T]) -> Self {
        Self {
            arr: arr.to_vec(),
            c: vec![0; arr.len()],
            i: 0,
        }
    }
}

impl<T: Clone> Iterator for Perm<T> {
    type Item = Vec<T>;

    /// [Wikipedia](https://en.wikipedia.org/wiki/Heap%27s_algorithm)
    fn next(&mut self) -> Option<Self::Item> {
        if self.i == 0 {
            self.i = 1;
            return Some(self.arr.clone());
        }
        if self.i >= self.arr.len() {
            return None;
        }
        while self.c[self.i] >= self.i {
            self.c[self.i] = 0;
            self.i += 1;
            if self.i >= self.arr.len() {
                return None;
            }
        }
        if self.i % 2 == 0 {
            self.arr.swap(0, self.i);
        } else {
            self.arr.swap(self.c[self.i], self.i);
        }

        self.c[self.i] += 1;
        self.i = 1;

        Some(self.arr.clone())
    }
}

/// [Wikipedia](https://en.wikipedia.org/wiki/Heap%27s_algorithm)
pub trait Permutations<T: Clone> {
    fn permutations(self) -> Perm<T>;
}

impl<I: Iterator> Permutations<I::Item> for I
where
    I::Item: Clone,
{
    fn permutations(self) -> Perm<I::Item>
    where
        Self: Sized,
    {
        let arr = self.collect::<Vec<I::Item>>();
        Perm::new(&arr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let arr = [1, 2, 3];
        assert_eq!(
            arr.iter().cloned().permutations().collect::<Vec<_>>(),
            vec![
                vec![1, 2, 3],
                vec![2, 1, 3],
                vec![3, 1, 2],
                vec![1, 3, 2],
                vec![2, 3, 1],
                vec![3, 2, 1],
            ]
        );
    }
}
