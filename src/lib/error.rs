#[derive(Debug)]
pub enum AocErr {
    IndexOutOfBounds,
    NoDays,
    Input(InputError),
    ThreadJoin,
}

#[derive(Debug)]
pub enum InputError {
    ReadFile,
    WriteFile,
    Request(ureq::Error),
    Response,
}
