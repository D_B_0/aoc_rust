use std::{
    fmt::Write,
    path::Path,
    time::{Duration, Instant},
};

use crate::{AocErr, Day, InputError};

use human_duration::human_duration;

pub enum RunArgs {
    One(usize),
    All,
    Last,
    AllExcept(usize),
    Range(Box<dyn Iterator<Item = usize>>),
}

pub trait Year {
    fn days(&self) -> Vec<Box<dyn Day>>;
    fn name(&self) -> usize;

    fn run(&self, args: RunArgs, session: Option<String>) -> Result<(), AocErr> {
        let mut days = self.days();

        match args {
            RunArgs::Last => {
                let day = days.last_mut().ok_or(AocErr::NoDays)?.as_mut();
                let day_name = day.name();
                println!(
                    "{}",
                    run_day(
                        self.name(),
                        day,
                        &get_or_retrieve_input(self.name(), day_name, session)
                            .map_err(|err| AocErr::Input(err))?
                    )
                )
            }
            RunArgs::One(index) => {
                let day = days
                    .get_mut(index)
                    .ok_or(AocErr::IndexOutOfBounds)?
                    .as_mut();
                let day_name = day.name();
                println!(
                    "{}",
                    run_day(
                        self.name(),
                        day,
                        &get_or_retrieve_input(self.name(), day_name, session)
                            .map_err(|err| AocErr::Input(err))?
                    ),
                )
            }
            RunArgs::All => {
                let start = Instant::now();
                let mut handles = vec![];
                for mut d in days {
                    let year_name = self.name();
                    let day_name = d.name();
                    let session = session.clone();
                    handles.push(std::thread::spawn(move || -> Result<String, InputError> {
                        Ok(run_day(
                            year_name,
                            d.as_mut(),
                            &get_or_retrieve_input(year_name, day_name, session)?,
                        ))
                    }));
                }
                for handle in handles {
                    println!(
                        "{}",
                        handle
                            .join()
                            .map_err(|_err| AocErr::ThreadJoin)?
                            .map_err(|err| AocErr::Input(err))?
                    )
                }
                println!(
                    "Year {} took {}",
                    self.name(),
                    human_duration(&start.elapsed())
                )
            }
            RunArgs::AllExcept(index) => {
                for (i, d) in days.iter_mut().enumerate() {
                    if i != index {
                        let day_name = d.name();
                        println!(
                            "{}",
                            run_day(
                                self.name(),
                                d.as_mut(),
                                &get_or_retrieve_input(self.name(), day_name, session.clone())
                                    .map_err(|err| AocErr::Input(err))?
                            )
                        );
                    }
                }
            }
            RunArgs::Range(iter) => {
                for i in iter {
                    let day = days.get_mut(i).ok_or(AocErr::IndexOutOfBounds)?.as_mut();
                    let day_name = day.name();
                    println!(
                        "{}",
                        run_day(
                            self.name(),
                            day,
                            &get_or_retrieve_input(self.name(), day_name, session.clone())
                                .map_err(|err| AocErr::Input(err))?
                        )
                    );
                }
            }
        }
        Ok(())
    }
}

fn run_day(year: usize, d: &mut dyn Day, input: &str) -> String {
    let mut output = format!("----- {} day {} -----\n", year, d.name());

    let start = Instant::now();
    output.push_str("Initializing... ");
    d.initialize(input);
    writeln!(output, "({})", human_duration(&start.elapsed())).unwrap();

    let start = Instant::now();
    let part1 = d.part1();
    writeln!(output, "{}", format_answer(1, &part1, start.elapsed())).unwrap();

    let start = Instant::now();
    let part2 = d.part2();
    write!(output, "{}", format_answer(2, &part2, start.elapsed())).unwrap();
    output
}

fn format_answer(which: usize, answer: &[String], duration: Duration) -> String {
    let mut iter = answer.iter();
    let mut output = format!(
        "Part {which}: {} ({})",
        iter.next().unwrap(),
        human_duration(&duration),
    );
    for line in iter {
        write!(output, "\n        {}", line).unwrap();
    }
    output
}

fn get_or_retrieve_input(
    year: usize,
    day: usize,
    session: Option<String>,
) -> Result<String, InputError> {
    let input_file = format!("inputs/{year}/day_{day:02}.txt");
    if let Ok(file) = std::fs::read_to_string(&input_file) {
        return Ok(file);
    }
    let Some(session) = session else {
        return Err(InputError::ReadFile);
    };
    let body = ureq::get(&format!("https://adventofcode.com/{year}/day/{day}/input"))
        .timeout(Duration::from_secs(10))
        .set("Cookie", &format!("session={session}"))
        .call()
        .map_err(|err| InputError::Request(err))?
        .into_string()
        .map_err(|_err| InputError::Response)?;
    let path = Path::new(&input_file);
    std::fs::create_dir_all(path.parent().ok_or(InputError::WriteFile)?)
        .map_err(|_| InputError::WriteFile)?;
    std::fs::write(path, body.clone()).map_err(|_| InputError::WriteFile)?;
    Ok(body)
}
