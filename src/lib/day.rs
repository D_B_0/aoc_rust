pub trait Day: Send {
    fn initialize(&mut self, input: &str);
    fn part1(&mut self) -> Vec<String>;
    fn part2(&mut self) -> Vec<String>;
    fn name(&self) -> usize;
}
