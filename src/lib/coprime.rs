use num::Integer;

pub trait Coprime {
    fn is_coprime_with(&self, other: &Self) -> bool;
}

impl Coprime for usize {
    fn is_coprime_with(&self, other: &Self) -> bool {
        self.gcd(other) == 1 || (self == &1 && other == &1)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        assert!(2usize.is_coprime_with(&1));
        assert!(!2usize.is_coprime_with(&4));
    }
}
